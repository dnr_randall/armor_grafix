(function() {
    "use strict";
    /* declare global variables within the class */
    var uiBillingContainer,
        uiShippingContainer,
        uiDifferentShippingCheckbox,
        uiCheckoutForm,
        uiBillingCountrySelect,
        uiShippingCountrySelect,
        uiBillingStateSelect,
        uiShippingStateSelect,
        uiInputSubtotalTotal,
        uiInputShippingTotal,
        uiInputTaxTotal,
        uiInputTaxPercentage,
        uiInputDiscountTotal,
        uiInputTotal,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function() {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function(oData) {
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function() {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }
    /*
     * This js file will only contain front_checkout events
     *
     * */
    CPlatform.prototype.front_checkout = {

            initialize: function() {
                /* assign a value to the global variable within this class */
                uiCheckoutForm = $('#checkout-form');
                uiBillingContainer = $('.billing-container');
                uiShippingContainer = $('.shipping-container');
                uiDifferentShippingCheckbox = $('[name="is_different_shipping_address"]');
                uiBillingCountrySelect = $('.billing-country-select');
                uiShippingCountrySelect = $('.shipping-country-select');
                uiBillingStateSelect = $('.billing-state-select');
                uiShippingStateSelect = $('.shipping-state-select');
                uiInputSubtotalTotal = $('[name="subtotal_total"]');
                uiInputShippingTotal = $('[name="shipping_total"]');
                uiInputTaxTotal = $('[name="tax_total"]');
                uiInputTaxPercentage = $('[name="tax_percentage"]');
                uiInputDiscountTotal = $('[name="discount_total"]');
                uiInputTotal = $('[name="total"]');

                platform.front_checkout.billing_detail_events();
                platform.front_checkout.shipping_detail_events();

                uiDifferentShippingCheckbox.on('change', function() {
                    var uiThis = $(this);
                    if (!uiThis.is(':checked')) {
                        uiShippingContainer.find('.form-group').removeClass('has-success has-error').find('.help-block').remove();
                        uiShippingContainer.slideUp();
                        shipping_fedex_get($('#billing_zip').val());
                    } else {
                        uiShippingContainer.slideDown();
                        shipping_fedex_get($('#shipping_zip').val());
                    }
                    // alert('check');
                    platform.front_checkout.is_different_shipping_address();
                });

                uiBillingContainer.find('input').on('change', function() {
                    // platform.front_checkout.is_different_shipping_address();
                });

                uiCheckoutForm.validate({
                    errorClass: 'help-block animation-slideDown',
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.form-group').find('.help-block').remove();
                    },
                    success: function(e) {
                        e.closest('.form-group').removeClass('has-success has-error');
                        e.closest('.form-group').find('.help-block').remove();
                    },
                    submitHandler: function(form) {
                        platform.front_checkout.is_different_shipping_address();
                        platform.show_spinner($(form).find('[type="submit"]'), true);
                        form.submit();
                    },
                    rules: {
                        'billing_first_name': {
                            required: true
                        },
                        'billing_last_name': {
                            required: true
                        },
                        'billing_email': {
                            required: true,
                            email: true
                        },
                        'billing_company': {
                            required: false
                        },
                        'billing_phone': {
                            required: true
                        },
                        'billing_address': {
                            required: true
                        },
                        'billing_city': {
                            required: true
                        },
                        'billing_state': {
                            required: true
                        },
                        'billing_zip': {
                            required: true
                        },
                        'billing_country': {
                            required: true
                        },
                        'shipping_first_name': {
                            'required': function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_last_name': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_email': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            },
                            email: true
                        },
                        'shipping_company': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_phone': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            },
                        },
                        'shipping_address': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_city': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_state': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_zip': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'shipping_country': {
                            required: function(element) {
                                return uiDifferentShippingCheckbox.is(':checked');
                            }
                        },
                        'card_number': {
                            required: true
                        },
                        'card_expiration_month': {
                            required: true
                        },
                        'card_expiration_year': {
                            required: true
                        },
                        'card_name': {
                            required: true
                        },
                        'card_cvc': {
                            required: true
                        },
                    },
                    messages: {
                        'billing_first_name': {
                            required: 'First Name is required.'
                        },
                        'billing_last_name': {
                            required: 'Last Name is required.'
                        },
                        'billing_email': {
                            required: 'Email is required.',
                            email: 'Please enter a valid email address.'
                        },
                        'billing_company': {
                            required: 'Company is required.'
                        },
                        'billing_phone': {
                            required: 'Phone is required.',
                        },
                        'billing_address': {
                            required: 'Address is required.'
                        },
                        'billing_city': {
                            required: 'City is required.'
                        },
                        'billing_state': {
                            required: 'State is required.'
                        },
                        'billing_zip': {
                            required: 'Zip is required.'
                        },
                        'billing_country': {
                            required: 'Country is required.'
                        },
                        'shipping_first_name': {
                            required: 'First Name is required.'
                        },
                        'shipping_last_name': {
                            required: 'Last Name is required.'
                        },
                        'shipping_email': {
                            required: 'Email is required.',
                            email: 'Please enter a valid email address.'
                        },
                        'shipping_company': {
                            required: 'Company is required.'
                        },
                        'shipping_phone': {
                            required: 'Phone is required.',
                        },
                        'shipping_address': {
                            required: 'Address is required.'
                        },
                        'shipping_city': {
                            required: 'City is required.'
                        },
                        'shipping_state': {
                            required: 'State is required.'
                        },
                        'shipping_zip': {
                            required: 'Zip is required.'
                        },
                        'shipping_country': {
                            required: 'Country is required.'
                        },
                        'card_number': {
                            required: 'Card number is required.',
                        },
                        'card_expiration_month': {
                            required: 'Card expiration month is required.',
                        },
                        'card_expiration_year': {
                            required: 'Card expiration year is required.',
                        },
                        'card_name': {
                            required: 'Card name is required.',
                        },
                        'card_cvc': {
                            required: 'Card CVC is required.',
                        },
                    }
                });
            },

            billing_detail_events: function() {
                /* state select */
                uiBillingStateSelect.select2({ width: '98.5%' }).on('change', function() {
                    var uiThis = $(this);
                    var iStateId = uiThis.val();
                    if (uiThis.val() != '') {
                        uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                        uiThis.closest('.form-group').removeClass('has-success has-error');
                        uiThis.closest('.form-group').find('.help-block').remove();
                    }
                    // platform.front_checkout.is_different_shipping_address();
                    platform.front_checkout.compute_tax();
                });

                /* checkout select */
                uiBillingCountrySelect.prop("disabled", true).select2({ width: '98.5%' }).on('change', function() {
                    var uiThis = $(this);
                    var iCountryId = uiThis.find('option:selected').attr('data-country-id');
                    if (platform.var_check(iCountryId)) {
                        // platform.front_checkout.is_different_shipping_address();
                        // platform.front_checkout.shipping_method();
                        uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                        uiThis.closest('.form-group').removeClass('has-success has-error');
                        uiThis.closest('.form-group').find('.help-block').remove();

                        uiBillingStateSelect.html('<option value=""></option>');
                        var sStates = uiThis.find('option[data-country-id="' + iCountryId + '"]').attr('data-states');
                        var oStates = platform.parse_json(sStates);
                        for (var x in oStates) {
                            var state = oStates[x];

                            var data = {
                                id: state.id,
                                code: state.code,
                                text: state.name,
                                tax: state.tax
                            };

                            var uiNewOption = '<option value="' + data.text + '" ' +
                                'data-state-name="' + data.text + '" ' +
                                'data-state-id="' + data.id + '" ' +
                                'data-state-tax="' + data.tax + '" ' +
                                'data-country-id="' + iCountryId + '" ' +
                                '>' + data.text + '</option>';
                            uiBillingStateSelect.append(uiNewOption);
                        }

                        uiBillingStateSelect.trigger('change');
                        uiBillingStateSelect.data('select2').destroy();
                        uiBillingStateSelect.select2({ width: '98.5%' });
                        // if (!uiBillingStateSelect.hasClass('loaded')) {
                        uiBillingStateSelect.val(uiBillingStateSelect.attr('data-old-value'));
                        uiBillingStateSelect.trigger('change');
                        // uiBillingStateSelect.addClass('loaded');
                        // }
                    }
                });

                if (!uiBillingCountrySelect.find('option[selected]').length) {
                    uiBillingCountrySelect.val(uiBillingCountrySelect.find('option[data-is-default="1"]').text());
                }
                uiBillingCountrySelect.trigger('change');
            },

            shipping_detail_events: function() {
                /* state select */
                uiShippingStateSelect.select2({ width: '98.5%' }).on('change', function() {
                    var uiThis = $(this);
                    var iStateId = uiThis.val();
                    if (uiThis.val() != '') {
                        uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                        uiThis.closest('.form-group').removeClass('has-success has-error');
                        uiThis.closest('.form-group').find('.help-block').remove();
                    }
                    platform.front_checkout.compute_tax();
                });

                /* checkout select */
                uiShippingCountrySelect.prop("disabled", true).select2({ width: '98.5%' }).on('change', function() {
                    var uiThis = $(this);
                    var iCountryId = uiThis.find('option:selected').attr('data-country-id');
                    if (platform.var_check(iCountryId)) {
                        // platform.front_checkout.shipping_method();
                        uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                        uiThis.closest('.form-group').removeClass('has-success has-error');
                        uiThis.closest('.form-group').find('.help-block').remove();

                        uiShippingStateSelect.html('<option value=""></option>');
                        var sStates = uiThis.find('option[data-country-id="' + iCountryId + '"]').attr('data-states');
                        var oStates = platform.parse_json(sStates);
                        for (var x in oStates) {
                            var state = oStates[x];

                            var data = {
                                id: state.id,
                                code: state.code,
                                text: state.name,
                                tax: state.tax
                            };

                            var uiNewOption = '<option value="' + data.text + '" ' +
                                'data-state-name="' + data.text + '" ' +
                                'data-state-id="' + data.id + '" ' +
                                'data-state-tax="' + data.tax + '" ' +
                                'data-country-id="' + iCountryId + '" ' +
                                '>' + data.text + '</option>';
                            uiShippingStateSelect.append(uiNewOption);
                        }

                        uiShippingStateSelect.trigger('change');
                        uiShippingStateSelect.data('select2').destroy();
                        uiShippingStateSelect.select2({ width: '98.5%' });
                        // if (!uiShippingStateSelect.hasClass('loaded')) {
                        uiShippingStateSelect.val(uiShippingStateSelect.attr('data-old-value'));
                        uiShippingStateSelect.trigger('change');
                        // uiShippingStateSelect.addClass('loaded');
                        // }
                    }
                });

                if (!uiShippingCountrySelect.find('option[selected]').length) {
                    uiShippingCountrySelect.val(uiShippingCountrySelect.find('option[data-is-default="1"]').text());
                }
                uiShippingCountrySelect.trigger('change');
            },

            is_different_shipping_address: function(fnCallback) {
                // alert('test');
                if (!uiCheckoutForm.find('[name="is_different_shipping_address"]').is(':checked')) {

                    var sBillingFirstName = uiBillingContainer.find('[name="billing_first_name"]').val();
                    var sBillingLastName = uiBillingContainer.find('[name="billing_last_name"]').val();
                    var sBillingEmail = uiBillingContainer.find('[name="billing_email"]').val();
                    var sBillingCompany = uiBillingContainer.find('[name="billing_company"]').val();
                    var sBillingPhone = uiBillingContainer.find('[name="billing_phone"]').val();
                    var sBillingExt = uiBillingContainer.find('[name="billing_ext"]').val();
                    var sBillingAddress = uiBillingContainer.find('[name="billing_address"]').val();
                    var sBillingAddress2 = uiBillingContainer.find('[name="billing_address_2"]').val();
                    var sBillingCity = uiBillingContainer.find('[name="billing_city"]').val();
                    var sBillingState = uiBillingContainer.find('[name="billing_state"]').val();
                    var sBillingZip = uiBillingContainer.find('[name="billing_zip"]').val();
                    var sBillingCountry = uiBillingContainer.find('[name="billing_country"]').val();
                    uiShippingContainer.find('[name="shipping_first_name"]').val(sBillingFirstName);
                    uiShippingContainer.find('[name="shipping_last_name"]').val(sBillingLastName);
                    uiShippingContainer.find('[name="shipping_email"]').val(sBillingEmail);
                    uiShippingContainer.find('[name="shipping_company"]').val(sBillingCompany);
                    uiShippingContainer.find('[name="shipping_phone"]').val(sBillingPhone);
                    uiShippingContainer.find('[name="shipping_ext"]').val(sBillingExt);
                    uiShippingContainer.find('[name="shipping_address"]').val(sBillingAddress);
                    uiShippingContainer.find('[name="shipping_address_2"]').val(sBillingAddress2);
                    uiShippingContainer.find('[name="shipping_city"]').val(sBillingCity);
                    uiShippingContainer.find('[name="shipping_zip"]').val(sBillingZip);
                    uiShippingContainer.find('[name="shipping_country"]').val(sBillingCountry);
                    uiShippingCountrySelect.trigger('change');
                    uiShippingContainer.find('[name="shipping_state"]').val(sBillingState);
                    uiShippingStateSelect.trigger('change');
                }

                if (typeof(fnCallback) == 'function') {
                    fnCallback();
                }
            },

            compute_shipping: function(fnCallback) {
                var iShippingTotal = 0;

                uiInputShippingTotal.val(iShippingTotal);

                platform.front_checkout.compute_total();

                if (typeof(fnCallback) == 'function') {
                    fnCallback();
                }
            },

            compute_tax: function(fnCallback) {
                var uiAddress = !uiCheckoutForm.find('[name="is_different_shipping_address"]').is(':checked') ? uiBillingStateSelect : uiShippingStateSelect;
                var iTax = uiAddress.find('option:selected').attr('data-state-tax');
                var iSubtotal = platform.remove_commas($('.subtotal_total').text());
                var iTaxTotal = 0.00;

                if (platform.var_check(iTax)) {
                    iTaxTotal = parseFloat(iSubtotal) * parseFloat(iTax);
                }
                $('.tax_total').text(platform.number_format(iTaxTotal.toFixed(2), 2, '.', ','));

                uiInputTaxTotal.val(iTaxTotal);
                uiInputTaxPercentage.val(iTax);

                platform.front_checkout.compute_total();

                if (typeof(fnCallback) == 'function') {
                    fnCallback();
                }
            },

            compute_total: function(fnCallback) {
                var iSubtotal = platform.remove_commas($('.subtotal_total').text());
                var iDiscountTotal = platform.remove_commas($('.discount_total').text());
                var iTaxTotal = platform.remove_commas($('.tax_total').text());
                var iShippingTotal = platform.remove_commas($('[name="shipping_total"]').val());
                var iTotal = (parseFloat(iSubtotal) + parseFloat(iTaxTotal) + parseFloat(iShippingTotal)) - (parseFloat(iDiscountTotal));
                console.log(iTotal);
                if (platform.var_check(iTotal)) {
                    $('.total').text(platform.number_format(iTotal.toFixed(2), 2, '.', ','));
                }

                uiInputTotal.val(iTotal);

                if (typeof(fnCallback) == 'function') {
                    fnCallback();
                }
            },
        }
        /* run initialize function on load of window */
    $(window).on('load', function() {
        // $('.billing-payment__place-order').prop('disabled', true);
        platform.front_checkout.initialize();
        // $("body").on("focusout", "#billing_zip", function() {
        //     _ajax({
        //         type: "POST",
        //         data: {
        //             zip_code: $(this).val(),
        //         },
        //         url: "shipping-rate"
        //     }, function(data) {
        //         if (data) {
        //             $(".shipping_rates").empty();
        //             for (let i = 0; i < Object.values(data).length; i++) {
        //                 $(".shipping_rates").append(`<div class="col-12 billing-payment--border-bottom">
        //                     <p><input style="width:50px;" type="radio" class="shippings" name="rad_ship" value="` + Object.values(data)[i] + `">` + Object.keys(data)[i].replace("_", " ") + `
        //                         <strong class="float-right">$ ` + Object.values(data)[i] + `</strong></p>
        //                 </div>`);
        //             }
        //             // $('.billing-payment__place-order').prop('disabled', false);
        //         } else
        //             alert("Area not supported by FedEx");
        //     });
        // })

        $("body").on("change", ".shippings", function() {
            var iSubtotal = ($('.subtotal_total').text().replace(/\,/g, ''));
            console.log(iSubtotal);
            var iDiscountTotal = $('.discount_total').text().replace(/\,/g, '');
            var iTaxTotal = $('.tax_total').text().replace(/\,/g, '');
            var iShippingTotal = $(this).val();
            var iTotal = (parseFloat(iSubtotal) + parseFloat(iTaxTotal) + parseFloat(iShippingTotal)) - (parseFloat(iDiscountTotal));
            $('[name="shipping_total"]').val(iShippingTotal);
            $('.total').text(platform.number_format(iTotal.toFixed(2), 2, '.', ','));
            $('[name="total"]').val(iTotal);
            $('#shipping_description').val($(this).attr('value_2'));
        });

        $("body").on("focusout", "#shipping_zip,#billing_zip", function() {

            var exec = '';
            if ($("#is_different_shipping_address").is(':checked')) {
                //shipping
                var val = $(this).val();
                if ($(this).attr('id') != 'billing_zip') {
                    shipping_fedex_get(val);
                }
            } else {
                //billing
                var val = $(this).val();
                shipping_fedex_get(val);
            }

        });




        // var values = {};
        // $.each($('#checkout-form').serializeArray(), function(i, field) {
        //     values[field.name] = field.value;
        // });

        // var sBillingFirstName = uiBillingContainer.find('[name="billing_first_name"]').val();
        // var sBillingLastName = uiBillingContainer.find('[name="billing_last_name"]').val();
        // var sBillingEmail = uiBillingContainer.find('[name="billing_email"]').val();
        // var sBillingCompany = uiBillingContainer.find('[name="billing_company"]').val();
        // var sBillingPhone = uiBillingContainer.find('[name="billing_phone"]').val();
        // var sBillingExt = uiBillingContainer.find('[name="billing_ext"]').val();
        // var sBillingAddress = uiBillingContainer.find('[name="billing_address"]').val();
        // var sBillingAddress2 = uiBillingContainer.find('[name="billing_address_2"]').val();
        // var sBillingCity = uiBillingContainer.find('[name="billing_city"]').val();
        // var sBillingState = uiBillingContainer.find('[name="billing_state"]').val();
        // var sBillingZip = uiBillingContainer.find('[name="billing_zip"]').val();
        // var sBillingCountry = uiBillingContainer.find('[name="billing_country"]').val();
        // var sBillingFirstName_ui_shiping = values['shipping_first_name'];
        // var sBillingLastName_ui_shiping = values['shipping_last_name'];
        // var sBillingAddress_ui_shiping = values['shipping_address'];
        // var sBillingAddress2_ui_shiping = values['shipping_address_2'];
        // var sBillingState_ui_shiping = values['shipping_state'];
        // var sBillingCity_ui_shiping = values['shipping_city'];
        // var sBillingZip_ui_shiping = values['shipping_zip'];

        $("body").on("click", "#next-checkout", function() {
            // Render the PayPal button into #paypal-button-container.
            var uiShippingContainer = $('.shipping-container');
            var sBillingcountry_ui_shiping = uiShippingContainer.find('[name="shipping_country"]');
            sBillingcountry_ui_shiping = sBillingcountry_ui_shiping.find('option:selected').attr('data-country-code');

            platform.front_checkout.is_different_shipping_address();
            var serialize = $('#checkout-form').serialize();

            console.log(serialize);

            $.ajax({
                type: "get",
                url: "checkout/validation",
                data: serialize,
                beforeSend: function() {
                    $('.return_valid_serv').each(function() {
                        $(this).html('');

                    });
                },
                success: function(data) {
                    $("#checkout-form :input").prop("disabled", true);
                    $("#edit-details").removeAttr("hidden");
                    $("#edit-details").removeAttr("disabled");
                    // $("#edit-details").prop("hidden",true);
                    $('#next-checkout').prop("hidden", true);
                    $('.billing-country-select').prop('disabled', true);
                    $('.shipping-country-select').prop('disabled', true);

                    $('.row_payment').prop("hidden", false);
                    console.log(data);
                    paypal_load(data);
                },
                error: function(err) {
                    // alert('Please complete necessary fields.');
                    $.each(err.responseJSON.errors, function(i, error) {
                        var el = $(document).find('[name="' + i + '"]');
                        if ($("#return_" + i).length == 0) {
                            el.after($('<span class="return_valid_serv" id="return_' + i + '" style="color: red;">This field is required.</span>'));
                        } else {
                            $("#return_" + i).html('<span class="return_valid_serv" id="return_' + i + '" style="color: red;">This field is required.</span>');
                        }

                        swal('Incomplete information.', 'Please complete the details.', 'error');
                    });
                }
            });
        });


        $('#edit-details').on('click', function() {
            $('#paypal-button-container').html('');
            $(this).attr('hidden', 'hidden');
            // $('#paypal_here').attr('hidden', 'hidden');
            $("#checkout-form :input").removeAttr("disabled");
            $('#next-checkout').removeAttr('hidden');
            $('.billing-country-select').prop('disabled', true);
            $('.shipping-country-select').prop('disabled', true);
            $('.row_payment').prop("hidden", true);
            // $('#order_place_hid').attr('hidden', 'hidden');
        });



        function paypal_load(data_info) {

            console.log(data_info);

            if (data_info['is_different_shipping_address'] == 'on') //same
            {

                var enable_ship = {
                    name: {
                        full_name: data_info['shipping_first_name'] + ' ' + data_info['shipping_last_name']
                    },
                    address: {
                        address_line_1: data_info['shipping_address'],
                        address_line_2: data_info['shipping_address_2'],
                        admin_area_1: "CA",
                        // admin_area_1: data_info['shipping_state'],
                        admin_area_2: data_info['shipping_city'],
                        postal_code: data_info['shipping_zip'],
                        country_code: 'US'
                    },
                };

            } else {


                var enable_ship = {
                    name: {
                        full_name: data_info['billing_first_name'] + ' ' + data_info['billing_last_name']
                    },
                    address: {
                        address_line_1: data_info['billing_address'],
                        address_line_2: data_info['billing_address_2'],
                        admin_area_1: "CA",
                        // admin_area_1: data_info['billing_state'],
                        admin_area_2: data_info['billing_city'],
                        postal_code: data_info['billing_zip'],
                        country_code: 'US'
                    },
                };


            }

            var FUNDING_SOURCES = [
                paypal.FUNDING.PAYPAL,
                // paypal.FUNDING.PAYLATER,
                // paypal.FUNDING.VENMO,
                paypal.FUNDING.CREDIT,
                paypal.FUNDING.CARD
            ];

            FUNDING_SOURCES.forEach(function(fundingSource) {

                var button = paypal.Buttons({
                    fundingSource: fundingSource,
                    // Set up the transaction
                    createOrder: function(data, actions) {
                        return actions.order.create({
                            payer: {
                                name: {
                                    given_name: data_info['billing_first_name'],
                                    surname: data_info['billing_last_name']
                                },
                                address: {
                                    address_line_1: data_info['billing_address'],
                                    address_line_2: data_info['billing_address_2'],
                                    // admin_area_1: data_info['billing_state'],
                                    admin_area_1: "CA",
                                    admin_area_2: data_info['billing_city'],
                                    postal_code: data_info['billing_zip'],
                                    country_code: "US"
                                },
                                email_address: data_info['billing_email'],
                                phone: {
                                    phone_type: "MOBILE",
                                    phone_number: {
                                        national_number: data_info['billing_phone'].replace(/[^0-9]/g, '')
                                    }
                                }
                            },
                            purchase_units: [{
                                amount: {
                                    value: parseFloat(data_info['total']).toFixed(2),
                                    currency_code: 'USD'
                                },
                                shipping: enable_ship
                            }],
                        });
                    },

                    // Finalize the transaction
                    onApprove: function(data, actions) {
                        return actions.order.capture().then(function(details) {
                            // Show a success message to the buyer
                            // alert('Transaction completed by ' + details.payer.name.given_name + '!');

                            //paypal details
                            console.log(details);
                            store_transaction(data_info, details['id']);
                            swal({
                                title: 'Almost there!',
                                text: 'Please wait.. We are processing your order.',
                                type: 'success',
                                showConfirmButton: false
                            });

                        });
                    },
                    onError: function() {
                        swal({
                            title: 'Opps something went wrong.',
                            text: 'Please refresh the page and try again.',
                            type: 'error',
                            // showConfirmButton: false
                        });
                    }
                });

                // Check if the button is eligible
                if (button.isEligible()) {

                    // Render the standalone button for that funding source
                    button.render('#paypal-button-container');
                }
            });

            //     paypal.Buttons({
            //     // Set up the transaction
            //     createOrder: function(data, actions) {
            //         return actions.order.create({
            //             payer: {
            //                 name: {
            //                     given_name: data_info['billing_first_name'],
            //                     surname: data_info['billing_last_name']
            //                 },
            //                 address: {
            //                     address_line_1: data_info['billing_address'],
            //                     address_line_2: data_info['billing_address_2'],
            //                     // admin_area_1: data_info['billing_state'],
            //                     admin_area_1: "CA",
            //                     admin_area_2: data_info['billing_city'],
            //                     postal_code: data_info['billing_zip'],
            //                     country_code: "US"
            //                 },
            //                 email_address: data_info['billing_email'],
            //                 phone: {
            //                     phone_type: "MOBILE",
            //                     phone_number: {
            //                         national_number: data_info['billing_phone'].replaceAll('-', '')
            //                     }
            //                 }
            //             },
            //             purchase_units: [{
            //                 amount: {
            //                     value: parseFloat(data_info['total']).toFixed(2),
            //                     currency_code: 'USD'
            //                 },
            //                 shipping: enable_ship
            //             }],
            //         });
            //     },
            //
            //     // Finalize the transaction
            //     onApprove: function(data, actions) {
            //         return actions.order.capture().then(function(details) {
            //             // Show a success message to the buyer
            //             // alert('Transaction completed by ' + details.payer.name.given_name + '!');
            //
            //             //paypal details
            //             console.log(details);
            //             store_transaction(data_info, details['id']);
            //             swal({
            //                 title: 'Order Complete!',
            //                 text: 'Please wait.. We will redirect you.',
            //                 type: 'success',
            //                 showConfirmButton: false
            //             });
            //
            //         });
            //     },
            //     onError: function() {
            //         swal({
            //             title: 'Opps something went wrong.',
            //             text: 'Please refresh the page and try again.',
            //             type: 'error',
            //             // showConfirmButton: false
            //         });
            //     }
            // }, ).render('#paypal-button-container');


        }

        function store_transaction(data, transaction_id) {
            $.ajax({
                type: "get",
                url: "checkout/store/transaction/" + transaction_id,
                data: data,
                beforeSend: function() {

                },
                success: function(data) {
                    // console.log(data);

                    window.location.href = 'checkout/' + data + '/success';

                    // alert('Order Saved');
                },
                error: function(err) {
                    swal('Oops! Something went wrong.', 'an error occurred while processing your request, Please try again.', 'error');
                }
            });

        }
    });


    // alert(csrf_token());

    if (check_auth() == 1) {
        shipping_fedex_get($('#billing_zip').val());
    }

    function shipping_fedex_get(val) {
        $.ajax({
            type: "POST",
            data: {
                zip_code: val,
                _token: csrf_token(),
            },
            url: "shipping-rate",
            beforeSend: function() {
                $(".shipping_rates").empty();
                $(".shipping_rates").append('<center>Please wait..</center>');
                $('#next-checkout').prop('disabled', true);
                $('#next-checkout').prop('style', 'background:#c89bd4');
            },
            success: function(data) {
                if (data) {
                    $(".shipping_rates").empty();
                    $('#next-checkout').prop('disabled', false);
                    $('#next-checkout').prop('style', '');
                    var shipping_available = ['STANDARD_OVERNIGHT', 'FEDEX_2_DAY', 'FEDEX_GROUND'];
                    var c = 0;
                    for (let i = 0; i < Object.values(data).length; i++) {

                        console.log(Object.keys(data)[i]);
                        if (shipping_available.includes(Object.keys(data)[i])) {
                            var s = '';
                            c++;
                            // if (c == 1) {
                            //     s = 'checked'
                            //     $('#shipping_description').val(Object.keys(data)[i]);
                            // }
                            $(".shipping_rates").append(`<div class="col-12 billing-payment--border-bottom">
                            <p><input style="width:50px;" type="radio" class="shippings" ` + s + ` name="rad_ship" value_2="` + Object.keys(data)[i] + `" value="` + Object.values(data)[i] + `">` + Object.keys(data)[i].replace("_", " ") + `
                                <strong class="float-right">$ ` + Object.values(data)[i] + `</strong></p>
                        </div>`);

                        }
                    }

                    if (data == '') {
                        $(".shipping_rates").empty();
                        $(".shipping_rates").append('<center>No Shipping available on the zip code.</center>');
                        $('#next-checkout').prop('disabled', true);
                        $('#next-checkout').prop('style', 'background:#c89bd4');
                    }
                    // $(".shipping_rates").append(`<div class="col-12 billing-payment--border-bottom">
                    //     <h4 class="float-left"></h4>
                    //     <h4 class="float-right billing-payment__right-side">
                    //         $
                    //         <span class="shipping_total"></span>
                    //     </h4>
                    // </div>`);
                    // // $("#shipping_total").html(data);
                    // $('.billing-payment__place-order').prop('disabled', false);
                } else
                    alert("Area not supported by FedEx");
            },
            complete: function() {
                $(".shipping_rates").append('<input type="hidden" id="shipping_description" name="shipping_method" value="">');
            }
        });
    };

    // function shipping_fedex_get(val) {

    //     $.ajax({
    //         type: "POST",
    //         data: {
    //             zip_code: val,
    //         },
    //         url: "shipping-rate",
    //         beforeSend: function() {
    //             console.log('beforesend');
    //         },
    //         success: function(data) {
    //             if (data) {
    //                 $(".shipping_rates").empty();
    //                 for (let i = 0; i < Object.values(data).length; i++) {
    //                     $(".shipping_rates").append(`<div class="col-12 billing-payment--border-bottom">
    //                     <p><input style="width:50px;" type="radio" class="shippings" name="rad_ship" value="` + Object.values(data)[i] + `">` + Object.keys(data)[i].replace("_", " ") + `
    //                         <strong class="float-right">$ ` + Object.values(data)[i] + `</strong></p>
    //                     </div>`);
    //                 }
    //             } else {
    //                 alert("Area not supported by FedEx");
    //             }
    //         }
    //     });
    // }

}());