(function () {
    "use strict";
    /* declare global variables within the class */
    var uiAddressForm,
        uiBillingCountrySelect,
        uiShippingCountrySelect,
        uiBillingStateSelect,
        uiShippingStateSelect,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_address events
     *
     * */
    CPlatform.prototype.front_address = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiAddressForm = $('#edit-address');
            uiBillingCountrySelect = $('.billing-country-select');
            uiShippingCountrySelect = $('.shipping-country-select');
            uiBillingStateSelect = $('.billing-state-select');
            uiShippingStateSelect = $('.shipping-state-select');

            platform.front_address.billing_detail_events();
            platform.front_address.shipping_detail_events();

            uiAddressForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'billing_first_name': {
                        required: true,
                    },
                    'billing_last_name': {
                        required: true,
                    },
                    'billing_email': {
                        required: true,
                        email: true,
                    },
                    'billing_company': {
                        required: true,
                    },
                    'billing_phone': {
                        required: true,
                    },
                    'billing_address': {
                        required: true,
                    },
                    'billing_city': {
                        required: true,
                    },
                    'billing_state': {
                        required: true,
                    },
                    'billing_zip': {
                        required: true,
                    },
                    'billing_country': {
                        required: true,
                    },
                    'shipping_first_name': {
                        required: true,
                    },
                    'shipping_last_name': {
                        required: true,
                    },
                    'shipping_email': {
                        required: true,
                        email: true,
                    },
                    'shipping_company': {
                        required: true,
                    },
                    'shipping_phone': {
                        required: true,
                    },
                    'shipping_address': {
                        required: true,
                    },
                    'shipping_city': {
                        required: true,
                    },
                    'shipping_state': {
                        required: true,
                    },
                    'shipping_zip': {
                        required: true,
                    },
                    'shipping_country': {
                        required: true,
                    },
                },
                messages: {
                    'billing_first_name': {
                        required: 'First name is required.'
                    },
                    'billing_last_name': {
                        required: 'Last name is required.'
                    },
                    'billing_email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.'
                    },
                    'billing_company': {
                        required: 'Company is required.'
                    },
                    'billing_phone': {
                        required: 'Phone is required.',
                    },
                    'billing_address': {
                        required: 'Address is required.'
                    },
                    'billing_city': {
                        required: 'City is required.'
                    },
                    'billing_state': {
                        required: 'State is required.'
                    },
                    'billing_zip': {
                        required: 'Zip is required.'
                    },
                    'billing_country': {
                        required: 'Country is required.'
                    },
                    'shipping_first_name': {
                        required: 'First name is required.'
                    },
                    'shipping_last_name': {
                        required: 'Last name is required.'
                    },
                    'shipping_email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.'
                    },
                    'shipping_company': {
                        required: 'Company is required.'
                    },
                    'shipping_phone': {
                        required: 'Phone is required.',
                    },
                    'shipping_address': {
                        required: 'Address is required.'
                    },
                    'shipping_city': {
                        required: 'City is required.'
                    },
                    'shipping_state': {
                        required: 'State is required.'
                    },
                    'shipping_zip': {
                        required: 'Zip is required.'
                    },
                    'shipping_country': {
                        required: 'Country is required.'
                    },
                }
            });
        },

        billing_detail_events: function () {
            /* state select */
            uiBillingStateSelect.select2({width: '100%'}).on('change', function () {
                var uiThis = $(this);
                var iStateId = uiThis.val();
                if (uiThis.val() != '') {
                    uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                    uiThis.closest('.form-group').removeClass('has-success has-error');
                    uiThis.closest('.form-group').find('.help-block').remove();
                }
            });

            /* checkout select */
            uiBillingCountrySelect.select2({width: '100%'}).on('change', function () {
                var uiThis = $(this);
                var iCountryId = uiThis.find('option:selected').attr('data-country-id');
                if (platform.var_check(iCountryId)) {
                    uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                    uiThis.closest('.form-group').removeClass('has-success has-error');
                    uiThis.closest('.form-group').find('.help-block').remove();

                    uiBillingStateSelect.html('<option value=""></option>');
                    var sStates = uiThis.find('option[data-country-id="' + iCountryId + '"]').attr('data-states');
                    var oStates = platform.parse_json(sStates);
                    for (var x in oStates) {
                        var state = oStates[x];

                        var data = {
                            id: state.id,
                            code: state.code,
                            text: state.name,
                            tax: state.tax
                        };

                        var uiNewOption = '<option value="' + data.text + '" ' +
                            'data-state-name="' + data.text + '" ' +
                            'data-state-id="' + data.id + '" ' +
                            'data-state-tax="' + data.tax + '" ' +
                            'data-country-id="' + iCountryId + '" ' +
                            '>' + data.text + '</option>';
                        uiBillingStateSelect.append(uiNewOption);
                    }

                    uiBillingStateSelect.trigger('change');
                    uiBillingStateSelect.data('select2').destroy();
                    uiBillingStateSelect.select2({width: '100%'});
                    // if (!uiBillingStateSelect.hasClass('loaded')) {
                        uiBillingStateSelect.val(uiBillingStateSelect.attr('data-old-value'));
                        uiBillingStateSelect.trigger('change');
                        // uiBillingStateSelect.addClass('loaded');
                    // }
                }
            });

            if (!uiBillingCountrySelect.find('option[selected]').length) {
                uiBillingCountrySelect.val(uiBillingCountrySelect.find('option[data-is-default="1"]').text());
            }
            uiBillingCountrySelect.trigger('change');
        },

        shipping_detail_events: function () {
            /* state select */
            uiShippingStateSelect.select2({width: '100%'}).on('change', function () {
                var uiThis = $(this);
                var iStateId = uiThis.val();
                if (uiThis.val() != '') {
                    uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                    uiThis.closest('.form-group').removeClass('has-success has-error');
                    uiThis.closest('.form-group').find('.help-block').remove();
                }
            });

            uiShippingCountrySelect.select2({width: '100%'}).on('change', function () {
                var uiThis = $(this);
                var iCountryId = uiThis.find('option:selected').attr('data-country-id');
                if (platform.var_check(iCountryId)) {
                    uiThis.closest('.form-group').find('.select2-container').removeClass('has-success has-error');
                    uiThis.closest('.form-group').removeClass('has-success has-error');
                    uiThis.closest('.form-group').find('.help-block').remove();

                    uiShippingStateSelect.html('<option value=""></option>');
                    var sStates = uiThis.find('option[data-country-id="' + iCountryId + '"]').attr('data-states');
                    var oStates = platform.parse_json(sStates);
                    for (var x in oStates) {
                        var state = oStates[x];

                        var data = {
                            id: state.id,
                            code: state.code,
                            text: state.name,
                            tax: state.tax
                        };

                        var uiNewOption = '<option value="' + data.text + '" ' +
                            'data-state-name="' + data.text + '" ' +
                            'data-state-id="' + data.id + '" ' +
                            'data-state-tax="' + data.tax + '" ' +
                            'data-country-id="' + iCountryId + '" ' +
                            '>' + data.text + '</option>';
                        uiShippingStateSelect.append(uiNewOption);
                    }

                    uiShippingStateSelect.trigger('change');
                    uiShippingStateSelect.data('select2').destroy();
                    uiShippingStateSelect.select2({width: '100%'});
                    // if (!uiShippingStateSelect.hasClass('loaded')) {
                        uiShippingStateSelect.val(uiShippingStateSelect.attr('data-old-value'));
                        uiShippingStateSelect.trigger('change');
                        // uiShippingStateSelect.addClass('loaded');
                    // }
                }
            });

            if (!uiShippingCountrySelect.find('option[selected]').length) {
                uiShippingCountrySelect.val(uiShippingCountrySelect.find('option[data-is-default="1"]').text());
            }
            uiShippingCountrySelect.trigger('change');
        },
    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_address.initialize();
});