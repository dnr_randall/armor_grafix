(function () {
    "use strict";
    /* declare global variables within the class */
    var uiChangePasswordRadio,
        uiChangePasswordContainer,
        uiEditUserForm,
        filler;

    /* private ajax function that will send request to backend */
    function _ajax(oParams, fnCallback, uiBtn) {
        if (platform.var_check(oParams)) {
            var oAjaxConfig = {
                "type": oParams.type,
                "data": oParams.data,
                "url": oParams.url,
                "token": platform.config('csrf.token'),
                "beforeSend": function () {
                    /* check if there is a button to add spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, true);
                    }
                },
                "success": function (oData) {
                    console.log(oData);
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(oData);
                    }
                },
                "complete": function () {
                    /* check if there is a button to remove spinner */
                    if (platform.var_check(uiBtn)) {
                        platform.show_spinner(uiBtn, false);
                    }
                }
            };

            platform.CentralAjax.ajax(oAjaxConfig);
        }
    }

    /*
     * This js file will only contain front_user events
     *
     * */
    CPlatform.prototype.front_user = {

        initialize: function () {
            /* assign a value to the global variable within this class */
            uiChangePasswordRadio = $('#change_password');
            uiChangePasswordContainer = $('.change-pass-container');
            uiEditUserForm = $('#edit-front_user');

            /*front_user change password*/
            uiChangePasswordRadio.on('change', function (e) {
                if ($(this).is(':checked')) {
                    uiChangePasswordContainer.slideDown();
                    uiChangePasswordContainer.find('input').removeAttr('disabled').removeClass('johnCena');
                } else {
                    uiChangePasswordContainer.slideUp();
                    uiChangePasswordContainer.find('input').attr('disabled', 'disabled').addClass('johnCena');
                }
            });

            /* edit front_user validation */
            uiEditUserForm.validate({
                errorClass: 'help-block animation-slideDown',
                errorElement: 'span',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.form-group').find('.help-block').remove();
                },
                success: function (e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.form-group').find('.help-block').remove();
                },
                submitHandler: function (form) {
                    platform.show_spinner($(form).find('[type="submit"]'), true);
                    form.submit();
                },
                rules: {
                    'first_name': {
                        required: true,
                    },
                    'last_name': {
                        required: true,
                    },
                    // 'user_name': {
                    //     required: true,
                    // },
                    'email': {
                        required: true,
                        email: true,
                    },
                    'old_password': {
                        required: true,
                        minlength: 6
                    },
                    'password': {
                        required: {
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        },
                        minlength: {
                            param: 6,
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        }
                    },
                    'password_confirmation': {
                        equalTo: {
                            param: uiEditUserForm.find('#password'),
                            depends: function () {
                                return uiChangePasswordRadio.is(':checked');
                            }
                        },
                    },
                },
                messages: {
                    'first_name': {
                        required: 'Firstname is required.',
                    },
                    'last_name': {
                        required: 'Lastname is required.',
                    },
                    // 'user_name': {
                    //     required: 'Username is required.',
                    // },
                    'email': {
                        required: 'Email is required.',
                        email: 'Please enter a valid email address.',
                    },
                    'old_password': {
                        required: 'Old Password is required.',
                        minlength: 'Your password must be at least 6 characters long.'
                    },
                    'password': {
                        required: 'Password is required.',
                        minlength: 'Your password must be at least 6 characters long.'
                    },
                    'password_confirmation': {
                        equalTo: 'Please enter the same password as above.'
                    },
                }
            });
        },

    }

}());

/* run initialize function on load of window */
$(window).on('load', function () {
    platform.front_user.initialize();
});