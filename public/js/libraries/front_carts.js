$(window).load(function() {
    $('#loading_land').attr('hidden', true);
    (function() {
        "use strict";
        /* declare global variables within the class */
        var uiAddProductToCart,
            uiAddCustomDesignToCart,
            uiEditProductFromCart,
            uiQuantitySelect,
            uiPriceInput,
            uiShapeRadio,
            uiSizeRadio,
            uiQtyRadio,
            uiDesignRadio,
            uiMaterialRadio,
            uiIncludeTamperEvidentCheckbox,
            uiBackToParentShapeBtn,
            uiNextStpBtn,
            uiPrevStpBtn,
            uiCheckoutBtn,
            uiAddToCartAndDesignAnotherLabelBtn,
            uiAddToCartType,
            uiValidateCouponCode,
            filler;

        /* private ajax function that will send request to backend */
        function _ajax(oParams, fnCallback, uiBtn) {
            if (platform.var_check(oParams)) {
                var oAjaxConfig = {
                    "type": oParams.type,
                    "data": oParams.data,
                    "url": oParams.url,
                    "token": platform.config('csrf.token'),
                    "beforeSend": function() {
                        /* check if there is a button to add spinner */
                        if (platform.var_check(uiBtn)) {
                            platform.show_spinner(uiBtn, true);
                        }
                    },
                    "success": function(oData) {
                        if (typeof(fnCallback) == 'function') {
                            fnCallback(oData);
                        }
                    },
                    "complete": function() {
                        /* check if there is a button to remove spinner */
                        if (platform.var_check(uiBtn)) {
                            platform.show_spinner(uiBtn, false);
                        }
                    }
                };

                platform.CentralAjax.ajax(oAjaxConfig);
            }
        }

        /*
         * This js file will only contain front_cart events
         *
         * */
        CPlatform.prototype.front_cart = {

                initialize: function() {
                    /* assign a value to the global variable within this class */
                    uiAddProductToCart = $('#add-product-to-cart');
                    uiAddCustomDesignToCart = $('#add-custom-design-to-cart');
                    uiEditProductFromCart = $('.edit-product-from-cart');
                    uiQuantitySelect = $('.quantity-select');
                    uiPriceInput = $('[name="price"]');
                    uiShapeRadio = $('[name="shape_id"][type="radio"]');
                    uiSizeRadio = $('[name="size_id"][type="radio"]');
                    uiQtyRadio = $('#qty_step2');
                    uiDesignRadio = $('[name="design_id"][type="radio"]');
                    uiMaterialRadio = $('[name="material_id"][type="radio"]');
                    uiIncludeTamperEvidentCheckbox = $('[name="include_tamper_evident"]');
                    uiBackToParentShapeBtn = $('.back-to-parent-shape');
                    uiNextStpBtn = $('.next-step-btn');
                    uiPrevStpBtn = $('.prev-step-btn');
                    uiCheckoutBtn = $('.checkout-btn');
                    uiAddToCartAndDesignAnotherLabelBtn = $('.add-to-cart-and-design-another-label-btn');
                    uiAddToCartType = $('[name="add_to_cart_type"]');
                    uiValidateCouponCode = $('#validate-coupon-code');

                    platform.front_cart.product_page_events();
                    platform.front_cart.custom_design_label_page_events();
                    platform.front_cart.cart_page_events();
                },

                product_page_events: function() {
                    uiQuantitySelect.on('change', function() {
                        var uiThis = $(this);
                        if (uiThis.val() != '') {
                            uiPriceInput.val(uiThis.find('option:selected').attr('data-price'));
                        } else {
                            uiPriceInput.val(0);
                        }
                    });

                    uiAddProductToCart.validate({
                        errorClass: 'help-block animation-slideDown',
                        errorElement: 'span',
                        errorPlacement: function(error, e) {
                            e.parents('.form-group').append(error);
                        },
                        highlight: function(e) {
                            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                            $(e).closest('.form-group').find('.help-block').remove();
                        },
                        success: function(e) {
                            e.closest('.form-group').removeClass('has-success has-error');
                            e.closest('.form-group').find('.help-block').remove();
                        },
                        submitHandler: function(form) {
                            platform.show_spinner($(form).find('[type="submit"]'), true);
                            form.submit();
                        },
                        rules: {
                            'quantity_id': {
                                required: true
                            },
                        },
                        messages: {
                            'quantity_id': {
                                required: 'Quantity is required.'
                            },
                        }
                    });
                },

                custom_design_label_page_events: function() {
                    uiCheckoutBtn.on('click', function(e) {
                        e.preventDefault();
                        platform.show_spinner($(this), true);
                        uiAddToCartAndDesignAnotherLabelBtn.prop('disabled', true);
                        uiAddToCartAndDesignAnotherLabelBtn.attr('disabled', 'disabled');
                        uiAddToCartAndDesignAnotherLabelBtn.css('pointer-events', 'none');
                        uiAddToCartType.val('checkout');
                        uiAddCustomDesignToCart.submit();
                    });

                    uiAddToCartAndDesignAnotherLabelBtn.on('click', function(e) {
                        e.preventDefault();
                        platform.show_spinner($(this), true);
                        uiCheckoutBtn.prop('disabled', true);
                        uiCheckoutBtn.attr('disabled', 'disabled');
                        uiCheckoutBtn.css('pointer-events', 'none');
                        uiAddToCartType.val('another');
                        uiAddCustomDesignToCart.submit();
                    });

                    uiShapeRadio.on('change', function() {
                        var uiThis = $(this);
                        var iVal = uiThis.val();

                        uiSizeRadio.prop('checked', false);
                        uiQtyRadio.prop('checked', false);
                        uiDesignRadio.prop('checked', false);
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.update_size_quantity_step_details();
                        platform.front_cart.update_design_step_details();

                        if (uiThis.parents('.shape-option:first').attr('data-shape-sub-shape-count') > 0) {
                            uiShapeRadio.prop('checked', false);
                            $('.shape-option[data-shape-parent-id]').hide();
                            if ($('.shape-option[data-shape-parent-id="' + iVal + '"]').length) {
                                $('.shape-option[data-shape-parent-id="' + iVal + '"]').show();
                                uiBackToParentShapeBtn.show();
                            } else {
                                uiBackToParentShapeBtn.hide();
                            }
                        }
                        platform.front_cart.validate_steps(1);
                    });

                    uiBackToParentShapeBtn.on('click', function() {
                        uiBackToParentShapeBtn.hide();
                        $('.shape-option[data-shape-parent-id]').hide();
                        $('.shape-option[data-shape-parent-id="0"]').show();
                        uiShapeRadio.prop('checked', false);
                        uiSizeRadio.prop('checked', false);
                        uiQtyRadio.prop('checked', false);
                        uiDesignRadio.prop('checked', false);
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.update_size_quantity_step_details();
                        platform.front_cart.update_design_step_details();
                        platform.front_cart.validate_steps(1);
                    });

                    uiNextStpBtn.on('click', function(e) {
                        e.preventDefault();
                        var iStep = $('.js-tabs').find('li.active').length;
                        platform.front_cart.validate_steps(iStep, function(bStatus) {
                            if (bStatus) {
                                platform.front_cart.update_tabs(iStep, 'next');
                            }
                        });
                    });

                    uiPrevStpBtn.on('click', function(e) {
                        e.preventDefault();
                        var iStep = $('.js-tabs').find('li.active').length;
                        platform.front_cart.update_tabs(iStep, 'prev');
                    });

                    platform.front_cart.update_tabs(1);
                    platform.front_cart.validate_steps(1);

                    uiSizeRadio.on('change', function() {
                        uiQtyRadio.prop('checked', false);
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.update_size_quantity_step_details();
                        platform.front_cart.validate_steps(2);
                    });

                    uiQtyRadio.on('change', function() {
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.update_size_quantity_step_details();
                        platform.front_cart.validate_steps(2);
                    });

                    uiDesignRadio.on('change', function() {
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.update_design_step_details();
                        platform.front_cart.validate_steps(3);
                    });

                    uiMaterialRadio.on('change', function() {
                        platform.front_cart.update_design_label_details();
                        platform.front_cart.validate_steps(4);
                    });

                    uiIncludeTamperEvidentCheckbox.on('change', function() {
                        platform.front_cart.update_design_label_details();
                    });

                    $('[data-toggle="tooltip"]').each(function() {
                        $(this).tooltip({
                            container: 'body',
                            placement: 'top',
                            animation: true,
                            trigger: 'hover',
                            content: function() {
                                return $(this).prop('title');
                            }
                        });
                    });
                },

                cart_page_events: function() {
                    uiEditProductFromCart.each(function() {
                        var uiThis = $(this);
                        uiThis.validate({
                            errorClass: 'help-block animation-slideDown',
                            errorElement: 'span',
                            errorPlacement: function(error, e) {
                                e.parents('.form-group').append(error);
                            },
                            highlight: function(e) {
                                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                                $(e).closest('.form-group').find('.help-block').remove();
                            },
                            success: function(e) {
                                e.closest('.form-group').removeClass('has-success has-error');
                                e.closest('.form-group').find('.help-block').remove();
                            },
                            submitHandler: function(form) {
                                $(form).find('[type="submit"]').find('i').addClass('fa-spin');
                                $(form).find('[type="submit"]').prop('disabled', true);
                                $(form).find('[type="submit"]').attr('disabled', 'disabled');
                                $(form).find('[type="submit"]').css('pointer-events', 'none');
                                // platform.show_spinner($(form).find('[type="submit"]'), true);
                                form.submit();
                            },
                            rules: {
                                'quantity_id': {
                                    required: true
                                },
                            },
                            messages: {
                                'quantity_id': {
                                    required: 'Quantity is required.'
                                },
                            }
                        });
                    });

                    $('body').on('click', '.delete-cart-btn', function(e) {
                        e.preventDefault();
                        var self = $(this);
                        /* open confirmation modal */
                        swal({
                            title: "Are you sure?",
                            text: "Are you sure you want to delete this record?",
                            type: "warning",
                            showCancelButton: true,
                            //confirmButtonColor: "#27ae60",
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true,
                            allowOutsideClick: true
                        }, function(isConfirm) {
                            /* if confirmed, send request ajax */
                            if (isConfirm) {
                                var oParams = {
                                    'data': { 'id': self.attr('data-cart-id') },
                                    'url': self.attr('data-cart-route')
                                };
                                platform.delete.delete(oParams, function(oData) {
                                    /* check return of ajax */
                                    if (platform.var_check(oData)) {
                                        /* check status if success */
                                        if (oData.status > 0) {
                                            /* if status is true, render success messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Deleted!",
                                                        'text': message,
                                                        'type': "success",
                                                        'showCancelButton': false,
                                                        'showConfirmButton': true,
                                                        'allowOutsideClick': false
                                                            //'confirmButtonColor': "#DD6B55",
                                                    }, function() {
                                                        /* remove cart container */
                                                    });
                                                    swal.disableButtons();
                                                    location.reload();
                                                }
                                            }
                                        } else {
                                            /* if status is false, render error messages */
                                            if (platform.var_check(oData.message)) {
                                                for (var x in oData.message) {
                                                    var message = oData.message[x];
                                                    swal({
                                                        'title': "Error!",
                                                        'text': message,
                                                        'type': "error"
                                                            //'confirmButtonColor': "#DD6B55",
                                                    }, function() {

                                                    });
                                                }
                                            }
                                        }
                                    }
                                }, self);
                            }
                        });
                    });

                    uiValidateCouponCode.validate({
                        errorClass: 'help-block animation-slideDown',
                        errorElement: 'span',
                        errorPlacement: function(error, e) {
                            e.parents('.form-group').append(error);
                        },
                        highlight: function(e) {
                            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                            $(e).closest('.form-group').find('.help-block').remove();
                        },
                        success: function(e) {
                            e.closest('.form-group').removeClass('has-success has-error');
                            e.closest('.form-group').find('.help-block').remove();
                        },
                        submitHandler: function(form) {
                            platform.show_spinner($(form).find('[type="submit"]'), true);
                            form.submit();
                        },
                        rules: {
                            'coupon_code': {
                                required: true
                            },
                        },
                        messages: {
                            'coupon_code': {
                                required: 'Coupon Code is required.'
                            },
                        }
                    });
                },

                validate_steps: function(iStep, fnCallback) {
                    if (iStep == 1) {
                        platform.front_cart.validate_step_1(function(sErrorMsg, bStatus) {
                            if (!bStatus) {
                                uiNextStpBtn.addClass('disabled');
                            } else {
                                uiNextStpBtn.removeClass('thisisstep2');
                                uiNextStpBtn.removeClass('disabled');
                            }

                            if (typeof(fnCallback) == 'function') {
                                fnCallback(bStatus);
                            }
                        });
                    } else if (iStep == 2) {
                        platform.front_cart.validate_step_2(function(sErrorMsg, bStatus) {
                            if (!bStatus) {
                                uiNextStpBtn.addClass('disabled');
                                uiNextStpBtn.addClass('thisisstep2');
                            } else {
                                uiNextStpBtn.removeClass('disabled');
                            }

                            if (typeof(fnCallback) == 'function') {
                                fnCallback(bStatus);
                            }
                        });

                    } else if (iStep == 3) {
                        platform.front_cart.validate_step_3(function(sErrorMsg, bStatus) {
                            if (!bStatus) {
                                uiNextStpBtn.addClass('disabled');
                            } else {
                                uiNextStpBtn.removeClass('disabled');
                                // uiNextStpBtn.removeClass('thisisstep2');
                            }

                            if (typeof(fnCallback) == 'function') {
                                fnCallback(bStatus);
                            }
                        });
                    } else if (iStep == 4) {
                        platform.front_cart.validate_step_4(function(sErrorMsg, bStatus) {
                            if (!bStatus) {
                                uiCheckoutBtn.addClass('disabled');
                                uiAddToCartAndDesignAnotherLabelBtn.addClass('disabled');
                            } else {
                                uiCheckoutBtn.removeClass('disabled');
                                uiAddToCartAndDesignAnotherLabelBtn.removeClass('disabled');
                            }

                            if (typeof(fnCallback) == 'function') {
                                fnCallback(bStatus);
                            }
                        });
                    }
                },

                update_tabs: function(iStep, sDirection) {
                    /* get new step by direction */
                    if (sDirection == 'next') {
                        iStep++;
                    } else if (sDirection == 'prev') {
                        iStep--;
                    }

                    /* prevent moving out of 1 to 4 steps */
                    if (iStep < 1 || iStep > 4) {
                        return false;
                    }

                    /* for hiding of all tabs */
                    $('.js-tabs').find('li').removeClass('active');
                    $('.steps').hide();

                    /* for showing of active tabs */
                    for (var i = 0; i < iStep; i++) {
                        $('.js-tab-' + (i + 1)).addClass("active");
                    }
                    $('.steps[data-step="' + iStep + '"]').show();

                    /* for special shape back buttons and add to cart buttons */
                    uiNextStpBtn.show();
                    uiCheckoutBtn.hide();
                    uiAddToCartAndDesignAnotherLabelBtn.hide();
                    if (iStep == 1) {
                        uiPrevStpBtn.hide();
                        if ($('.shape-option:visible').attr('data-shape-parent-id') > 0) {
                            uiBackToParentShapeBtn.show();
                        }
                    } else if (iStep == 4) {
                        uiNextStpBtn.hide();
                        uiCheckoutBtn.show();
                        uiAddToCartAndDesignAnotherLabelBtn.show();
                    } else {
                        uiBackToParentShapeBtn.hide();
                        uiPrevStpBtn.show();
                    }

                    platform.front_cart.validate_steps(iStep);
                },

                update_design_label_details: function(fnCallback) {
                    var sShapeName = '';
                    if ($('[name="shape_id"]:checked').length) {
                        sShapeName = $('[name="shape_id"]:checked').attr('data-shape-name');
                        $('.shape-detail-container').addClass('active');
                    } else {
                        $('.shape-detail-container').removeClass('active');
                    }
                    $('.shape-name').html(sShapeName);

                    var sSizeName = '';
                    if ($('[name="size_id"]:checked').length) {
                        sSizeName = $('[name="size_id"]:checked').attr('data-size-name');
                    }
                    $('.size-name').html(sSizeName);

                    var sQuantityName = '';
                    if ($('[name="quantity_id"]:checked').length) {
                        sQuantityName = $('[name="quantity_id"]:checked').attr('data-quantity-name');
                        uiPriceInput.val($('[name="quantity_id"]:checked').attr('data-price'));
                    } else {
                        uiPriceInput.val(0);
                    }
                    $('.quantity-name').html(sQuantityName);

                    if ($('[name="size_id"]:checked').length && $('#qty_step2').val().length) {
                        $('.size-detail-container').addClass('active');
                    } else {
                        $('.size-detail-container').removeClass('active');
                    }

                    var sDesignName = '';
                    if ($('[name="design_id"]:checked').length) {
                        sDesignName = $('[name="design_id"]:checked').attr('data-design-name');
                        $('.design-detail-container').addClass('active');
                    } else {
                        $('.design-detail-container').removeClass('active');
                    }
                    $('.design-name').html(sDesignName);

                    var sMaterialName = '';
                    if ($('[name="material_id"]:checked').length) {
                        sMaterialName = $('[name="material_id"]:checked').attr('data-material-name');
                        $('.material-detail-container').addClass('active');
                    } else {
                        $('.material-detail-container').removeClass('active');
                    }
                    $('.material-name').html(sMaterialName);

                    var sIncludeTamperEvident = '';
                    if ($('[name="include_tamper_evident"]:checked').length) {
                        sIncludeTamperEvident = 'Include Tamper Evident';
                    }
                    $('.include-tamper-evident').html(sIncludeTamperEvident);

                    if (typeof(fnCallback) == 'function') {
                        fnCallback();
                    }
                },

                update_size_quantity_step_details: function(fnCallback) {
                    var sShapeImage = '';
                    var iShapeId = 0;
                    $('.size-option').hide();
                    if ($('[name="shape_id"]:checked').length) {
                        iShapeId = $('[name="shape_id"]:checked').val();
                        sShapeImage = $('[name="shape_id"]:checked').attr('data-shape-image');
                        $('.size-option[data-shape-id="' + iShapeId + '"]').show();
                        $('.shape-image').attr('src', sShapeImage);
                    }

                    var iSizeId = 0;
                    if ($('[name="size_id"]:checked').length) {
                        iSizeId = $('[name="size_id"]:checked').val();
                        $('.quantity-option[data-size-id="' + iSizeId + '"]').show();
                    }

                    if (typeof(fnCallback) == 'function') {
                        fnCallback();
                    }
                },

                update_design_step_details: function(fnCallback) {
                    var iShapeId = 0;
                    $('.design-option').hide();
                    if ($('[name="shape_id"]:checked').length) {
                        iShapeId = $('[name="shape_id"]:checked').val();
                        $('.design-option[data-shape-id="' + iShapeId + '"]').show();
                    }

                    if (typeof(fnCallback) == 'function') {
                        fnCallback();
                    }
                },

                validate_step_1: function(fnCallback) {
                    var sErrorMsg = '';
                    var bStatus = true;
                    if ($('[name="shape_id"]:checked').length == 0) {
                        sErrorMsg = 'Shape is required.';
                        bStatus = false;
                    }

                    if (typeof(fnCallback) == 'function') {
                        fnCallback(sErrorMsg, bStatus);
                    }
                },

                validate_step_2: function(fnCallback) {
                    var sErrorMsg = '';
                    var bStatus = true;
                    if ($('[name="size_id"]:checked').length == 0 || !$("#qty_step2").val()) {
                        sErrorMsg = 'Size and Quantity is required.';
                        bStatus = false;
                    }
                    if (typeof(fnCallback) == 'function') {
                        fnCallback(sErrorMsg, bStatus);
                    }
                },

                validate_step_3: function(fnCallback) {
                    var sErrorMsg = '';
                    var bStatus = true;
                    if ($('[name="design_id"]:checked').length == 0) {
                        sErrorMsg = 'Design is required.';
                        bStatus = false;
                    }

                    if (typeof(fnCallback) == 'function') {
                        fnCallback(sErrorMsg, bStatus);
                    }
                },

                validate_step_4: function(fnCallback) {
                    var sErrorMsg = '';
                    var bStatus = true;
                    if ($('[name="material_id"]:checked').length == 0) {
                        sErrorMsg = 'Material is required.';
                        bStatus = false;
                    }

                    if (typeof(fnCallback) == 'function') {
                        fnCallback(sErrorMsg, bStatus);
                    }
                },
            }
            /* run initialize function on load of window */
        platform.front_cart.initialize();

        var prices = [];
        $("body").on("input", "#qty_step2", function(e) {
            for (let i = 0; i < prices.length; i++) {
                if (prices[i].name <= $(this).val()) {
                    $("#price_label").html((prices[i].price * $(this).val()).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","))


                    if (prices[i].label != 0) {
                        $("#label_roll").html(`(` + prices[i].label + ` Labels / Roll)`);
                    } else {
                        $("#label_roll").html('');
                    }

                }

            }
        });
        $("body").on("input", ".size_id", function(e) {

            // alert($("#qty_step2").val() + ', ' + $('[name="size_id"]:checked').val() + ', ' + $('[name="_token"]').val());

            $.ajax({
                type: "POST",
                data: {
                    quantity_id: $("#qty_step2").val(),
                    size_id: $('[name="size_id"]:checked').val(),
                    _token: $('input[name="_token"]').val(),
                },
                url: "price-check",
                beforeSend: function() {
                    $(".sec-bulk-prices").html('Loading..')
                    $("#price_label").html('')
                    $(".next-step-btn").attr('hidden', true);

                },
                success: function(data) {
                    var prices_text = ``;
                    prices = data.prices;
                    for (let i = 0; i < data.prices.length; i++) {
                        var price = data.prices[i]['price'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        prices_text += `<span>$` + price + ` per roll/sheet if you purchase ` + data.prices[i]['name'] + `+</span>`
                    }

                    $("#qty_step2").val(1);

                    // console.log(data.prices[0]);
                    // alert(data.prices[0]);

                    if (typeof data.prices[0] === 'undefined') {

                        $(".sec-bulk-prices").html('Not available at this moment.');
                        $("#price_label").html('');
                        $('.thisisstep2').addClass('disabled');
                        $("#label_roll").html('');
                        $(".next-step-btn").attr('hidden', false);

                    } else {
                        $(".sec-bulk-prices").html(`
                        <p><strong>Save by purchasing in bulk.</strong></p>` +
                            prices_text
                        )
                        $("#price_label").html(data.prices[0].price)
                        $('.thisisstep2').removeClass('disabled');
                        $(".next-step-btn").attr('hidden', false);

                    }

                    if (data.prices[0].label != 0) {
                        $("#label_roll").html(`(` + data.prices[0].label + ` Labels / Roll)`);
                    } else {
                        $("#label_roll").html('');
                    }

                    // $("#label_roll").html(`(` + data.prices[0].label + ` Labels / Roll)`)

                    $(".next-step-btn").removeClass('disabled');
                }
            });

        });


    }());

});