@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><a href="javascript:void(0)"
                                                                                class="{{--page-link--}}"><i
                            class="fa fa-angle-left"></i></a></li>
        @else
            <li class="page-item"><a class="{{--page-link--}}"
                                                                       href="{{ $paginator->previousPageUrl() }}"
                                                                       rel="prev"><i class="fa fa-angle-left"></i></a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><a href="javascript:void(0)"
                                                                   class="{{--page-link--}}"><i>{{ $element }}</i></a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a href="javascript:void(0)"
                                                                         class="{{--page-link--}}"><i>{{ $page }}</i></a></li>
                    @else
                        <li class="page-item"><a class="{{--page-link--}}" href="{{ $url }}"><i>{{ $page }}</i></a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="{{--page-link--}}"
                                                                       href="{{ $paginator->nextPageUrl() }}"
                                                                       rel="next"><i class="fa fa-angle-right"></i></a>
            </li>
        @else
            <li class="page-item disabled"><a href="javascript:void(0)"
                                                                                class="{{--page-link--}}"><i
                            class="fa fa-angle-right"></i></a></li>
        @endif
    </ul>
@endif
