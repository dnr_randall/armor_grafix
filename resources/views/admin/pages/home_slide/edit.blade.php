@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.home_slides.index') }}">Home Slides</a></li>
        <li><span href="javascript:void(0)">Edit Home Slide</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-home_slide',
            'route' => ['admin.home_slides.update', $home_slide->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Home Slide "{{$home_slide->title}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_title">Title</label>

                    <div class="col-md-9">
                    <textarea id="home_slide_title" name="title" rows="9" class="form-control "
                              placeholder="Enter Home Slide title..">{!! Request::old('title') ? : $home_slide->title !!}</textarea>
                        @if($errors->has('title'))
                            <span class="help-block animation-slideDown">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('background_image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="background_image">Background Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Choose File <input type="file" name="background_image" style="display: none;">
                                </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('background_image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('background_image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="{{ asset($home_slide->background_image) }}" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="{{ $home_slide->background_image != '' ? asset($home_slide->background_image) : '' }}"
                                 alt="{{ $home_slide->background_image != '' ? asset($home_slide->background_image) : '' }}"
                                 class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn"
                           style="display: {{ $home_slide->background_image != '' ? '' : 'none' }};"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_background_image" class="remove-image" value="0">
                    </div>
                </div>

                <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_short_description">Short Description</label>

                    <div class="col-md-9">
                    <textarea id="home_slide_short_description" name="short_description" rows="9" class="form-control "
                              placeholder="Enter Home Slide short description..">{!! Request::old('short_description') ? : $home_slide->short_description !!}</textarea>
                        @if($errors->has('short_description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('short_description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('button_label_1') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_button_label_1">Button Label 1</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="home_slide_button_label_1" name="button_label_1"
                               value="{{  Request::old('button_label_1') ? : $home_slide->button_label_1 }}"
                               placeholder="Enter Home Slide button label 1..">
                        @if($errors->has('button_label_1'))
                            <span class="help-block animation-slideDown">{{ $errors->first('button_label_1') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('button_link_1') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_button_link_1">Button Link 1</label>

                    <div class="col-md-9">
                    <textarea id="home_slide_button_link_1" name="button_link_1" rows="9" class="form-control "
                              style="resize: vertical; min-height: 150px;"
                              placeholder="Enter Home Slide button link 1..">{!! Request::old('button_link_1') ? : $home_slide->button_link_1 !!}</textarea>
                        @if($errors->has('button_link_1'))
                            <span class="help-block animation-slideDown">{{ $errors->first('button_link_1') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('button_label_2') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_button_label_2">Button Label 2</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="home_slide_button_label_2" name="button_label_2"
                               value="{{  Request::old('button_label_2') ? : $home_slide->button_label_2 }}"
                               placeholder="Enter Home Slide button label 2..">
                        @if($errors->has('button_label_2'))
                            <span class="help-block animation-slideDown">{{ $errors->first('button_label_2') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('button_link_2') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_button_link_2">Button Link 2</label>

                    <div class="col-md-9">
                    <textarea id="home_slide_button_link_2" name="button_link_2" rows="9" class="form-control "
                              style="resize: vertical; min-height: 150px;"
                              placeholder="Enter Home Slide button link 2..">{!! Request::old('button_link_2') ? : $home_slide->button_link_2 !!}</textarea>
                        @if($errors->has('button_link_2'))
                            <span class="help-block animation-slideDown">{{ $errors->first('button_link_2') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('bottom_short_description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="home_slide_bottom_short_description">Bottom Short Description</label>

                    <div class="col-md-9">
                    <textarea id="home_slide_bottom_short_description" name="bottom_short_description" rows="9" class="form-control "
                              placeholder="Enter Home Slide bottom short description..">{!! Request::old('bottom_short_description') ? : $home_slide->bottom_short_description !!}</textarea>
                        @if($errors->has('bottom_short_description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('bottom_short_description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($home_slide->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.home_slides.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/libraries/home_slides.js') }}"></script>
@endpush