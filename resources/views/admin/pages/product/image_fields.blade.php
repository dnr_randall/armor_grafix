<div class="form-group">
    <label class="col-md-3 control-label">Images</label>

    <div class="col-md-9">
        <div id="upload-image" class="">
            <div class="dz-message" data-dz-message style="margin: 10px 0 !important;">
                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                <div>Drag & drop files here
                    <br/> or
                </div>
                <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
            </div>
            <div class="fallback">
                <input name="file" type="file" multiple/>
            </div>
        </div>
    </div>
</div>