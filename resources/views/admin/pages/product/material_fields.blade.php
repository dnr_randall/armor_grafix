@if ($materials->count())
    <div class="form-group{{ $errors->has('material_id') ? ' has-error' : '' }}">
        <label class="col-md-3 control-label" for="material_id">Material</label>
        <div class="col-md-9">
            <select name="material_id" id="material_id"
                    class="material-select"
                    data-placeholder="Choose material..">
                <option value=""></option>
                @foreach($materials as $material)
                    <option value="{{ $material->id }}" {{ !empty($product) && !empty($product->material) ? ($product->material_id == $material->id ? 'selected' : '') : '' }}>{{ ucwords($material->name) }}</option>
                @endforeach
            </select>
            @if($errors->has('material_id'))
                <span class="help-block animation-slideDown">{{ $errors->first('material_id') }}</span>
            @endif
        </div>
    </div>
@endif
