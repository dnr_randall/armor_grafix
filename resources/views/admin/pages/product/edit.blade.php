@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.products.index') }}">Products</a></li>
        <li><span href="javascript:void(0)">Edit Product</span></li>
    </ul>
    <div class="row">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-product',
            'route' => ['admin.products.update', $product->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Edit Product "{{$product->name}}"</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_name" name="name"
                               value="{{  Request::old('name') ? : $product->name }}"
                               placeholder="Enter Product name..">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('sku') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_sku">SKU</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_sku" name="sku"
                               value="{{  Request::old('sku') ? : $product->sku }}"
                               placeholder="Enter Product sku..">
                        @if($errors->has('sku'))
                            <span class="help-block animation-slideDown">{{ $errors->first('sku') }}</span>
                        @endif
                    </div>
                </div>
                <!-- <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_short_description">Short Description</label>

                    <div class="col-md-9">
                    <textarea id="product_short_description" name="short_description" rows="9" class="form-control "
                              placeholder="Enter Product short description..">{!! Request::old('short_description') ? : $product->short_description !!}</textarea>
                        @if($errors->has('short_description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('short_description') }}</span>
                        @endif
                    </div>
                </div> -->
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_description">Description</label>

                    <div class="col-md-9">
                    <textarea id="product_description" name="description" rows="9" class="form-control ckeditor"
                              placeholder="Enter Product description..">{!! Request::old('description') ? : $product->description !!}</textarea>
                        @if($errors->has('description'))
                            <span class="help-block animation-slideDown">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                @include('admin.pages.product.image_fields')
                @include('admin.pages.product.shape_fields')
                @include('admin.pages.product.material_fields')
                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_price">Price</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_price" name="price"
                               placeholder="Enter Product price.." value="{!! Request::old('price') ? : $product->price !!}">
                        @if($errors->has('price'))
                            <span class="help-block animation-slideDown">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_weight">Weight</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_weight" name="weight"
                               placeholder="Enter Product weight.." value="{!! Request::old('weight') ? : $product->weight !!}">
                        @if($errors->has('weight'))
                            <span class="help-block animation-slideDown">{{ $errors->first('weight') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('dimension') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="product_dimension">Dimension</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="product_dimension" name="dimension"
                               placeholder="Enter Product dimension.." value="{!! Request::old('dimension') ? : $product->dimension !!}">
                        @if($errors->has('dimension'))
                            <span class="help-block animation-slideDown">{{ $errors->first('dimension') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Featured?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_featured" name="is_featured"
                                   value="1" {{ Request::old('is_featured') ? : ($product->is_featured ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Is Active?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="1" {{ Request::old('is_active') ? : ($product->is_active ? 'checked' : '') }}>
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.products.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
<script>
    @if (!empty($product->product_images))
        var sProductImages = '{!! $product->product_images()->get()->toJson() !!}';
        console.log(sProductImages)
    @endif
</script>
<script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/libraries/products.js') }}"></script>
@endpush