@if ($shapes->count())
    <div class="form-group{{ $errors->has('shape_id') ? ' has-error' : '' }}">
        <label class="col-md-3 control-label" for="shape_id">Shape</label>
        <div class="col-md-9">
            <select name="shape_id" id="shape_id"
                    class="shape-select"
                    data-placeholder="Choose shape..">
                <option value=""></option>
                @foreach($shapes as $shape)
                    @if ($shape->children()->count())
                        <optgroup label="{{ $shape->name }}">
                            @foreach($shape->children()->get() as $sub_shape)
                                <option value="{{ $sub_shape->id }}"
                                        data-shape-id="{{ $sub_shape->id }}"
                                        data-shape-name="{{ $sub_shape->name }}"
                                        data-sizes="{{ json_encode($sub_shape->sizes()->get()) }}"
                                        {{ !empty($product) && !empty($product->shape) ? ($product->shape_id == $sub_shape->id ? 'selected' : '') : '' }}
                                >{{ ucwords($sub_shape->name) }}</option>
                            @endforeach
                        </optgroup>
                    @else
                        <option value="{{ $shape->id }}"
                                data-shape-id="{{ $shape->id }}"
                                data-shape-name="{{ $shape->name }}"
                                data-sizes="{{ json_encode($shape->sizes()->get()) }}"
                                {{ !empty($product) && !empty($product->shape) ? ($product->shape_id == $shape->id ? 'selected' : '') : '' }}
                        >{{ ucwords($shape->name) }}</option>
                    @endif
                @endforeach
            </select>
            @if($errors->has('shape_id'))
                <span class="help-block animation-slideDown">{{ $errors->first('shape_id') }}</span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('size_id') ? ' has-error' : '' }}">
        <label class="col-md-3 control-label" for="size_id">Size</label>
        <div class="col-md-9">
            <select name="size_id" id="size_id"
                    class="size-select"
                    data-placeholder="Choose size.." data-size-id="{{ !empty($product) ? $product->size_id : 0 }}">
                <option value=""></option>
            </select>
            @if($errors->has('size_id'))
                <span class="help-block animation-slideDown">{{ $errors->first('size_id') }}</span>
            @endif
        </div>
    </div>
@endif
