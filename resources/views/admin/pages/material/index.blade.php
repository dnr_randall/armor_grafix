@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Material'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.materials.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Material
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-cubes sidebar-nav-icon"></i>
                <strong>Materials</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable material-empty {{$materials->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Materials found.
        </div>
        <div class="table-responsive {{$materials->count() == 0 ? 'johnCena' : '' }}">
            <table id="materials-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Is active
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($materials as $material)
                    <tr data-material-id="{{$material->id}}">
                        <td class="text-center"><strong>{{ $material->id }}</strong></td>
                        <td class="text-center"><strong>{{ $material->name }}</strong></td>
                        <td class="text-center">{{ $material->created_at->format('F d, Y') }}</td>
                        <td class="text-center"><strong>{{ $material->is_active == 1 ? 'Active' : 'Not Active' }}</strong></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Material'))
                                    <a href="{{ route('admin.materials.edit', $material->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Material'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-material-btn"
                                       data-original-title="Delete"
                                       data-material-id="{{ $material->id }}"
                                       data-material-route="{{ route('admin.materials.delete', $material->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/materials.js') }}"></script>
@endpush