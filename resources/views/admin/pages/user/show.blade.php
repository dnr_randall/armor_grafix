@extends('admin.layouts.base')

@section('content')
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ route('admin.users.index') }}">Users</a></li>
        <li><span href="javascript:void(0)">View User</span></li>
    </ul>

    <div class="row">
        <div class="{{ $user->hasRole('Customer') ? 'col-lg-5' : 'col-lg-12' }}">
            <!-- Customer Info Block -->
            <div class="block">
                <!-- Customer Info Title -->
                <div class="block-title">
                    <h2><i class="fa fa-user"></i> <strong>User</strong> Info</h2>
                    @if (auth()->user()->can('Update User'))
                        <div class="block-options pull-right">
                            <a href="{{ route('admin.users.edit', $user->id) }}" class="label label-default"><strong><i
                                            class="fa fa-pencil"></i> Edit</strong></a>
                        </div>
                    @endif
                </div>
                <!-- END Customer Info Title -->

                <!-- Customer Info -->
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                    </a>
                    <img src="{{ asset('public/images/placeholders/avatars/avatar4@2x.jpg') }}" alt="avatar"
                         class="img-circle">
                    <h3>
                        <strong>{{ $user->first_name.' '.$user->last_name }}</strong><br>
                        <small></small>
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                    <tr>
                        <td class="text-right" style="width: 50%;"><strong>Roles</strong></td>
                        <td>{{ $user->roles()->pluck('name')->implode(', ') }}</td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td class="text-right"><strong>Username</strong></td>--}}
                    {{--<td>{{ $user->user_name }}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td class="text-right"><strong>Email</strong></td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Creation Date</strong></td>
                        <td>{{ $user->created_at->format('F d, Y h:i:s a') }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Last Visit</strong></td>
                        <td>{{ date('F d, Y h:i:s a', strtotime($user->last_login)) }}</td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Status</strong></td>
                        <td>
                            @if ($user->is_active)
                                <span class="label label-success"><i class="fa fa-check"></i> Active</span>
                            @else
                                <span class="label label-danger"><i class="fa fa-times"></i> Inactive</span>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!-- END Customer Info -->
            </div>
            <!-- END Customer Info Block -->
        </div>
        @if ($user->hasRole('Customer'))
            <div class="col-lg-7">
                <!-- Orders Block -->
                <div class="block">
                    <!-- Orders Title -->
                    <div class="block-title">
                        {{--<div class="block-options pull-right">--}}
                        {{--<span class="label label-success"><strong>$ 2125,00</strong></span>--}}
                        {{--</div>--}}
                        <h2><i class="fa fa-shopping-cart"></i> <strong>Orders</strong></h2>
                    </div>
                    <!-- END Orders Title -->

                    <!-- Orders Content -->
                    @if($user->orders()->count())
                        <table class="table table-bordered table-striped table-vcenter" id="orders-table">
                            <tbody>
                            @foreach($user->orders()->orderBy('created_at', 'desc')->get() as $order)
                                <tr>
                                    <td class="text-center" style="width: 20%;">
                                        <a href="{{ route('admin.orders.show', $order->id) }}"><strong>{{ $order->reference_no }}</strong></a>
                                    </td>
                                    <td class="hidden-xs">
                                        <a href="javascript:void(0)">{{ $order->items->count() }} Product(s)</a>
                                    </td>
                                    <td class="text-right hidden-xs">
                                        <strong>$ {{ $order->total_amount }}</strong>
                                    </td>
                                    <td>{{ $order->status->name }}</td>
                                    <td class="hidden-xs text-center">{{ $order->created_at->format('F d, Y h:i A') }}</td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs">
                                            <a href="{{ route('admin.orders.show', $order->id) }}" data-toggle="tooltip"
                                               title=""
                                               class="btn btn-default" data-original-title="View"><i
                                                        class="fa fa-eye"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info alert-dismissable">
                            <i class="fa fa-info-circle"></i> No Orders found.
                        </div>
                    @endif
                <!-- END Orders Content -->
                </div>
                <!-- END Orders Block -->

                <!-- Customer Addresses Block -->
                <div class="block">
                    <!-- Customer Addresses Title -->
                    <div class="block-title">
                        <h2><i class="fa fa-building-o"></i> <strong>Addresses</strong></h2>
                    </div>
                    <!-- END Customer Addresses Title -->

                    <!-- Customer Addresses Content -->
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Billing Address Block -->
                            <div class="block">
                                <!-- Billing Address Title -->
                                <div class="block-title">
                                    <h2><i class="fa fa-credit-card"></i> Billing Address</h2>
                                </div>
                                <!-- END Billing Address Title -->

                                <!-- Billing Address Content -->
                                @if(!empty($user->billing_address))
                                    <table class="table table-borderless table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">To</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->billing_address) ? $user->billing_address->first_name . ' ' . $user->billing_address->last_name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Address</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->billing_address) ? $user->billing_address->address . ' ' . $user->billing_address->address_2 . ' ' . $user->billing_address->city . ' ' . $user->billing_address->state . ' ' . $user->billing_address->zip . ' ' . $user->billing_address->country : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Email</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->billing_address) ? $user->billing_address->email : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Company</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->billing_address) ? $user->billing_address->company : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Phone</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->billing_address) ? $user->billing_address->phone : '' }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                @else
                                    <div class="alert alert-info alert-dismissable">
                                        <i class="fa fa-info-circle"></i> No billing address found.
                                    </div>
                                @endif
                                <!-- END Billing Address Content -->
                            </div>
                            <!-- END Billing Address Block -->
                        </div>
                        <div class="col-lg-6">
                            <!-- Shipping Address Block -->
                            <div class="block">
                                <!-- Shipping Address Title -->
                                <div class="block-title">
                                    <h2><i class="fa fa-map-marker"></i> Shipping Address</h2>
                                </div>
                                <!-- END Shipping Address Title -->

                                <!-- Shipping Address Content -->
                                @if(!empty($user->shipping_address))
                                    <table class="table table-borderless table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">To</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->shipping_address) ? $user->shipping_address->first_name . ' ' . $user->shipping_address->last_name : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Address</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->shipping_address) ? $user->shipping_address->address . ' ' . $user->shipping_address->address_2 . ' ' . $user->shipping_address->city . ' ' . $user->shipping_address->state . ' ' . $user->shipping_address->zip . ' ' . $user->shipping_address->country : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Email</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->shipping_address) ? $user->shipping_address->email : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Company</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->shipping_address) ? $user->shipping_address->company : '' }}</td>
                                    </tr>
                                    <tr>
                                        <td class=" text-right" style="width: 20%">
                                            <strong class="fontface-oswald text-muted">Phone</strong></td>
                                        <td class=""
                                            style="width: 80%">{{ !empty($user->shipping_address) ? $user->shipping_address->phone : '' }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                @else
                                    <div class="alert alert-info alert-dismissable">
                                        <i class="fa fa-info-circle"></i> No shipping address found.
                                    </div>
                                @endif
                                <!-- END Shipping Address Content -->
                            </div>
                            <!-- END Shipping Address Block -->
                        </div>
                    </div>
                    <!-- END Customer Addresses Content -->
                </div>
                <!-- END Customer Addresses Block -->
            </div>
        @endif
    </div>
@endsection

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/users.js') }}"></script>
@endpush