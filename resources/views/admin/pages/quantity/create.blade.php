@extends('admin.layouts.base')

@section('content')
    @if ($size->shape->parent_id != 0)
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.shapes.show', $size->shape->parent->id) }}">{{ $size->shape->parent->name }}</a></li>
            <li><a href="{{ route('admin.sizes.index', $size->shape->id) }}">{{ $size->shape->name }}</a></li>
            <li><a href="{{ route('admin.quantities.index', $size->id) }}">{{ $size->name }}</a></li>
            <li><a href="{{ route('admin.quantities.index', $size->id) }}">Quantity</a></li>
            <li><span href="javascript:void(0)">Add New Quantity</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $size->name }}</h1>
            </div>
        </div>
    @else
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.sizes.index', $size->shape->id) }}">{{ $size->shape->name }}</a></li>
            <li><a href="{{ route('admin.quantities.index', $size->id) }}">{{ $size->name }}</a></li>
            <li><a href="{{ route('admin.quantities.index', $size->id) }}">Quantity</a></li>
            <li><span href="javascript:void(0)">Add New Quantity</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $size->name }}</h1>
            </div>
        </div>
    @endif
    <div class="row">
        {{  Form::open([
            'method' => 'POST',
            'id' => 'create-quantity',
            'route' => ['admin.quantities.store', $size->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add new Quantity</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="quantity_name">Quantity</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control input-number-no-dec" id="quantity_name" name="name"
                               placeholder="Enter Quantity.." value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="quantity_label">Label / Roll</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control input-number" id="quantity_label" name="label"
                               placeholder="Enter Label.." value="{{ (old('label') ? : 0) }}">
                        <small><i>0 if not applicable</i></small>
                        @if($errors->has('label'))
                            <span class="help-block animation-slideDown">{{ $errors->first('label') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="quantity_price">Price</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control input-number" id="quantity_price" name="price"
                               placeholder="Enter Quantity price.." value="{{ old('price') }}">
                        @if($errors->has('price'))
                            <span class="help-block animation-slideDown">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.quantities.index', $size->id) }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/quantities.js') }}"></script>
@endpush