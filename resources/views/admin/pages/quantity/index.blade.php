@extends('admin.layouts.base')

@section('content')
    @if ($size->shape->parent_id != 0)
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.shapes.show', $size->shape->parent->id) }}">{{ $size->shape->parent->name }}</a></li>
            <li><a href="{{ route('admin.sizes.index', $size->shape->id) }}">{{ $size->shape->name }}</a></li>
            <li><span href="javascript:void(0)">{{ $size->name }}</span></li>
            <li><span href="javascript:void(0)">View Quantities</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $size->name }}</h1>
            </div>
        </div>
    @else
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.sizes.index', $size->shape->id) }}">{{ $size->shape->name }}</a></li>
            <li><span href="javascript:void(0)">{{ $size->name }}</span></li>
            <li><span href="javascript:void(0)">View Quantities</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $size->name }}</h1>
            </div>
        </div>
    @endif
    @if (auth()->user()->can('Create Quantity'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.quantities.create', $size->id) }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Quantity
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-table sidebar-nav-icon"></i>
                <strong>Quantities</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable quantity-empty {{$quantities->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Quantities found.
        </div>
        <div class="table-responsive {{$quantities->count() == 0 ? 'johnCena' : '' }}">
            <table id="quantities-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Quantity
                    </th>
                    <th class="text-left">
                        Label / Roll
                    </th>
                    <th class="text-left">
                        Price
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($quantities as $quantity)
                    <tr data-quantity-id="{{$quantity->id}}">
                        <td class="text-center"><strong>{{ $quantity->id }}</strong></td>
                        <td class="text-center"><strong>{{ $quantity->name }}</strong></td>
                        <td class="text-left"><strong>{{ $quantity->label }}</strong></td>
                        <td class="text-left"><strong>{{ $quantity->price }}</strong></td>
                        <td class="text-center">{{ $quantity->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Quantity'))
                                    <a href="{{ route('admin.quantities.edit', $quantity->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Quantity'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-quantity-btn"
                                       data-original-title="Delete"
                                       data-quantity-id="{{ $quantity->id }}"
                                       data-quantity-route="{{ route('admin.quantities.delete', $quantity->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/quantities.js') }}"></script>
@endpush