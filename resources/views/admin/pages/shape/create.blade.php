@extends('admin.layouts.base')

@section('content')
    @if (!empty($shape) && $shape->parent_id == 0)
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.shapes.show', $shape->id) }}">{{ $shape->name }}</a></li>
            <li><span href="javascript:void(0)">Add New Sub-Shape</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $shape->name }}</h1>
            </div>
        </div>
    @else
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><span href="javascript:void(0)">Add New Shape</span></li>
        </ul>
    @endif
    <div class="row">
        {{  Form::open([
            'method' => 'POST',
            'id' => 'create-shape',
            'route' => ['admin.shapes.store', ((!empty($shape)) ? $shape->id : '' )],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add new Shape</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="shape_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="shape_name" name="name"
                               placeholder="Enter Shape name.." value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="shape_image">Image</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="image" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="" alt="" class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn" style="display: none;"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_image" class="remove-image" value="0">
                    </div>
                </div>
                <div class="form-group {{ (!empty($shape)) ? 'johnCena' : '' }}">
                    <label class="col-md-3 control-label">Is Popular?</label>

                    <div class="col-md-9">
                        <label class="switch switch-primary">
                            <input type="checkbox" id="is_popular" name="is_popular"
                                   value="1">
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        @if (!empty($shape))
                            <a href="{{ route('admin.shapes.show', $shape->id) }}" class="btn btn-sm btn-warning">Cancel</a>
                        @else
                            <a href="{{ route('admin.shapes.index') }}" class="btn btn-sm btn-warning">Cancel</a>
                        @endif
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/shapes.js') }}"></script>
@endpush