@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Shape'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.shapes.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Shape
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-cubes sidebar-nav-icon"></i>
                <strong>Shapes</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable shape-empty {{$shapes->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Shapes found.
        </div>
        <div class="table-responsive {{$shapes->count() == 0 ? 'johnCena' : '' }}">
            <table id="shapes-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($shapes as $shape)
                    <tr data-shape-id="{{$shape->id}}">
                        <td class="text-center"><strong>{{ $shape->id }}</strong></td>
                        <td class="text-center"><strong>{{ $shape->name }}</strong></td>
                        <td class="text-center">{{ $shape->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read Size') && $shape->children->count() == 0)
                                    <a href="{{ route('admin.sizes.index', $shape->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View sizes"><i class="fa fa-list-ol"></i></a>
                                @endif
                                @if (auth()->user()->can('Read Design') && $shape->children->count() == 0)
                                    <a href="{{ route('admin.designs.index', $shape->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View designs"><i class="fa fa-file-image-o"></i></a>
                                @endif
                                @if (auth()->user()->can('Read Shape'))
                                    <a href="{{ route('admin.shapes.show', $shape->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View sub-shapes"><i class="fa fa-cubes"></i></a>
                                @endif
                            </div>
                            <div class="btn-group btn-group-xs">
                                    @if (auth()->user()->can('Update Shape'))
                                    <a href="{{ route('admin.shapes.edit', $shape->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Shape'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-shape-btn"
                                       data-original-title="Delete"
                                       data-shape-id="{{ $shape->id }}"
                                       data-shape-route="{{ route('admin.shapes.delete', $shape->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/shapes.js') }}"></script>
@endpush