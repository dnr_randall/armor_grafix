@extends('admin.layouts.base')

@section('content')
    @if (auth()->user()->can('Create Why Choose Us Section'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.why_choose_us_sections.create') }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Why Choose Us Section
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-sliders sidebar-nav-icon"></i>
                <strong>Why Choose Us Sections</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable why_choose_us_section-empty {{$why_choose_us_sections->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Why Choose Us Sections found.
        </div>
        <div class="table-responsive {{$why_choose_us_sections->count() == 0 ? 'johnCena' : '' }}">
            <table id="why_choose_us_sections-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Title
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($why_choose_us_sections as $why_choose_us_section)
                    <tr data-why_choose_us_section-id="{{$why_choose_us_section->id}}">
                        <td class="text-center"><strong>{{ $why_choose_us_section->id }}</strong></td>
                        <td class="text-center"><strong>{{ $why_choose_us_section->title }}</strong></td>
                        <td class="text-center">{{ $why_choose_us_section->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Update Why Choose Us Section'))
                                    <a href="{{ route('admin.why_choose_us_sections.edit', $why_choose_us_section->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Why Choose Us Section'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-why_choose_us_section-btn"
                                       data-original-title="Delete"
                                       data-why_choose_us_section-id="{{ $why_choose_us_section->id }}"
                                       data-why_choose_us_section-route="{{ route('admin.why_choose_us_sections.delete', $why_choose_us_section->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/why_choose_us_sections.js') }}"></script>
@endpush