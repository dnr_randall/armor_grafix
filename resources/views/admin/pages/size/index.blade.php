@extends('admin.layouts.base')

@section('content')
    @if ($shape->parent_id != 0)
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.shapes.show', $shape->parent->id) }}">{{ $shape->parent->name }}</a></li>
            <li><span href="javascript:void(0)">{{ $shape->name }}</span></li>
            <li><span href="javascript:void(0)">View Sizes</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $shape->name }}</h1>
            </div>
        </div>
    @else
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><span href="javascript:void(0)">{{ $shape->name }}</span></li>
            <li><span href="javascript:void(0)">View Sizes</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $shape->name }}</h1>
            </div>
        </div>
    @endif
    @if (auth()->user()->can('Create Size'))
        <div class="row text-center">
            <div class="col-sm-12 col-lg-12">
                <a href="{{ route('admin.sizes.create', $shape->id) }}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background">
                        <h4 class="widget-content-light">
                            <strong>Add New</strong>
                            Size
                        </h4>
                    </div>
                    <div class="widget-extra-full">
                        <span class="h2 text-primary animation-expandOpen">
                            <i class="fa fa-plus"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
    @endif
    <div class="block full">
        <div class="block-title">
            <h2>
                <i class="fa fa-list-ol sidebar-nav-icon"></i>
                <strong>Sizes</strong>
            </h2>
        </div>
        <div class="alert alert-info alert-dismissable size-empty {{$sizes->count() == 0 ? '' : 'johnCena' }}">
            <i class="fa fa-info-circle"></i> No Sizes found.
        </div>
        <div class="table-responsive {{$sizes->count() == 0 ? 'johnCena' : '' }}">
            <table id="sizes-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr role="row">
                    <th class="text-center">
                        ID
                    </th>
                    <th class="text-center">
                        Name
                    </th>
                    <th class="text-center">
                        Date Created
                    </th>
                    <th class="text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($sizes as $size)
                    <tr data-size-id="{{$size->id}}">
                        <td class="text-center"><strong>{{ $size->id }}</strong></td>
                        <td class="text-center"><strong>{{ $size->name }}</strong></td>
                        <td class="text-center">{{ $size->created_at->format('F d, Y') }}</td>
                        <td class="text-center">
                            <div class="btn-group btn-group-xs">
                                @if (auth()->user()->can('Read Quantity'))
                                    <a href="{{ route('admin.quantities.index', $size->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="View quantities"><i class="fa fa-table"></i></a>
                                @endif
                                @if (auth()->user()->can('Update Size'))
                                    <a href="{{ route('admin.sizes.edit', $size->id) }}"
                                       data-toggle="tooltip"
                                       title=""
                                       class="btn btn-default"
                                       data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if (auth()->user()->can('Delete Size'))
                                    <a href="javascript:void(0)" data-toggle="tooltip"
                                       title=""
                                       class="btn btn-xs btn-danger delete-size-btn"
                                       data-original-title="Delete"
                                       data-size-id="{{ $size->id }}"
                                       data-size-route="{{ route('admin.sizes.delete', $size->id) }}">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('extrascripts')
    <script type="text/javascript" src="{{ asset('public/js/libraries/sizes.js') }}"></script>
@endpush