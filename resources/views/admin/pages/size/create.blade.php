@extends('admin.layouts.base')

@section('content')
    @if ($shape->parent_id != 0)
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.shapes.show', $shape->parent->id) }}">{{ $shape->parent->name }}</a></li>
            <li><a href="{{ route('admin.sizes.index', $shape->id) }}">{{ $shape->name }}</a></li>
            <li><a href="{{ route('admin.sizes.index', $shape->id) }}">Sizes</a></li>
            <li><span href="javascript:void(0)">Add New Size</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $shape->name }}</h1>
            </div>
        </div>
    @else
        <ul class="breadcrumb breadcrumb-top">
            <li><a href="{{ route('admin.shapes.index') }}">Shapes</a></li>
            <li><a href="{{ route('admin.sizes.index', $shape->id) }}">{{ $shape->name }}</a></li>
            <li><a href="{{ route('admin.sizes.index', $shape->id) }}">Sizes</a></li>
            <li><span href="javascript:void(0)">Add New Size</span></li>
        </ul>
        <div class="content-header">
            <div class="header-section">
                <h1>{{ $shape->name }}</h1>
            </div>
        </div>
    @endif
    <div class="row">
        {{  Form::open([
            'method' => 'POST',
            'id' => 'create-size',
            'route' => ['admin.sizes.store', $shape->id],
            'class' => 'form-horizontal ',
            'files' => TRUE
            ])
        }}
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add new Size</strong></h2>
                </div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="size_name">Name</label>

                    <div class="col-md-9">
                        <input type="text" class="form-control" id="size_name" name="name"
                               placeholder="Enter Size name.." value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="material_image">Label Sample</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Choose File <input type="file" name="image" style="display: none;">
                            </span>
                            </label>
                            <input type="text" class="form-control" readonly>
                        </div>
                        @if($errors->has('image'))
                            <span class="help-block animation-slideDown">{{ $errors->first('image') }}</span>
                        @endif
                    </div>
                    <div class="col-md-offset-3 col-md-9">
                        <a href="" class="zoom img-thumbnail" style="cursor: default !important;" data-toggle="lightbox-image">
                            <img src="" alt="" class="img-responsive center-block" style="max-width: 100px;">
                        </a>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger remove-image-btn" style="display: none;"><i class="fa fa-trash"></i> Remove</a>
                        <input type="hidden" name="remove_image" class="remove-image" value="0">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <a href="{{ route('admin.sizes.index', $shape->id) }}" class="btn btn-sm btn-warning">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@push('extrascripts')
    <script>

        uiInputBannerImage = $('input[name="image"]');
        uiRemoveImgBtn = $('.remove-image-btn');

        /* input file event */
        uiInputBannerImage.on('change', function () {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, ''),
                sValue = $(this).val(),
                ext = sValue.substring(sValue.lastIndexOf('.') + 1).toLowerCase();
            _fileselect($(this), numFiles, label, ext);
        });

        uiRemoveImgBtn.off('click').on('click', function () {
            var uiImgContainer = $(this).parents('.form-group:first').find('.img-responsive');
            var input = $(this).parents('.form-group:first').find('.input-group').find(':text');
            var element = $(this).parents('.form-group:first').find('input[type="file"]');
            uiImgContainer.attr('src', '');
            uiImgContainer.attr('alt', '');
            uiImgContainer.parents('.zoom:first').attr('href', uiImgContainer.attr('alt'));
            input.val('');
            element.val('');
            element.closest('.form-group').find('.remove-image-btn').hide();
            element.closest('.form-group').find('input.remove-image').val(1);
        });

        /* private fileselect function that will check/validate files input */
        function _fileselect(element, numFiles, label, ext) {
            var input = $(element).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            var uiImgContainer = $(element).parents('.form-group:first').find('.img-responsive');

            if (element.val() == '') {
                uiImgContainer.attr('src', uiImgContainer.attr('alt'));
                uiImgContainer.parents('.zoom:first').attr('href', uiImgContainer.attr('alt'));
                element.closest('.form-group').find('.remove-image-btn').hide();
                _fileselect_error(element, input, 'File is required.');
            } else {
                if (ext == 'jpeg' || ext == 'jpg' || ext == 'png') {
                    element.closest('.form-group').find('.help-block').remove();
                    element.closest('.form-group').removeClass('has-success has-error');
                    element.closest('.form-group').find('.remove-image-btn').show();
                    element.closest('.form-group').find('input.remove-image').val(0);

                    if (input.length) {
                        input.val(log);
                        platform.readURL($(element).get(0).files[0], uiImgContainer, []);
                    } else {
                        if (log) {
                            console.log(log);
                        }
                    }
                }
                else {
                    uiImgContainer.attr('src', uiImgContainer.attr('alt'));
                    uiImgContainer.parents('.zoom:first').attr('href', uiImgContainer.attr('alt'));
                    element.closest('.form-group').find('.remove-image-btn').hide();
                    _fileselect_error(element, input, 'The upload file must be a file of type: jpeg, jpg, png.');
                }
            }

            element.closest('.form-group').find('.remove-image-btn').off('click').on('click', function () {
                uiImgContainer.attr('src', '');
                uiImgContainer.attr('alt', '');
                uiImgContainer.parents('.zoom:first').attr('href', uiImgContainer.attr('alt'));
                input.val('');
                element.val('');
                element.closest('.form-group').find('.remove-image-btn').hide();
                element.closest('.form-group').find('input.remove-image').val(1);
            });
        }

        /* private fileselect_error function that will append/display error messages of file input */
        function _fileselect_error(element, input, msg) {
            element.closest('.form-group').find('.help-block').remove();
            element.closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            element.parents('.form-group > div').append('<span id="file-error" class="help-block animation-slideDown">' + msg + '</span>');
            element.val('');
            input.val('');
        }
    </script>
    <script type="text/javascript" src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/libraries/sizes.js') }}"></script>
@endpush