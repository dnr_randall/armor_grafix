@extends('front.layouts.base')

@section('content')
    @if (!empty($page))
        @php
            $item = $page;
        @endphp
    @else
        @php
            $item = (object) ['name' => 'register'];
        @endphp
    @endif
    @include('front.layouts.sections.header')

    {{-- <section class="sub-banner"
             data-aos="fade-down"
             data-aos-duration="2000">
        <div class="sub-banner__wrapper container container-lg-important">
            <div class="sub-banner__wrapper--row row">
                <div class="col-md-12">
                    <div class="sub-banner__image"
                         style="background-image:url('{{ asset('public/img/transbg3.png') }}');">
                        <h1>Register</h1>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="site-content site-section customer-register"
             data-aos="fade-down"
             data-aos-duration="2000">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-7">
                    <img src="{!! asset($item->page_sections()->where('section', 'image')->first()->content) ?? '' !!}" alt="Image" class="img-fluid c41 m63">
                </div>
                <div class="col-md-5 site-block">
                <h1>Register</h1>
                    {{  Form::open([
                        'method' => 'POST',
                        'id' => 'form-register',
                        'route' => ['customer.register.post'],
                        'class' => 'form-horizontal'
                        ])
                    }}
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                <input type="text" id="first_name" name="first_name" class="input-lg"
                                       placeholder="First name"
                                       value="{{ old('first_name') }}" autofocus>
                            </div>
                            @if ($errors->has('first_name'))
                                <span id="first_name-error" class="help-block animation-slideDown">
                                    {{ $errors->first('first_name') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                <input type="text" id="last_name" name="last_name" class="input-lg"
                                       placeholder="Last name"
                                       value="{{ old('last_name') }}">
                            </div>
                            @if ($errors->has('last_name'))
                                <span id="last_name-error" class="help-block animation-slideDown">
                                    {{ $errors->first('last_name') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--<div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">--}}
                        {{--<div class="col-xs-12">--}}
                            {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon"><i class="gi gi-user"></i></span>--}}
                                {{--<input type="text" id="user_name" name="user_name" class="input-lg"--}}
                                       {{--placeholder="Username"--}}
                                       {{--value="{{ old('user_name') }}">--}}
                            {{--</div>--}}
                            {{--@if ($errors->has('user_name'))--}}
                                {{--<span id="user_name-error" class="help-block animation-slideDown">--}}
                                    {{--{{ $errors->first('user_name') }}--}}
                                {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                <input type="text" id="email" name="email" class="input-lg"
                                       placeholder="Email"
                                       value="{{ old('email') }}">
                            </div>
                            @if ($errors->has('email'))
                                <span id="email-error" class="help-block animation-slideDown">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                <input type="password" id="password" name="password" class="input-lg"
                                       placeholder="Password">
                                @if ($errors->has('password'))
                                    <span id="password-error" class="help-block animation-slideDown">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                                <input type="password" id="password_confirmation" name="password_confirmation"
                                       class="input-lg" placeholder="Verify Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-6 text-right">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Sign Up
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                    <div class="text-center">
                        <a href="{{ url('customer/password/email') }}">
                            <small>Forgot password?</small>
                        </a>
                    </div>
                    <div class="text-center">
                        <a href="{{ url('customer/login') }}">
                            <small>Did you remember your password? Login here</small>
                        </a>
                    </div>
                    <!-- END Sign Up Form -->
                </div>
            </div>
        </div>
    </section>
    <!-- END Sign Up -->
    @include('front.layouts.sections.footer')
@endsection

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/front_login.js') }}"></script>
@endpush
