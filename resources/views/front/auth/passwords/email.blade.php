@extends('front.layouts.base')

@section('content')
    @if (!empty($page))
        @php
            $item = $page;
        @endphp
    @else
        @php
            $item = (object) ['name' => 'forgot password'];
        @endphp
    @endif
    @include('front.layouts.sections.header')

    <!-- <section class="sub-banner"
             data-aos="fade-down"
             data-aos-duration="2000">
        <div class="sub-banner__wrapper container container-lg-important">
            <div class="sub-banner__wrapper--row row">
                <div class="col-md-12">
                    <div class="sub-banner__image"
                         style="background-image:url('{{ asset('public/img/transbg3.png') }}');">
                        <h1>Forgot Password</h1>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <section class="site-content site-section customer-login forgot-pw"
             data-aos="fade-down"
             data-aos-duration="2000">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-7">
                    <img src="{!! asset($item->page_sections()->where('section', 'image')->first()->content) ?? '' !!}" alt="Image" class="img-fluid c41 m63">
                </div>
                <div class="col-md-5 site-block">
                <h1>Forgot Password</h1>
                    {{  Form::open([
                        'method' => 'POST',
                        'id' => 'form-email',
                        'route' => ['customer.password.email.post'],
                        'class' => 'form-horizontal'
                        ])
                    }}
                    @if (session()->has('status'))
                        <div class="alert alert-success">
                            {{ session()->get('status') }}
                        </div>
                    @endif
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                <input type="text" id="email" name="email"
                                       class="input-lg" placeholder="Email"
                                       value="{{ old('email') }}" autofocus>
                            </div>
                            @if ($errors->has('email'))
                                <span id="email-error" class="help-block animation-slideDown">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-12 text-right no-padding-important">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i>
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                    <div class="text-center">
                        <a href="{{ url('customer/password/email') }}">
                            <small>Forgot password?</small>
                        </a>
                    </div>
                    <div class="text-center">
                        <a href="{{ url('customer/register') }}">
                            <small>No account yet? Register here</small>
                        </a>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </section>
    @include('front.layouts.sections.footer')
@endsection

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/front_login.js') }}"></script>
@endpush