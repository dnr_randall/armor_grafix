@extends('front.layouts.base')

@section('content')
    @include('front.layouts.sections.header')
    <div id="dashboard">
        <section class="sub-banner"
                 data-aos="fade-down"
                 data-aos-duration="2000">
            <div class="sub-banner__wrapper container container-lg-important">
                <div class="sub-banner__wrapper--row row">
                    <div class="col-md-12">
                        <div class="sub-banner__image" style="background-image:url('{{ asset('public/img/transbg3.png') }}');">
                            <h1>My Account</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="site-content site-section site-slide-content dashboard__wrapper">
            <div class="container">
                <div class="row">
                    @include('front.pages.dashboard.sections.nav')
                    <div class="col-md-9">
                        <!-- Tab panes -->
                        <div class="dashboard__tab-content">
                            <div class="tab-content">
                                @include('front.pages.dashboard.sections.address')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('front.layouts.sections.footer')
@endsection

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/front_address.js') }}"></script>
@endpush