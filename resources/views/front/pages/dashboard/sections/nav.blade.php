<div class="col-md-3">
    <div class="dashboard__tab-menu">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('customer/dashboard') ? 'active' : '' }}" href="{{ route('customer.dashboard') }}"><h3><i>&#10148;</i> Dashboard</h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('customer/orders') ? 'active' : '' }}" href="{{ route('customer.orders') }}"><h3><i>&#10148;</i> Orders</h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('customer/address') ? 'active' : '' }}" href="{{ route('customer.address') }}"><h3><i>&#10148;</i> Addresses</h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('customer/profile') ? 'active' : '' }}" href="{{ route('customer.profile') }}"><h3><i>&#10148;</i> Edit Profile</h3></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('customer.logout') }}"><h3><i>&#10148;</i> Logout</h3></a>
            </li>
        </ul>
    </div>
</div>
