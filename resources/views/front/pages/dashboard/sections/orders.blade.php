<div class="tab-pane container active">
    <h2>Orders</h2>
    <div class="alert alert-warning {{$orders->count() == 0 ? '' : 'd-none' }}">
        No orders found
    </div>
    <div class="table-responsive {{$orders->count() == 0 ? 'd-none' : '' }}">
        <table id="orders-table" class="table table-borderless table-vcenter" style="width: 100%;">
            <thead>
            <tr role="row">
                <th class="text-left fontface-oswald">
                    Order No
                </th>
                <th class="text-left fontface-oswald">
                    Order Date
                </th>
                <th class="text-left fontface-oswald">
                    Bill To
                </th>
                <th class="text-left fontface-oswald">
                    Total Amount
                </th>
                <th class="text-center">
                    Action
                </th>
                <th class="text-left fontface-oswald" style="display: none;">
                    Order HTML
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr data-order-template-id="{{$order->id}}">
                    <td class="text-left">{{ $order->reference_no }}</td>
                    <td class="text-left">{{ $order->created_at->format('F d, Y h:i:s A') }}</td>
                    <td class="text-left">{{ !empty($order->billing_address) ? $order->billing_address->first_name . ' ' . $order->billing_address->last_name : '' }}</td>
                    <td class="text-left">$ {{ $order->total_amount }}</td>
                    <td class="text-center view-details">
                        <div class="btn-group btn-group-xs  ">
                            <a href="javascript:void(0)" title="" class="btn btn-link text-bold color-red details-control" data-order="{{ $order }}">
                                <i class="fas fa-chevron-right color-red"></i>
                                View Details
                            </a>
                        </div>
                    </td>
                    <td class="text-left" style="display: none;">
                        <div class="row">
                            <div class="col-lg-6 pd-h-0">
                                <div class="block">
                                    <div class="block-title">
                                        <h4>
                                            <i class="fa fa-credit-card text-muted"></i>
                                            <strong class="fontface-oswald text-muted">
                                                Billing Information
                                            </strong>
                                        </h4>
                                    </div>
                                    <table class="table table-borderless table-vcenter">
                                        <tbody>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">To</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->billing_address) ? $order->billing_address->first_name . ' ' . $order->billing_address->last_name : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Address</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->billing_address) ? $order->billing_address->address . ' ' . $order->billing_address->address_2 . ' ' . $order->billing_address->city . ' ' . $order->billing_address->state . ' ' . $order->billing_address->zip . ' ' . $order->billing_address->country : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Email</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->billing_address) ? $order->billing_address->email : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Company</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->billing_address) ? $order->billing_address->company : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Phone</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->billing_address) ? $order->billing_address->phone : '' }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-6 pd-h-0">
                                <div class="block">
                                    <div class="block-title">
                                        <h4><i class="fa fa-map-marker text-muted"></i>
                                            <strong class="fontface-oswald text-muted">
                                                Shipping Information
                                            </strong>
                                        </h4>
                                    </div>
                                    <table class="table table-borderless table-vcenter">
                                        <tbody>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">To</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->shipping_address) ? $order->shipping_address->first_name . ' ' . $order->shipping_address->last_name : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Address</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->shipping_address) ? $order->shipping_address->address . ' ' . $order->shipping_address->address_2 . ' ' . $order->shipping_address->city . ' ' . $order->shipping_address->state . ' ' . $order->shipping_address->zip . ' ' . $order->shipping_address->country : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Email</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->shipping_address) ? $order->shipping_address->email : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Company</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->shipping_address) ? $order->shipping_address->company : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class=" text-right" style="width: 20%">
                                                <strong class="fontface-oswald text-muted">Phone</strong></td>
                                            <td class=""
                                                style="width: 80%">{{ !empty($order->shipping_address) ? $order->shipping_address->phone : '' }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 pd-h-0">
                                <div class="block">
                                    <div class="block-title">
                                        <h4><i class="fa fa-bars text-muted"></i> <strong class="fontface-oswald text-muted">Additional Details</strong></h4>
                                    </div>
                                    <table class="table table-borderless table-vcenter">
                                        <tbody>
                                        <tr>
                                            <td style="width: 30%" class="text-right"><strong>Payment Method</strong></td>
                                            <td style="width: 70%">
                                                {{ $order->payment_details->gateway }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%" class="text-right"><strong>Transaction ID</strong></td>
                                            <td style="width: 70%">
                                                {{ $order->payment_details->transaction_id }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%" class="text-right"><strong>Shipping Method</strong></td>
                                            <td style="width: 70%">
                                                {{ $order->shipping_details->shipping_method }}
                                            </td>
                                        </tr>
                                        @if (!empty($order->coupon_details))
                                            <tr>
                                                <td style="width: 30%" class="text-right"><strong>Coupon Code</strong></td>
                                                <td style="width: 70%">
                                                    {{ $order->coupon_details->coupon_code }}
                                                    {{ !empty($order->coupon_details->coupon) ?
                                                        ($order->coupon_details->coupon->type == 1 ?
                                                            '(-' . $order->coupon_details->coupon->value . '%)'
                                                            : '(-$ ' . $order->coupon_details->coupon->value . ')' )
                                                     : '' }}
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td style="width: 30%" class="text-right"><strong>Notes</strong></td>
                                            <td style="width: 70%">
                                                {!! $order->notes != '' ? $order->notes : 'none' !!}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 pd-h-0">
                                <div class="block full">
                                    <div class="block-title">
                                        <h4>
                                            <i class="fa fa-shopping-cart text-muted sidebar-nav-icon"></i>&nbsp;<strong class="fontface-oswald text-muted">Products</strong>
                                        </h4>
                                    </div>
                                    <div class="row" style="margin: 0;">
                                        <table class="table table-bordered table-vcenter">
                                            <thead>
                                            <tr>
                                                <th class="">
                                                    Product
                                                </th>
                                                <th class="">
                                                    Total
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (!empty($order->items))
                                                @foreach($order->items as $item_key => $item)
                                                    <tr>
                                                        <td>
                                                            <b>{{ $item->name }}</b>
                                                            <br>
                                                            <div style="margin-left: 15px">
                                                                <span>Shape: {{ $item->shape }}</span>
                                                                <br>
                                                                @if ($item->design != '')
                                                                    <span>Design: {{ $item->design }}</span>
                                                                    <br>
                                                                @endif
                                                                <span>Size: {{ $item->size }}</span>
                                                                <br>
                                                                <span>Qty: {{ $item->quantity }}</span>
                                                                <br>
                                                                <span>Include Tamper Evident: {{ $item->include_tamper_evident ? 'Yes' : 'No' }}</span>
                                                                <br>
                                                                <span>Comments: {!! $item->comments ? $item->comments : 'None' !!}</span>
                                                                <br>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            $ {{ $item->price }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Subtotal</strong>
                                                </td>
                                                <td>
                                                    $ {{ $order->subtotal_amount }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Tax</strong>
                                                </td>
                                                <td>
                                                    $ {{ $order->tax_details->total_amount }}
                                                </td>
                                            </tr>
                                            @if (!empty($order->coupon_details))
                                                <tr>
                                                    <td class="text-right">
                                                        <strong>Coupon</strong>
                                                    </td>
                                                    <td>
                                                        - $ {{ $order->coupon_details->total_amount }}
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Shipping</strong>
                                                </td>
                                                <td>
                                                    $ {{ $order->shipping_details->total_amount }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Total</strong>
                                                </td>
                                                <td>
                                                    $ {{ $order->total_amount }}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
