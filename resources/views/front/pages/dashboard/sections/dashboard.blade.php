<div class="tab-pane container active">
    <h2>{{ auth()->user()->first_name . ' ' . auth()->user()->last_name }}</h2>
    <div class="desc-txt">
        <p>
            From your account dashboard you can view your <a href="{{ route('customer.orders') }}">recent orders</a>,
            manage your <a href="{{ route('customer.address') }}">addresses</a>, and edit your <a href="{{ route('customer.profile') }}">password and account details</a>.
        </p>
    </div>
</div>