<div class="tab-pane container active">
    <h2>Address</h2>
    <div class="customer-register">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-address',
            'route' => ['customer.address.update', auth()->user()->id],
            'class' => ''
            ])
        }}
        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
        <input type="hidden" name="billing_id" value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->id : '' }}">
        <input type="hidden" name="shipping_id" value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->id : '' }}">
        <div class="row">
            <h3 class="col-md-12">Billing Address</h3>
            <div class="col-md-6 form-group{{ $errors->has('billing_first_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_first_name">First name</label>
                    <input type="text" id="billing_first_name" name="billing_first_name" class="form-control"
                           placeholder="First name"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->first_name : '' }}">
                    @if ($errors->has('billing_first_name'))
                        <span id="billing_first_name-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_first_name') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_last_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_last_name">Last name</label>
                    <input type="text" id="billing_last_name" name="billing_last_name" class="form-control"
                           placeholder="Last name"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->last_name : '' }}">
                    @if ($errors->has('billing_last_name'))
                        <span id="billing_last_name-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_last_name') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group{{ $errors->has('billing_email') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_email">Email</label>
                    <input type="text" id="billing_email" name="billing_email" class="form-control"
                           placeholder="Email"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->email : '' }}">
                    @if ($errors->has('billing_email'))
                        <span id="billing_email-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_email') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_phone') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_phone">Phone</label>
                    <input type="text" id="billing_phone" name="billing_phone" class="form-control"
                           placeholder="Phone"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->phone : '' }}">
                    @if ($errors->has('billing_phone'))
                        <span id="billing_phone-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_phone') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_company') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_company">Company</label>
                    <input type="text" id="billing_company" name="billing_company" class="form-control"
                           placeholder="Company"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->company : '' }}">
                    @if ($errors->has('billing_company'))
                        <span id="billing_company-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_company') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_address') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_address">Address</label>
                    <input type="text" id="address" name="billing_address" class="form-control"
                           placeholder="House number and street name"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->address : '' }}">
                    @if ($errors->has('billing_address'))
                        <span id="billing_address-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_address') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group">
                <div>
                    <label class="control-label" for="billing_address_2">&nbsp;</label>
                    <input type="text" id="billing_address_2" name="billing_address_2" class="form-control"
                           placeholder="Apartment, suite, unit etc. (optional)"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->address_2 : '' }}">
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_country') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_country">Country</label>
                    <select name="billing_country" id="billing_country"
                            class="billing-country-select form-control form-control"
                            data-placeholder="Choose country..">
                        @if (!empty($countries))
                            @foreach($countries as $country)
                                <option value="{{ $country->name }}"
                                        data-country-id="{{ $country->id }}"
                                        data-country-code="{{ $country->code }}"
                                        data-country-name="{{ $country->name }}"
                                        data-is-default="{{ $country->is_default }}"
                                        data-states="{{ json_encode($country->states()->get()) }}"
                                        {{ !empty(auth()->user()->billing_address) ?
                                            (auth()->user()->billing_address->country == $country->name ? 'selected' : '')
                                        : ($country->is_default == 1 ? 'selected' : '') }}
                                >{{ $country->name }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if ($errors->has('billing_country'))
                        <span id="billing_country-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_country') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_city') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_city">City</label>
                    <input type="text" id="billing_city" name="billing_city" class="form-control"
                           placeholder="City"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->city : '' }}">
                    @if ($errors->has('billing_city'))
                        <span id="billing_city-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_city') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_state') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_state">State</label>
                    <select name="billing_state" id="billing_state"
                            class="billing-state-select form-control"
                            data-placeholder="Choose state.."
                            data-old-value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->state : '' }}">
                    </select>
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('billing_zip') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="billing_zip">Zip</label>
                    <input type="text" id="billing_zip" name="billing_zip" class="form-control"
                           placeholder="Zip"
                           value="{{ !empty(auth()->user()->billing_address) ? auth()->user()->billing_address->zip : '' }}">
                    @if ($errors->has('billing_zip'))
                        <span id="billing_zip-error" class="help-block animation-slideDown">
                            {{ $errors->first('billing_zip') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <h3 class="col-md-12">Shipping Address</h3>
            <div class="col-md-6 form-group{{ $errors->has('shipping_first_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_first_name">First name</label>
                    <input type="text" id="shipping_first_name" name="shipping_first_name" class="form-control"
                           placeholder="First name"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->first_name : '' }}">
                    @if ($errors->has('shipping_first_name'))
                        <span id="shipping_first_name-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_first_name') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_last_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_last_name">Last name</label>
                    <input type="text" id="shipping_last_name" name="shipping_last_name" class="form-control"
                           placeholder="Last name"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->last_name : '' }}">
                    @if ($errors->has('shipping_last_name'))
                        <span id="shipping_last_name-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_last_name') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group{{ $errors->has('shipping_email') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_email">Email</label>
                    <input type="text" id="shipping_email" name="shipping_email" class="form-control"
                           placeholder="Email"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->email : '' }}">
                    @if ($errors->has('shipping_email'))
                        <span id="shipping_email-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_email') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_phone') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_phone">Phone</label>
                    <input type="text" id="shipping_phone" name="shipping_phone" class="form-control"
                           placeholder="Phone"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->phone : '' }}">
                    @if ($errors->has('shipping_phone'))
                        <span id="shipping_phone-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_phone') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_company') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_company">Company</label>
                    <input type="text" id="shipping_company" name="shipping_company" class="form-control"
                           placeholder="Company"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->company : '' }}">
                    @if ($errors->has('shipping_company'))
                        <span id="shipping_company-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_company') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_address">Address</label>
                    <input type="text" id="address" name="shipping_address" class="form-control"
                           placeholder="House number and street name"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->address : '' }}">
                    @if ($errors->has('shipping_address'))
                        <span id="shipping_address-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_address') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group">
                <div>
                    <label class="control-label" for="shipping_address_2">&nbsp;</label>
                    <input type="text" id="shipping_address_2" name="shipping_address_2" class="form-control"
                           placeholder="Apartment, suite, unit etc. (optional)"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->address_2 : '' }}">
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_country') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_country">Country</label>
                    <select name="shipping_country" id="shipping_country"
                            class="shipping-country-select form-control form-control"
                            data-placeholder="Choose country..">
                        @if (!empty($countries))
                            @foreach($countries as $country)
                                <option value="{{ $country->name }}"
                                        data-country-id="{{ $country->id }}"
                                        data-country-code="{{ $country->code }}"
                                        data-country-name="{{ $country->name }}"
                                        data-is-default="{{ $country->is_default }}"
                                        data-states="{{ json_encode($country->states()->get()) }}"
                                        {{ !empty(auth()->user()->shipping_address) ?
                                            (auth()->user()->shipping_address->country == $country->name ? 'selected' : '')
                                        : ($country->is_default == 1 ? 'selected' : '') }}
                                >{{ $country->name }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if ($errors->has('shipping_country'))
                        <span id="shipping_country-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_country') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_city') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_city">City</label>
                    <input type="text" id="shipping_city" name="shipping_city" class="form-control"
                           placeholder="City"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->city : '' }}">
                    @if ($errors->has('shipping_city'))
                        <span id="shipping_city-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_city') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_state') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_state">State</label>
                    <select name="shipping_state" id="shipping_state"
                            class="shipping-state-select form-control"
                            data-placeholder="Choose state.."
                            data-old-value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->state : '' }}">
                    </select>
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('shipping_zip') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="shipping_zip">Zip</label>
                    <input type="text" id="shipping_zip" name="shipping_zip" class="form-control"
                           placeholder="Zip"
                           value="{{ !empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->zip : '' }}">
                    @if ($errors->has('shipping_zip'))
                        <span id="shipping_zip-error" class="help-block animation-slideDown">
                            {{ $errors->first('shipping_zip') }}
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 form-group form-actions">
                <div class="col-xs-6 text-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-caret-right"></i> Submit</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>