<div class="tab-pane container active">
    <h2>Edit Profile</h2>
    <div class="customer-register">
        {{  Form::open([
            'method' => 'PUT',
            'id' => 'edit-front_user',
            'route' => ['customer.profile.update', auth()->user()->id],
            'class' => ''
            ])
        }}
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="first_name">First name</label>
                    <input type="text" id="first_name" name="first_name" class="form-control"
                           placeholder="First name"
                           value="{{ auth()->user()->first_name }}">
                    @if ($errors->has('first_name'))
                        <span id="first_name-error" class="help-block animation-slideDown">
                                                        {{ $errors->first('first_name') }}
                                                    </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="last_name">Last name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control"
                           placeholder="Last name"
                           value="{{ auth()->user()->last_name }}">
                    @if ($errors->has('last_name'))
                        <span id="last_name-error" class="help-block animation-slideDown">
                                                        {{ $errors->first('last_name') }}
                                                    </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div>
                    <label class="control-label" for="email">Email</label>
                    <input type="text" id="email" name="email" class="form-control"
                           placeholder="Email"
                           value="{{ auth()->user()->email }}">
                    @if ($errors->has('email'))
                        <span id="email-error" class="help-block animation-slideDown">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group change-pass-checkbox-container">
                <div>
                    <label class="switch switch-primary">
                        Change Password?
                        <input type="checkbox" id="change_password" name="change_password"
                               value="1" {{ $errors->has('password') || $errors->has('old_password') ? 'checked' : ''}}>
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="change-pass-container col-md-12"
                 style="{{ $errors->has('password') || $errors->has('old_password') ? '' : 'display:none;'}}">
                <div class="row">
                    <div class="col-md-6 form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <div>
                            <label class="control-label" for="old_password">Current Password</label>
                            <input type="password" class="form-control" id="old_password"
                                   name="old_password"
                                   placeholder="Enter current password.." autocomplete="off">
                            @if($errors->has('old_password'))
                                <span class="help-block animation-slideDown">{{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div>
                            <label class="control-label" for="password">New Password</label>
                            <input type="password" class="form-control" id="password"
                                   name="password"
                                   placeholder="Enter new password..">
                            @if($errors->has('password'))
                                <span class="help-block animation-slideDown">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="offset-md-6 col-md-6 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div>
                            <label class="control-label" for="password_confirmation">Verify New
                                Password</label>
                            <input type="password" class="form-control" name="password_confirmation"
                                   placeholder="Verify new password..">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group form-actions">
                <div class="col-xs-12 text-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-caret-right"></i> Submit</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>