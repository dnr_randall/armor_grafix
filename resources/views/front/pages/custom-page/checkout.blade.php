@include('front.layouts.sections.header')
<main role="main" class="page-checkout">
	@include('front.pages.custom-page.sections.billing-payment')
</main>
@include('front.layouts.sections.footer')
