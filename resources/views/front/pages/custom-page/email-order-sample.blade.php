<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{!! $seo_meta['title'] !!}</title>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0 !important;
            }

            body[yahoo] .button a {
                background-color: #1ec1b8;
                padding: 15px 25px !important;
            }
        }

        @media only screen and (min-device-width: 601px) {
            .content {
                width: 600px !important;
            }

            .col387 {
                width: 387px !important;
            }
        }
    </style>
</head>
<body bgcolor="#32323a" style="font-family: Roboto, sans-serif; margin: 0; padding: 0;" yahoo="fix" class="email-order-content">
    <div class="information" style="margin: 0 auto; width: 600px; background: #ffffff; padding: 50px 20px; border: 2px dashed #8a558d;">
        <img class="m11" src="{{asset('/public/img/header-logo.png')}}" alt="Logo" style="text-align: center; margin: 0 auto; display: block;">
        <div class="introduction" style="font-family: Roboto, sans-serif; margin-top: 30px; color: #8a558d;">
            <h3 style="font-family: Roboto, sans-serif; font-size: 1.75rem;">Dear Sheila Olid,</h3>
            <p>We have received your order and we will respond without delay.</p>
        </div>

        <div class="information__order-details" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <!-- <h3>Order Details</h3> -->
            <div class="information__order-details--order-number" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Order:</span> OR000024</h4>
            </div>

            <div class="information__order-details--order-date" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Order Date:</span> December 17, 2020 07:44PM</h4>
            </div>
        </div>

        <div class="information__product-information" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <h3 style="text-align: center; font-weight: 700; font-family: Roboto, sans-serif; font-size: 18px; color: #6d6d6d; text-transform: uppercase; margin-bottom: 18px;">Product Information</h3>
            <div class="information__product-information--name" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Name:</span> Security Holographic Textile Tag Labels - Brand Protection Iron on Labels!</h4>
            </div>

            <div class="information__product-information--shape" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Shape:</span> Squares</h4>
            </div>

            <div class="information__product-information--size" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Size:</span> 15 x 15 mm ( 0.590 x 0.590 in)</h4>
            </div>

            <div class="information__product-information--material" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Material:</span> Metalized</h4>
            </div>

            <div class="information__product-information--quantity" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Quantity:</span> 1</h4>
            </div>

            <div class="information__product-information--price" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Price:</span> $13.00</h4>
            </div>

            <div class="information__product-information--comments" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Comment:</span> Hide if none</h4>
            </div>

            <div class="information__product-information--tamper" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Tamper Evident:</span> No</h4>
            </div>
        </div>

        <div class="information__order-details" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <h3 style="text-align: center; font-weight: 700; font-family: Roboto, sans-serif; font-size: 18px; color: #6d6d6d; text-transform: uppercase; margin-bottom: 18px;">Order Total</h3>
            <div class="information__order-details--subtotal" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Subtotal:</span> $ 4291</h4>
            </div>

            <div class="information__order-details--tax" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Tax:</span> $ 311.10</h4>
            </div>

            <div class="information__order-details--shipping" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Shipping:</span> 	$ 176.38</h4>
            </div>

            <div class="information__order-details--total" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Total:</span> $ 176.38</h4>
            </div>

            <div class="information__order-details--payment-method" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Payment Method:</span> Paypal</h4>
            </div>

            <div class="information__order-details--shipping-method" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Shipping Method:</span> FEDEX - FEDEX_2_DAY</h4>
            </div>

            <div class="information__order-details--notes" style="display: inline-block; width: 48%; vertical-align: top; margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif; margin: 0;"><span style="font-weight: 700; text-transform: uppercase;">Notes:</span> Hide if none</h4>
            </div>
        </div>

        <div class="information__billing-details" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <h3 style="text-align: center; font-family: Roboto, sans-serif; font-weight: 700; font-size: 18px; color: #6d6d6d; text-transform: uppercase; margin-bottom: 18px;">Billing Details</h3>
            <div class="information__billing-details--to" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">To:</span> Sheila Olid</h4>
            </div>

            <div class="information__billing-details--company" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Company:</span> Dog and Rooster</h4>
            </div>

            <div class="information__billing-details--address" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Address:</span> 5755 Oberline Dr Suite 106 San Diego California 92121</h4>
            </div>

            <div class="information__billing-details--email" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Email:</span> webfe1@dogandrooster.com</h4>
            </div>

            <div class="information__billing-details--phone" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Phone:</span> 125486133</h4>
            </div>
        </div>

        <div class="information__shipping-details" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <h3 style="text-align: center; font-weight: 700; font-size: 18px; font-family: Roboto, sans-serif; color: #6d6d6d; text-transform: uppercase; margin-bottom: 18px;">Billing Details</h3>
            <div class="information__shipping-details--to" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">To:</span> Sheila Olid</h4>
            </div>

            <div class="information__shipping-details--company" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Company:</span> Dog and Rooster</h4>
            </div>

            <div class="information__shipping-details--address" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Address:</span> 5755 Oberline Dr Suite 106 San Diego California 92121</h4>
            </div>

            <div class="information__shipping-details--email" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Email:</span> webfe1@dogandrooster.com</h4>
            </div>

            <div class="information__shipping-details--phone" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Phone:</span> 125486133</h4>
            </div>
        </div>

        <h3 class="thank-you" style="font-family: Roboto, sans-serif; text-align: center; text-transform: uppercase; font-weight: 700; color: #6d6d6d;">Thank you for your puchase!</h3>
    </div>

    <footer style="padding: 20px; margin: 0 auto; width: 600px; background: #8a558d;">
        <p class="company-name" style="font-family: Roboto, sans-serif; text-align: center; color: #fff; font-size: 15px; margin-bottom: 0;">Nano Grafix</p>
        <p class="address" style="font-family: Roboto, sans-serif; text-align: center; color: #fff; font-size: 15px; margin-bottom: 0;">12526 High Bluff Drive, Suite 300, San Diego, CA 92130, USA</a>
    </footer>
</body>
</html>