@include('front.layouts.sections.header')
<main role="main" class="page-product-details">
    @include('front.pages.custom-page.sections.product-details')
    @include('front.pages.custom-page.sections.featured-labels')
</main>
@include('front.layouts.sections.footer')