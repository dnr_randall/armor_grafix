@include('front.layouts.sections.header')

{{-- HP BANNER --}}
@include('front.pages.custom-page.sections.banner')
{{-- END OF HP BANNER --}}

{{-- HP QUICK n EASY --}}
<section id="quickNEasy">
    <h2 class="hidden">QUICK N EASY SECTION</h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 np">
                {{--<div class="c17 m50">--}}
                    {{--Quick & Easy Label Creation Process!--}}
                {{--</div>--}}
                <div class="c23 eg1 m52">
                    <div class="container-fluid np">
                        <div class="row np">
                            <div class="col-md-10 np">
                                <div class="container-fluid np">
                                    <div class="row np c111 m40">
                                        <div class="col-lg-3 col-sm-12 np m39">
                                            <div class="g4 mg42"><img class="lazy"
                                                                      src="{!! asset($item->page_sections()->where('section', 'section_1_banner_step_1_icon')->first()->content) ?? '' !!}"
                                                                      alt=""></div>
                                            <div class="g5 mg43">
                                                <div class="mg40">01:</div>
                                                <div class="mg41">{!! $item->page_sections()->where('section', 'section_1_banner_step_1_title')->first()->content ?? '' !!}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 np">
                                            <div class="g4 mg42"><img class="lazy"
                                                                      src="{!! asset($item->page_sections()->where('section', 'section_1_banner_step_2_icon')->first()->content) ?? '' !!}"
                                                                      alt=""></div>
                                            <div class="g5 mg43">
                                                <div class="mg40">02:</div>
                                                <div class="mg41">{!! $item->page_sections()->where('section', 'section_1_banner_step_2_title')->first()->content ?? '' !!}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 np">
                                            <div class="g4 mg42"><img class="lazy"
                                                                      src="{!! asset($item->page_sections()->where('section', 'section_1_banner_step_3_icon')->first()->content) ?? '' !!}"
                                                                      alt=""></div>
                                            <div class="g5 mg43">
                                                <div class="mg40">03:</div>
                                                <div class="mg41">{!! $item->page_sections()->where('section', 'section_1_banner_step_3_title')->first()->content ?? '' !!}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 np">
                                            <div class="g4 mg42"><img class="lazy"
                                                                      src="{!! asset($item->page_sections()->where('section', 'section_1_banner_step_4_icon')->first()->content) ?? '' !!}"
                                                                      alt=""></div>
                                            <div class="g5 mg43">
                                                <div class="mg40">04:</div>
                                                <div class="mg41">{!! $item->page_sections()->where('section', 'section_1_banner_step_4_title')->first()->content ?? '' !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 np m55">
                                <div class="g4 c25 mg42 m37"><img class="lazy"src="{!! asset($item->page_sections()->where('section', 'section_1_banner_step_5_icon')->first()->content) ?? '' !!}" alt=""></div>
                                <div class="c24 mg41 m38">{!! $item->page_sections()->where('section', 'section_1_banner_step_5_title')->first()->content ?? '' !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- END OF QUICK n EASY --}}



<section id="whychooseus-new">
    <h2 >WHY CHOOSE US</h2>
    <div class="whychooseus-slick">
        @foreach($why_choose_us_sections as $why_choose_us_section)
            <div class="whychooseus-item">
                <img src="{{ asset($why_choose_us_section->background_image) }}" class="hover-bg">
                <div class="icon">
                    <img @if(! $why_choose_us_section->description) style="margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center; z-index:1;" @endif class="g17-2" src="{{ asset($why_choose_us_section->icon) }}" alt="">
                    <img @if(! $why_choose_us_section->description) style="margin-left: auto; margin-right: auto; left: 0; right: 0; text-align: center; z-index:1;" @endif class="g17-1" src="{{ asset($why_choose_us_section->hover_icon) }}" alt="">
                </div>

                <div class="g8 e6-6 {{ $why_choose_us_section->description ? '' : 'text-center' }}" >{{ $why_choose_us_section->title }}</div>
                <div class="g9 e6-6"> {!! $why_choose_us_section->description !!} </div>
            </div>
        @endforeach
    </div>

    <div class="col-md-12 np" data-aos="fade-up" data-aos-duration="800">
        <div class="container-fluid c29 eg1x">
            <div class="row">
                <div class="col-md-12 c37">
                    <div class="container-fluid">
                        <div class="row d-flex justify-content-center">
                            <a href="{!! $item->page_sections()->where('section', 'why_choose_us_section_button_1_link')->first()->content ?? '' !!}" class="c65 m77">
                                <div class="c66 m75">
                                    <b>{!! $item->page_sections()->where('section', 'why_choose_us_section_button_1_label')->first()->content ?? '' !!}</b>
                                </div>
                            </a>
                            <a href="{!! $item->page_sections()->where('section', 'why_choose_us_section_button_2_link')->first()->content ?? '' !!}">
                                <div class="c67 m76">
                                    <b>{!! $item->page_sections()->where('section', 'why_choose_us_section_button_2_label')->first()->content ?? '' !!}</b>
                                </div>
                            </a>
                        </div>
                        </div>
                        {!! $item->page_sections()->where('section', 'why_choose_us_section_content')->first()->content ?? '' !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


{{-- HP WHY CHOOSE US --}}
<!-- <section id="whyChooseUs">
    <h2 class="hidden">WHY CHOOSE US SECTION</h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center c28 m57"
                 data-aos="fade-up"
                 data-aos-duration="800">
                {!! $item->page_sections()->where('section', 'why_choose_us_section_title')->first()->content ?? '' !!}
            </div>
            <div class="col-md-12"
                 data-aos="fade-up"
                 data-aos-duration="800">
                <div class="container-fluid eg1 m61 c113">
                    <div class="row s3 m60">
                        @include('front.pages.custom-page.sections.why_choose_us_section')
                    </div>
                </div>
            </div>
            <div class="col-md-12 np"
                 data-aos="fade-up"
                 data-aos-duration="800">
                <div class="container-fluid c29 eg1x">
                    <div class="row">
                        <div class="col-md-12 c37 m62">
                            <div class="container-fluid">
                                <div class="row d-flex justify-content-center">
                                    <a href="{!! $item->page_sections()->where('section', 'why_choose_us_section_button_1_link')->first()->content ?? '' !!}" class="c65 m77">
                                        <div class="c66 m75">
                                            <b>{!! $item->page_sections()->where('section', 'why_choose_us_section_button_1_label')->first()->content ?? '' !!}</b>
                                        </div>
                                    </a>
                                    <a href="{!! $item->page_sections()->where('section', 'why_choose_us_section_button_2_link')->first()->content ?? '' !!}">
                                        <div class="c67 m76">
                                            <b>{!! $item->page_sections()->where('section', 'why_choose_us_section_button_2_label')->first()->content ?? '' !!}</b>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            {!! $item->page_sections()->where('section', 'why_choose_us_section_content')->first()->content ?? '' !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
{{-- END OF WHY CHOOSE US --}}

{{-- HP FEATURED --}}
<section id="ftHolorgraphLabel" class="bg-lt-gray">
    <h2 class="hidden">FEATURED HOLORGRAPHIC LABEL SECTION</h2>
    <div class="container-fluid c39 eg1 m73"
         data-aos="fade-up"
         data-aos-duration="800">
        <div class="row">
            <div class="col-12 c40">
                <div class="container-fluid np">
                    <div class="row np">
                        <div class="col-md-6 np m72">
                            <div class="c42 m69">
                                <img src="{!! asset($item->page_sections()->where('section', 'featured_holographic_section_image')->first()->content) ?? '' !!}" alt=""
                                     class="img-fluid c41 m63 lazy">
                            </div>
                        </div>
                        <div class="col-md-3 np m65">
                            <div class="c43 m68 lazy" data-loader="">
                                {!! $item->page_sections()->where('section', 'featured_holographic_section_subtitle')->first()->content ?? '' !!}
                            </div>
                            <div class="c44 m74 lazy" data-loader="">
                                {!! $item->page_sections()->where('section', 'featured_holographic_section_title')->first()->content ?? '' !!}
                            </div>
                            <div class="c45 m70"></div>
                            <div class="c46 m66 lazy" data-loader="">
                                {!! $item->page_sections()->where('section', 'featured_holographic_section_content')->first()->content ?? '' !!}
                            </div>
                            <div class="c47 m67 lazy" data-loader="">
                                {!! $item->page_sections()->where('section', 'featured_holographic_section_subcontent')->first()->content ?? '' !!}
                            </div>
                        </div>
                        <div class="col-md-3 m64 np lazy tryfade" data-loader="appendScroll">
                            <div class="c47">
                                {!! $item->page_sections()->where('section', 'featured_holographic_section_subcontent')->first()->content ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- END FEATURED --}}

{{-- HP CATEGORY SLIDER --}}
<section id="categorySlider" class="bg-lt-gray">
    <h2 class="hidden">CATEGORY SLIDER SECTION</h2>
    <div class="container-fluid np"
         data-aos="fade-up"
         data-aos-duration="800">
        <div class="row np s4">
            @if (!empty($featured_products) && count($featured_products))
                <div class="col-12 np" style="font-size: 0">
                    <div class="featured-slider-large featured-nav">
                        @foreach($featured_products as $featured_product_key => $featured_product)
                            <div class="item pr-5 pl-5">
                                @php
                                    $default_image = $featured_product->product_images()->orderBy('is_default', 'desc')->first();
                                    $image = '';
                                    if (!empty($default_image)) {
                                        $image = $default_image->file;
                                    }

                                    if ($featured_product_key == 0) {
                                        $featured_product['ctr'] = $featured_products->count() + 1;
                                    } else {
                                        $featured_product['ctr'] = $featured_product_key + 1;
                                    }
                                @endphp
                                <div class="m83">
                                    <div class="g12 owl-feat m79">
                                        <div class="g11 m85">
                                            <div class="container-fluid">
                                                <div class="row">

                                                    <div class="col-6 np">
                                                        <div class="c51v2"><img class="img-fluid" src="{{ asset($image) }}" alt=""></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="c52 m80">{!! $featured_product->shape->name !!}</div>
                                                        <div class="c53"><a href="{{ url('products/' . $featured_product->slug) }}">{!! $featured_product->name !!}</a></div>
                                                        <div class="c54"></div>
                                                        <div class="c55">
                                                            @php
                                                                $qty = !empty($featured_product->size) ?
                                                                    $featured_product->size
                                                                    ->quantities()
                                                                    ->first() : NULL;
                                                            @endphp
                                                            Starting at: <span>$ {!! $featured_product->price !!}</span>
                                                        </div>
                                                        <!-- <div class="c56">
                                                            {!! $featured_product->short_description !!}
                                                        </div> -->
                                                        <a href="{{ url('products/' . $featured_product->slug) }}">
                                                            <div class="c57">
                                                                View Label
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="featured-slider-small featured-nav">
                        @foreach($featured_products->sortBy('ctr') as $featured_product)
                            <div class="item pr-5 pl-5">
                                @php
                                    $default_image = $featured_product->product_images()->orderBy('is_default', 'desc')->first();
                                    $image = '';
                                    if (!empty($default_image)) {
                                        $image = $default_image->file;
                                    }
                                @endphp
                                <div class="g12 owl-next-item m79">
                                    <div class="g11 m85">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="c52 m80">{!! $featured_product->shape->name !!}</div>
                                                    <div class="c53v2 m78"><a href="{{ url('products/' . $featured_product->slug) }}">{!! $featured_product->name !!}</a></div>
                                                </div>
                                                <div class="col-md-12 np">
                                                    <div class="c51v3">
                                                        <img class="img-fluid" src="{{asset($image)}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-12 np">
                    <div class="c33 c33-1">
                        <a href="javascript:void(0)" class="featured-slider-prev">
                            <div><img src="{{asset('/public/img/arrowleft.png')}}" alt=""></div>
                            <div class="mg63 ">PREVIOUS</div>
                        </a>
                    </div>
                    <div class="c34 c34-1">
                        <a href="javascript:void(0)" class="featured-slider-next">
                            <div><img src="{{asset('/public/img/arrowright.png')}}" alt=""></div>
                            <div class="mg63 ">NEXT</div>
                        </a>
                    </div>
                </div>
            @endif
            {{--<div class="col-12 np">
                <div class="owl-carousel js-owl-carousel-2 owl-theme c60 m71">
                    <div class="item">
                        <div class="m83">
                            <div class="g12 owl-feat m79">
                                <div class="g11 m85">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-6 np">
                                                <div class="c51v2"><img class="img-fluid"
                                                                        src="{{asset('/public/img/blueroundfeat.png')}}"
                                                                        alt=""></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="c52 m80">Category Name</div>
                                                <div class="c53">QR & Barcode Labels</div>
                                                <div class="c54"></div>
                                                <div class="c55">
                                                    Starting at: <span>$15.99</span>
                                                </div>
                                                <div class="c56">
                                                    Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor
                                                    blandit nullam urna quis
                                                </div>
                                                <a href="{{ url('product-details') }}">
                                                    <div class="c57">
                                                        View Label
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m84">
                            <div class="g12 owl-next-item m79">
                                <div class="g11 m85">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="c52 m80">Category Name</div>
                                                <div class="c53v2 m78">QR & Barcode Labels</div>
                                            </div>
                                            <div class="col-md-12 np">
                                                <div class="c51v3"><img class="img-fluid lazy"
                                                                        data-src="{{asset('/public/img/blueroundfeat.png')}}"
                                                                        alt=""></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 1</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/whiteroundfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 2</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/bluerectfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 3</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/whiteroundfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 4</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/bluerectfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 5</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/whiteroundfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item c63">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 6</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/bluerectfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item c63">
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">Category Name 7</div>
                                            <div class="c53v2 m78">Holographic Security Labels</div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3"><img class="img-fluid"
                                                                    src="{{asset('/public/img/whiteroundfeat.png')}}"
                                                                    alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <div class="container-fluid c64">
        <div class="row d-flex justify-content-center">
            <a href="{!! $item->page_sections()->where('section', 'featured_holographic_section_button_1_link')->first()->content ?? '' !!}" class="c65 m77">
                <div class="c66 m75">
                    <b>{!! $item->page_sections()->where('section', 'featured_holographic_section_button_1_label')->first()->content ?? '' !!}</b>
                </div>
            </a>
            <a href="{!! $item->page_sections()->where('section', 'featured_holographic_section_button_2_link')->first()->content ?? '' !!}">
                <div class="c67 m76">
                    <b>{!! $item->page_sections()->where('section', 'featured_holographic_section_button_2_label')->first()->content ?? '' !!}</b>
                </div>
            </a>
        </div>
    </div>
</section>
{{-- END OF CATEGORY SLIDER --}}

@php
    //$has_no_sub_banner=1;
@endphp
@include('front.layouts.sections.footer')
@push('extrascripts')
<script type="text/javascript">
    $('.js-owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoWidth: true, //ok na to kasi naglagay nako ng .owl-stage paddings 140px; see gen.scss
        smartSpeed: 1000,
        // center:true,
        // stagePadding:340, //kaaway to ng autoWidth:true
        nav: true,
        navText: [`
        <div class="c33 c33-1">
          <div><img src="{{asset('/public/img/arrowleft.png')}}" alt=""></div>
          <div class="mg63">PREVIOUS</div>
        </div>`, `
        <div class="c34 c34-1">
          <div><img src="{{asset('/public/img/arrowright.png')}}" alt=""></div>
          <div class="mg63">NEXT</div>
        </div>`],
        dots: false
    });
    $('.js-owl-carousel-2').owlCarousel({
        loop: false,
        margin: 10,
        autoWidth: true, //ok na to kasi naglagay nako ng .owl-stage paddings 140px; see gen.scss
        smartSpeed: 1000,
        // center:true,
        // stagePadding:340, //kaaway to ng autoWidth:true
        nav: true,
        navText: [`
        <div class="c33 m81">
          <div><img src="{{asset('/public/img/arrowleft.png')}}" alt=""></div>
          <div class="mg63">PREVIOUS</div>
        </div>`, `
        <div class="c34 m82">
          <div><img src="{{asset('/public/img/arrowright.png')}}" alt=""></div>
          <div class="mg63">NEXT</div>
        </div>`],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });
</script>
@endpush