@include('front.layouts.sections.header')
<main role="main" class="page-about-us">
    @php
        $image = $item->page_sections()->where('section', 'banner_image')->first()->content;
    @endphp
    @subbanner(['image'=>asset($image)])
    @slot('h1')
        About Us
    @endslot
    @slot('p')
        {!! $item->page_sections()->where('section', 'banner_description')->first()->content ?? '' !!}
    @endslot
    @endsubbanner
	@include('front.pages.custom-page.sections.left-image')
	@include('front.pages.custom-page.sections.right-image')
</main>
@include('front.layouts.sections.footer')
