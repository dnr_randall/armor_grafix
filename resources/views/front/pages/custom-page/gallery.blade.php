@include('front.layouts.sections.header')
<main role="main" class="page-gallery">
	@php
		$image = $item->page_sections()->where('section', 'banner_image')->first()->content;
	@endphp
	@subbanner(['image'=>asset($image)])
	@slot('h1')
		Gallery
	@endslot
	@slot('p')
		{!! $item->page_sections()->where('section', 'banner_description')->first()->content ?? '' !!}
	@endslot
	@endsubbanner
	@include('front.pages.custom-page.sections.gallery-items')
</main>
@include('front.layouts.sections.footer')
