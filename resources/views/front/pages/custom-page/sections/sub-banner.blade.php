<section class="sub-banner"
      data-aos="fade-down"
      data-aos-duration="2000">
	<div class="sub-banner__wrapper container container-lg-important">
		<div class="sub-banner__wrapper--row row">
			<div class="col-md-12">
				@isset($image)
					<div class="sub-banner__image" style="background-image:url('{{$image}}');">
						@isset($h1)
							<h1>{{$h1}}</h1>
						@endisset
						@isset($h2)
							<h2>{{$h2}}</h2>
						@endisset
						@isset($p)
							<p>{{$p}}</p>
						@endisset
					</div>
				@endisset
			</div>
		</div>
	</div>
</section>