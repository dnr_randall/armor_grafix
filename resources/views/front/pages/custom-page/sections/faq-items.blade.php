<section class="faq-items">
	<div class="faq-items__wrapper container container-md-important">
		<div class="faq-items__wrapper--row">
			<div class="col-md-12">
				<div id="accordion" class="row faq-items__items">
					@foreach($faqs as $faq_key => $faq)
						<div class="{{--card--}} col-12"
						      data-aos="fade-right"
						      data-aos-duration="2000">
							<div class="faq-items__item {{--active--}}">
								<div class="{{--card-header--}}" id="heading_{{ $faq_key }}">
									<a data-toggle="collapse" data-target="#collapse_{{ $faq_key }}">
										<h2>
												{!! $faq->title !!}
											<i class="arrow-down"></i>
										</h2>
									</a>
								</div>
								<div id="collapse_{{ $faq_key }}" class="collapse " aria-labelledby="heading_{{ $faq_key }}" data-parent="#accordion">
									<div class="card-body">
										{!! $faq->content !!}
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>

@push('extrascripts')
	<script>
        $('#accordion').on('shown.bs.collapse', function() {
			$(this).find('.collapse.show').parents('.faq-items__item').addClass('active');
        }).on('hidden.bs.collapse', function() {
			$(this).find('.collapse').parents('.faq-items__item').removeClass('active');
        });
	</script>
@endpush