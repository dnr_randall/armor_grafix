<section class="left-image">
	<div class="left-image__wrapper container container-lg-important">
		<div class="left-image__wrapper--row row">
			<div class="col-md-12">
				<div class="container container-lg-important no-padding-important">
					<div class="row">
						<div class="col-md-6 left-image__left"
						      data-aos="fade-right"
						      data-aos-duration="2000">
							<img src="{!! asset($item->page_sections()->where('section', 'section_1_image')->first()->content) ?? '' !!}">
						</div>
						<div class="col-md-6 left-image__right"
						      data-aos="fade-left"
						      data-aos-duration="2000">
							<i class="building-blue"></i>
							<h2>{!! $item->page_sections()->where('section', 'section_1_title')->first()->content ?? '' !!}</h2>
							<i class="blue-line"></i>
							{!! $item->page_sections()->where('section', 'section_1_content')->first()->content ?? '' !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
