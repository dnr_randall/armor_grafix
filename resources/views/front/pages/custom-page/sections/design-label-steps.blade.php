<section class="label-shape js-design-label">
	<div class="label-shape__wrapper container container-md-important">
		<div class="label-shape__wrapper--row row"
			data-aos="fade-up"
			data-aos-duration="1000">
			<div class="col-md-12">
				<div class="container container-md-important">
					<div class="row">
						<div class="col-md-12 label-shape__card">
							<div class="label-shape__navigation">
								<ol class="row container-sm js-tabs" type="1">
									<li class="col-md-3 no-padding-important active js-tab-1" data-step="1">
										<label>Shape</label>
									</li>
									<li class="col-md-3 no-padding-important js-tab-2" data-step="2">
										<label>Size & Quantity</label>
									</li>
									<li class="col-md-3 no-padding-important js-tab-3" data-step="3">
										<label>Design</label>
									</li>
									<li class="col-md-3 no-padding-important js-tab-4" data-step="4">
										<label>Material</label>
									</li>
								</ol>
							</div>
							<div class="label-shape__view">
								<div class="container container-md-important">
									{{  Form::open([
										'method' => 'POST',
										'id' => 'add-custom-design-to-cart',
										'route' => ['carts.store'],
										'class' => '',
										'onkeydown' => "return event.key != 'Enter'"
										/*'files' => TRUE*/
										])
									}}
									
										<div class="row">
											<div class="col-md-9 label-shape__view--left no-padding-important js-design-label-steps">

												<p id="loading_land" style="text-align: center; margin-top:50px;">Page is loading please wait..</p>

												{{--<div id="first" class="step">--}}
													@include('front.pages.custom-page.sections.step-1')
												{{--</div>--}}
												{{--<div id="second" class="step">--}}
													@include('front.pages.custom-page.sections.step-2')
												{{--</div>--}}
												{{--<div id="third" class="step">--}}
													@include('front.pages.custom-page.sections.step-3')
												{{--</div>--}}
												{{--<div id="fourth" class="step">--}}
													@include('front.pages.custom-page.sections.step-4')
												{{--</div>--}}
											</div>
											<div class="col-md-3 label-shape__view--right">
												@include('front.pages.custom-page.sections.design-label-option-container')
											</div>
										</div>
									{{ Form::close() }}
								</div>
							</div>
						</div>
					</div>
					{{-- <div class="row">
						<div class="col-md-12 label-shape__cta">
							<div class="container container-sm-important">
								<div class="row">
									<div class="col-md-8">
										{!! $item->page_sections()->where('section', 'bottom_section_content')->first()->content ?? '' !!}
									</div>
									<div class="col-md-4">
										<a href="{!! $item->page_sections()->where('section', 'bottom_section_button_link')->first()->content ?? '' !!}">
											<i class="checklist"></i>
											{!! $item->page_sections()->where('section', 'bottom_section_button_label')->first()->content ?? '' !!}
										</a>
									</div>
								</div>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</section>