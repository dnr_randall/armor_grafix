<section class="cta-faq">
	<div class="cta-faq__wrapper container container-md-important">
		<div class="cta-faq__wrapper--row">
			<div class="col-md-12">
				<div class="row cta-faq__items"
			      data-aos="fade-up"
			      data-aos-duration="2000">
					<div class="col-md-4">
						<div class="cta-faq__item" style="background-image:url('{{asset('public/images/sections/cta-faq/background-photo-1.png')}}');">
							<i class="building"></i>
							<h2>{!! $item->page_sections()->where('section', 'block_1_title')->first()->content ?? '' !!}</h2>
							<a href="{!! $item->page_sections()->where('section', 'block_1_button_link')->first()->content ?? '' !!}">
								{!! $item->page_sections()->where('section', 'block_1_button_label')->first()->content ?? '' !!} <i class="small-caret-right"></i>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="cta-faq__item" style="background-image:url('{{asset('public/images/sections/cta-faq/background-photo-2.png')}}');">
							<i class="phone"></i>
							<h2>{!! $item->page_sections()->where('section', 'block_2_title')->first()->content ?? '' !!}</h2>
							<a href="{!! $item->page_sections()->where('section', 'block_2_button_link')->first()->content ?? '' !!}">
								{!! $item->page_sections()->where('section', 'block_2_button_label')->first()->content ?? '' !!} <i class="small-caret-right"></i>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="cta-faq__item" style="background-image:url('{{asset('public/images/sections/cta-faq/background-photo-3.png')}}');">
							<i class="mail"></i>
							<h2>{!! $item->page_sections()->where('section', 'block_3_title')->first()->content ?? '' !!}</h2>
							<a href="{!! $item->page_sections()->where('section', 'block_3_button_link')->first()->content ?? '' !!}">
								{!! $item->page_sections()->where('section', 'block_3_button_label')->first()->content ?? '' !!} <i class="small-caret-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
