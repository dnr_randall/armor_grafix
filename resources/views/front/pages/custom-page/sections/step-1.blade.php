<div class="steps" data-step="1">
    {!! $item->page_sections()->where('section', 'step_1_instruction')->first()->content ?? '' !!}
    <div class="label-shape__choose js-select-radio-with-shadow">
        <div class="row form-container">
            @foreach($shapes as $shape)
                <div class="col-md-3 no-padding-important shape-option" data-shape-id="{{ $shape->id }}" data-shape-parent-id="{{ $shape->parent_id }}"
                     data-shape-sub-shape-count="{{ $shape->children()->count() }}"
                     style="{{ $shape->parent_id > 0 ? 'display:none' : '' }}">
                    <div class="label-shape__item">
                        @if($shape->is_popular)
                            <label class="is-popular"><i class="star"></i>Most Popular</label>
                        @endif
                        <label>
                            <input type="radio" name="shape_id" class="shape-radio" value="{{ $shape->id }}"
                                   data-shape-name="{{ (($shape->parent_id > 0) ? '<span>' . $shape->parent->name . ':</span>' : '') . '<span>'. $shape->name .'</span>' }}"
                                   data-shape-image="{{ asset($shape->image) }}"
                            >
                            <i class="shadow"></i>
                            <img class="inline" src="{{ asset($shape->image) }}">
                            <h4>{{ $shape->name }}</h4>
                        </label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>