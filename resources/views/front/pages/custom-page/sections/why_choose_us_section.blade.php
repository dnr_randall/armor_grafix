<div class="owl-carousel owl-carousel2 js-owl-carousel owl-theme">
    @foreach($why_choose_us_sections as $why_choose_us_section)
        <div class="item e6-2">
            <div class="owl-next-item">
                <div class="">
                    <div class="container-fluid">
                        <div class="row s5">
                            <div class="col-md-12">
                                <div class="c30 e6-1"><img src="{{ asset($why_choose_us_section->background_image) }}" alt="">
                                </div>
                                <div class="g17 e6-5-2"
                                ><img 
                                    @if(! $why_choose_us_section->description)
                                        style="margin-left: auto;
                                        margin-right: auto;
                                        left: 0;
                                        right: 0;
                                        text-align: center;"
                                    @endif
                                    class="g17-2" src="{{ asset($why_choose_us_section->icon) }}" alt=""></div>
                                <div class="g17 e6-5-1"
                                    ><img
                                    @if(! $why_choose_us_section->description)
                                        style="margin-left: auto;
                                        margin-right: auto;
                                        left: 0;
                                        right: 0;
                                        text-align: center;"
                                    @endif
                                    class="g17-1" src="{{ asset($why_choose_us_section->hover_icon) }}" alt=""></div>
                                <div class="g8 e6-6 {{ $why_choose_us_section->description ? '' : 'text-center' }}"
                                >{{ $why_choose_us_section->title }}</div>
                                <div class="g9 e6-6">
                                    {!! $why_choose_us_section->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>