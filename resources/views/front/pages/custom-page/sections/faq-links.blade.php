<section class="faq-links"
      data-aos="fade-up"
      data-aos-duration="2000">
	<div class="faq-links__wrapper container">
		<div class="faq-links__wrapper--row">
			<div class="col-md-12 faq-links__buttons">
				<a href="{!! $item->page_sections()->where('section', 'button_section_button_1_link')->first()->content ?? '' !!}" class="faq-links__buttons__button--reversed">
					{!! $item->page_sections()->where('section', 'button_section_button_1_label')->first()->content ?? '' !!}
				</a>
				<a href="{!! $item->page_sections()->where('section', 'button_section_button_2_link')->first()->content ?? '' !!}" class="faq-links__buttons__button">
					{!! $item->page_sections()->where('section', 'button_section_button_2_label')->first()->content ?? '' !!} <i class="caret-right"></i>
				</a>
			</div>
		</div>
	</div>
</section>
