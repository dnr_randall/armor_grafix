<div class="material-selection__view material-selection__view--left steps" data-step="4">
    {!! $item->page_sections()->where('section', 'step_4_instruction')->first()->content ?? '' !!}
    <div class="row material-selection__items js-select-material-type">
        @foreach($materials as $material)
            <div class="col-md-4 js-smt-item no-padding-important material-selection__item">
                <label>
                    <input type="radio" name="material_id" value="{{ $material->id }}"
                           data-material-name="{{ $material->name }}">
                    <img class="js-smt-normal" src="{{ asset($material->image) }}">
                    <div>
                        {{ $material->name }}
                        {!! $material->description != '' ? '<i class="small-info-button" data-placement="top" data-container="body" data-toggle="tooltip" data-trigger="hover" title="'.$material->description.'"></i>' : ''!!}
                    </div>
                </label>
            </div>
        @endforeach
        {{--<div class="col-md-3 js-smt-item js-has-selected no-padding-important material-selection__item">
            <label>
                <input type="checkbox" name="include_tamper_evident" value="1">
                <img class="unchecked"
                     src="{{asset('public/images/sections/material-selection/photo-checkbox.jpg')}}">
                <img class="checked"
                     src="{{asset('public/images/sections/material-selection/photo-checkbox-selected-hovered.jpg')}}">
                <div>
                    Include Tamper Evident
                    <i class="small-info-button"
                       data-placement="top" data-container="body" data-toggle="tooltip" data-trigger="hover" title="Include Tamper Evident Material Type"></i>
                </div>
            </label>
        </div>--}}
    </div>
    <div class="row material-selection__add-comments">
        <div class="col-12">
            <p>Leave notes:</p>
            <textarea style="min-height: 100px;" name="comments" placeholder="Additional Comments..."></textarea>
        </div>
    </div>
</div>