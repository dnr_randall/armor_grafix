<div class="size-quantity__view steps" data-step="2">
    {!! $item->page_sections()->where('section', 'step_2_instruction')->first()->content ?? '' !!}
    <div class="size-quantity__choose container-fluid js-size-quantity">
        <div class="row">
            <div class="col-md-6 no-padding-important size-quantity__choose--left">
                <div class="size-quantity__item">
                    <i class="right-border"></i>
                    <h2>Size:</h2>
                    <p>(width x height) - How Big is That?</p>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 no-padding-important">
                                @foreach($shapes as $shape)
                                    @foreach($shape->sizes as $size)
                                        <label class="col-md-5 no-padding-important size-option"
                                               data-shape-id="{{ $shape->id }}"
                                               data-size-id="{{ $size->id }}">

                                            <input type="radio" name="size_id" class="size_id" value="{{ $size->id }}"
                                                   data-size-name="{{ 'Size: ' . $size->name }}"><i
                                                    class="js-size-quantity-icon"></i><span>{{ $size->name }}</span>
                                        </label>
                                    @endforeach
                                @endforeach
                            </div>
                            @foreach($shapes as $shape)
                                @foreach($shape->sizes as $size)
                                    <div hidden id="div_{{ $size->id }}" class="col-md-12 no-padding-important div_label_image">
                                        <div class="size-quantity__sample">
                                            <label>Label Sample:</label>
{{--                                            {{$size->image}}--}}
                                            @if($size->image != '')
                                                <img class="" src="{{asset(''.$size->image.'')}}" style="width:100%;">
                                            @else
                                                <img class="shape-image" src="{{asset('public/images/sections/size-quantity/photo.png')}}" style="width:100%;">
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 no-padding-important size-quantity__choose--right">
                <div class="size-quantity__item">
                    <h2>Quantity:</h2>
                    <div class="price-discount-section">
                        <span class="sec-bulk-prices">
                            
                        </span>
                    </div>
                    <!-- <p>(Prices shown in USD)</p> -->
                    <label class="col-md-12 no-padding-important"
                            data-shape-id="{{ $shape->id }}"
                            data-size-id="{{ $size->id }}">
                        <input type="text" value="1" name="quantity_id" id="qty_step2">
                        $ <span id="price_label"></span>
                        @csrf
                    </label>
                    <span id="label_roll"></span>
                </div>
            </div>
        </div>
    </div>
</div>
