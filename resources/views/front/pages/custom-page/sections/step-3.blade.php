<div class="design-choices__view steps" data-step="3">
    {!! $item->page_sections()->where('section', 'step_3_instruction')->first()->content ?? '' !!}
    <div class="design-choices__choose">
        <div class="row design-choices__designs">
            @foreach($shapes as $shape)
                @foreach($shape->designs as $design)
                    <div class="col-md-3 no-padding-important design-option" data-shape-id="{{ $shape->id }}"
                         data-design-id="{{ $design->id }}">
                        <div class="design-choices__designs__item">
                            <label>
                                <input type="radio" name="design_id" value="{{ $design->id }}"
                                       data-design-name="{{ $design->name }}">
                                <img class="inline" src="{{ asset($design->image) }}">
                                <label>{{ $design->name }}</label>
                            </label>
                        </div>
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
</div>