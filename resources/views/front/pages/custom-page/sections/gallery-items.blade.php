<section class="gallery-items">
	<div class="gallery-items__wrapper container container-md-important">
		<div class="gallery-items__wrapper--row">
			<div class="col-md-12">
				<div class="row gallery-items__gallery js-gallery js-click-for-modal js-show-modal-show-items">
					@foreach($galleries as $gallery)
						<div class="col-md-3"
							data-aos="fade-up"
							data-aos-duration="1000">
							<a href="#" class="js-modal-item">
								<img src="{{asset($gallery->image)}}">
								<div class="gallery-items__hover">
									<i class="round-plus"></i>
								</div>
								<p class="description">
									{!! $gallery->description !!}
								</p>
							</a>
						</div>
					@endforeach
				</div>
				{{--<div class="row">--}}
					{{--<div class="col-md-12 gallery-items__load-more">--}}
						{{--<a href="#">Load More</a>--}}
					{{--</div>--}}
				{{--</div>--}}
			</div>
		</div>
	</div>
	<div class="gallery-items__modal js-modal-out js-modal-slider" style="//display:inline;">
		<div class="js-modal-close"></div>
		<div class="gallery-items__modal__scale">
			<div class="gallery-items__modal__preview">
				<div class="js-modal-slider-close float-right">
					<a href="javascript:void(0)" class="js-modal-close-btn">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="js-modal-slider-prev">
					<i class="big-white-caret-left"></i>
				</div>
				<div class="big-photo js-modal-slider-photos js-modal-target">
					{{-- dont put anything in here --}}
				</div>
				<div class="js-modal-slider-next">
					<i class="big-white-caret-right"></i>
				</div>
				<p class="slider-description">1Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.</p>
			</div>
			<div class="js-modal-slider-navigation-holder">
				<div class="gallery-items__modal__nav js-modal-slider-navigation d-flex justify-content-center soft">
				</div>
			</div>
		</div>
	</div>
</section>