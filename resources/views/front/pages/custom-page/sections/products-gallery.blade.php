<section class="products-gallery">
	<div class="products-gallery__wrapper container container-md-important">
		<div class="products-gallery__wrapper--row">
			<div class="col-md-12">
				<div class="container-fluid">
					<div class="row products-gallery__gallery">
						@if (!empty($products) && count($products))
							@foreach($products as $product)
								<div class="col-md-4"
									 data-aos="fade-up"
									 data-aos-duration="2000">
									<a href="{{ url('products/' . $product->slug) }}">
										<div class="products-gallery__item">
											@php
												$default_image = $product->product_images()->orderBy('is_default', 'desc')->first();
												$image = '';
												if (!empty($default_image)) {
													$image = $default_image->file;
												}
												$qty = !empty($product->size) ?
													$product->size
													->quantities()
													->first() : NULL;
											@endphp
											<img src="{{ asset($image) }}">
											<h3>{!! $product->shape->name !!}</h3>
											<h2>{!! $product->name !!}</h2>
											<div class="products-gallery__item__line"></div>
											<label>Starting at: <span>${!! number_format(($product->price), 2) !!}</span></label>
										</div>
									</a>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning">
								No Products Found!
							</div>
						@endif
					</div>
					<div class="row">
						<div class="col-md-12">
							@include('front.pages.custom-page.sections.pagination')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>