<section class="featured-labels">
    <div class="featured-labels__wrapper container">
        <div class="featured-labels__wrapper--row row">
            <div class="col-md-12">

            </div>
        </div>
    </div>
</section>

<div class="row np s4 s4-1">
    @if (!empty($featured_products) && count($featured_products))
        <h1 class="c114"
            data-aos="fade-down"
            data-aos-duration="2000">
            Featured Labels
        </h1>
        <i class="c115"
           data-aos="fade-right"
           data-aos-duration="2000"></i>
        <div class="col-12 np"
             data-aos="fade-left"
             data-aos-duration="2000" style="font-size: 0">
            <div class="featured-slider-large featured-nav">
                @foreach($featured_products as $featured_product_key => $featured_product)
                    <div class="item pr-5 pl-5">
                        @php
                            $default_image = $featured_product->product_images()->orderBy('is_default', 'desc')->first();
                            $image = '';
                            if (!empty($default_image)) {
                                $image = $default_image->file;
                            }

                            if ($featured_product_key == 0) {
                                $featured_product['ctr'] = $featured_products->count() + 1;
                            } else {
                                $featured_product['ctr'] = $featured_product_key + 1;
                            }
                        @endphp
                        <div class="m83">
                            <div class="g12 owl-feat m79">
                                <div class="g11 m85">
                                    <div class="container-fluid">
                                        <div class="row">

                                            <div class="col-6 np">
                                                <div class="c51v2"><img class="img-fluid" src="{{ asset($image) }}" alt=""></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="c52 m80">{!! $featured_product->shape->name !!}</div>
                                                <div class="c53"><a href="{{ url('products/' . $featured_product->slug) }}">{!! $featured_product->name !!}</a></div>
                                                <div class="c54"></div>
                                                <div class="c55">
                                                    @php
                                                        $qty = !empty($featured_product->size) ?
                                                            $featured_product->size
                                                            ->quantities()
                                                            ->first() : NULL;
                                                    @endphp
                                                    <!-- <label>Starting at: <span>$ {!! number_format(($product->price), 2) !!}</span> / Label</label> -->
                                                </div>
                                                <div class="c56">
                                                 {!!  str_limit( strip_tags($featured_product->description), 135) !!}
                                                </div>
                                                <a href="{{ url('products/' . $featured_product->slug) }}">
                                                    <div class="c57">
                                                        View Label
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="featured-slider-small featured-nav">
                @foreach($featured_products->sortBy('ctr') as $featured_product)
                    <div class="item pr-5 pl-5">
                        @php
                            $default_image = $featured_product->product_images()->orderBy('is_default', 'desc')->first();
                            $image = '';
                            if (!empty($default_image)) {
                                $image = $default_image->file;
                            }
                        @endphp
                        <div class="g12 owl-next-item m79">
                            <div class="g11 m85">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="c52 m80">{!! $featured_product->shape->name !!}</div>
                                            <div class="c53v2 m78"><a href="{{ url('products/' . $featured_product->slug) }}">{!! $featured_product->name !!}</a></div>
                                        </div>
                                        <div class="col-md-12 np">
                                            <div class="c51v3">
                                                <img class="img-fluid" src="{{asset($image)}}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-12 np"
             data-aos="fade-left"
             data-aos-duration="2000">
            <div class="c33 c33-1">
                <a href="javascript:void(0)" class="featured-slider-prev">
                    <div><img src="{{asset('/public/img/arrowleft.png')}}" alt=""></div>
                    <div class="mg63 ">PREVIOUS</div>
                </a>
            </div>
            <div class="c34 c34-1">
                <a href="javascript:void(0)" class="featured-slider-next">
                    <div><img src="{{asset('/public/img/arrowright.png')}}" alt=""></div>
                    <div class="mg63 ">NEXT</div>
                </a>
            </div>
        </div>
    @endif
</div>
