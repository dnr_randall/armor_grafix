<section class="right-image">
	<div class="right-image__wrapper container container-xs-important">
		<div class="right-image__wrapper--row row">
			<div class="col-md-12">
				<div class="container">
					<div class="row right-image__card">
						<div class="col-md-6 right-image__card--left"
						      data-aos="fade-right"
						      data-aos-duration="2000">
							<i class="building-skeleton"></i>
							<p>
								{!! $item->page_sections()->where('section', 'section_2_title')->first()->content ?? '' !!}
							</p>
						</div>
						<div class="col-md-6 right-image__card--right"
					      data-aos="fade-left"
					      data-aos-duration="2000">
							<img src="{!! asset($item->page_sections()->where('section', 'section_2_image')->first()->content) ?? '' !!}">
						</div>
					</div>
					{!! $item->page_sections()->where('section', 'section_2_content')->first()->content ?? '' !!}
				</div>
			</div>
		</div>
	</div>
</section>
