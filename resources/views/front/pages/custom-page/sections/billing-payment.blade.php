<section class="billing-payment">
    <div class="billing-payment__wrapper container container-lg2-important">
        <div class="billing-payment__wrapper--row row">
            <div class="col-md-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <h1>Checkout</h1>
                        </div>
                        {{  Form::open([
                            'method' => 'POST',
                            'id' => 'checkout-form',
                            'route' => ['checkout.store'],
                            'class' => '',
                            /*'files' => TRUE*/
                            ])
                        }}
                        <div class="col-md-10 offset-md-1">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="billing-payment--left">
                                        <div class="row billing-container">
                                            <div class="col no-padding-important">
                                            {{-- <a href="javascript:void(0)" hidden class="" style="float:right" id="btn_edit_details">Edit Details</a> --}}
                                                <h3>
                                                    <i class="fas fa-chevron-down"></i>
                                                    Billing Details
                                                </h3>
                                            </div>
                                            <i class="line"></i>
                                        </div>
                                        <div class="row billing-container">
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>First Name <span>*</span></label>
                                                    <input type="text" name="billing_first_name"
                                                           value="{{ auth()->check() ? auth()->user()->first_name : old('billing_first_name') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Last Name <span>*</span></label>
                                                    <input type="text" name="billing_last_name"
                                                           value="{{ auth()->check() ? auth()->user()->last_name : old('billing_last_name')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Phone <span>*</span></label>
                                                    <input type="text" name="billing_phone"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->phone : '') : old('billing_phone')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Email address <span>*</span></label>
                                                    <input type="text" name="billing_email"
                                                           value="{{ auth()->check() ? auth()->user()->email : old('billing_email')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Company Name(optional)</label>
                                                    <input type="text" name="billing_company"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->company : '') : old('billing_company')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Country <span>*</span></label>
                                                    <select name="billing_country" id="billing_country"
                                                            class="billing-country-select form-control"
                                                            data-placeholder="Choose country..">
                                                        @if (!empty($countries))
                                                            @foreach($countries as $country)
                                                                @if($country->id == 231)
                                                                    <option value="{{ $country->name }}"
                                                                            data-country-id="{{ $country->id }}"
                                                                            data-country-code="{{ $country->code }}"
                                                                            data-country-name="{{ $country->name }}"
                                                                            data-is-default="{{ $country->is_default }}"
                                                                            data-states="{{ json_encode($country->states()->get()) }}"
                                                                            {{ auth()->check() ?
                                                                                (!empty(auth()->user()->billing_address) ?
                                                                                    (auth()->user()->billing_address->country == $country->name ? 'selected' : '') :
                                                                                    ($country->is_default == 1 ? 'selected' : ''))
                                                                            : ($country->is_default == 1 ? 'selected' : '') }}
                                                                    >{{ $country->name }}</option>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Street Address <span>*</span></label>
                                                    <input type="text" name="billing_address"
                                                           placeholder="House number and street name"
                                                           class="billing-payment__input-on-next"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->address : '') : old('billing_address')  }}">
                                                    <input type="text" name="billing_address_2"
                                                           placeholder="Apartment, suite, unit etc. (optional)"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->address_2 : '') : old('billing_address_2')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Town / City <span>*</span></label>
                                                    <input type="text" name="billing_city"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->city : '') : old('billing_city')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>State <span>*</span></label>
                                                    <select name="billing_state" id="billing_state"
                                                            class="billing-state-select form-control"
                                                            data-placeholder="Choose state.."
                                                            data-old-value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->state : '') : old('billing_state') }}">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Zip <span>*</span></label>
                                                    <input type="text" id="billing_zip" autocomplete="false" name="billing_zip"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->billing_address) ? auth()->user()->billing_address->zip : '') : old('billing_zip')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 js-select-material-type billing-payment__shipping-address">
                                                <div class="js-smt-item">
                                                    <label class="billing-payment__shipping-label">
                                                        <input type="checkbox" id="is_different_shipping_address" name="is_different_shipping_address">
                                                        <img class="unchecked"
                                                             src="{{asset('public/images/icons/checkbox-unchecked.png')}}">
                                                        <img class="checked"
                                                             src="{{asset('public/images/icons/checkbox-checked.png')}}">
                                                        Shipping address is different then billing address
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row shipping-container" style="display: none;">
                                            <div class="col no-padding-important">
                                                <h3>
                                                    <i class="fas fa-chevron-down"></i>
                                                    Shipping Details
                                                </h3>
                                            </div>
                                            <i class="line"></i>
                                        </div>
                                        <div class="row shipping-container" style="display: none;">
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>First Name <span>*</span></label>
                                                    <input type="text" name="shipping_first_name"
                                                           value="{{ auth()->check() ? auth()->user()->first_name : old('shipping_first_name') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Last Name <span>*</span></label>
                                                    <input type="text" name="shipping_last_name"
                                                           value="{{ auth()->check() ? auth()->user()->last_name : old('shipping_last_name')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Phone <span>*</span></label>
                                                    <input type="text" name="shipping_phone"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->phone : '') : old('shipping_phone')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Email address <span>*</span></label>
                                                    <input type="text" name="shipping_email"
                                                           value="{{ auth()->check() ? auth()->user()->email : old('shipping_email')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Company Name(optional)</label>
                                                    <input type="text" name="shipping_company"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->company : '') : old('shipping_company')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Country <span>*</span></label>
                                                    <select name="shipping_country" id="shipping_country"
                                                            class="shipping-country-select form-control"
                                                            data-placeholder="Choose country..">
                                                        @if (!empty($countries))
                                                            @foreach($countries as $country)
                                                                <option value="{{ $country->name }}"
                                                                        data-country-id="{{ $country->id }}"
                                                                        data-country-code="{{ $country->code }}"
                                                                        data-country-name="{{ $country->name }}"
                                                                        data-is-default="{{ $country->is_default }}"
                                                                        data-states="{{ json_encode($country->states()->get()) }}"
                                                                        {{ auth()->check() ?
                                                                            (!empty(auth()->user()->shipping_address) ?
                                                                                (auth()->user()->shipping_address->country == $country->name ? 'selected' : '') :
                                                                                ($country->is_default == 1 ? 'selected' : ''))
                                                                        : ($country->is_default == 1 ? 'selected' : '') }}
                                                                >{{ $country->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Street Address <span>*</span></label>
                                                    <input type="text" name="shipping_address"
                                                           placeholder="House number and street name"
                                                           class="billing-payment__input-on-next"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->address : '') : old('shipping_address')  }}">
                                                    <input type="text" name="shipping_address_2"
                                                           placeholder="Apartment, suite, unit etc. (optional)"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->address_2 : '') : old('shipping_address_2')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Town / City <span>*</span></label>
                                                    <input type="text" name="shipping_city"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->city : '') : old('shipping_city')  }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>State <span>*</span></label>
                                                    <select name="shipping_state" id="shipping_state"
                                                            class="shipping-state-select form-control"
                                                            data-placeholder="Choose state.."
                                                            data-old-value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->state : '') : old('shipping_state') }}">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <div>
                                                    <label>Zip <span>*</span></label>
                                                    <input type="text" id="shipping_zip" name="shipping_zip"
                                                           value="{{ auth()->check() ? (!empty(auth()->user()->shipping_address) ? auth()->user()->shipping_address->zip : '') : old('shipping_zip')  }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row billing-payment__add-info">
                                            <div class="col">
                                                <h3>
                                                    <i class="fas fa-chevron-down"></i>
                                                    Additional Information
                                                </h3>
                                                <i class="line"></i>
                                            </div>
                                        </div>
                                        <div class="row billing-payment__order-notes">
                                            <div class="col">
                                                <label>Order notes (Optional)</label>
                                                <textarea name="notes" style="min-height: 100px;"
                                                          placeholder="Notes about your order, e.g. special notes for delivery..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class=" billing-payment--right">
                                        <div class="row">
                                            <div class="col">
                                                <h3>
                                                    <i class="fas fa-chevron-down"></i>
                                                    Total
                                                </h3>
                                                <i class="line"></i>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="billing-payment__total">
                                                    <div class="row">
                                                        <div class="col-12 billing-payment--border-bottom">
                                                            <h4 class="float-left">Product</h4>
                                                            <h4 class="float-right billing-payment__right-side">
                                                                Total</h4>
                                                        </div>
                                                        @foreach($carts as $cart)
                                                            <div class="col-12 billing-payment__item-total">
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <h5>
                                                                            <b>{{ !empty($cart->product) ? $cart->product->name : 'Custom Label' }}</b>
                                                                        </h5>
                                                                    </div>
                                                                    <div class="col-md-4 billing-payment__price">
                                                                        <h5 class="billing-payment__right-side">
                                                                            $ {{ number_format($cart->price,2) }}</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 billing-payment--border-bottom billing-payment__item-details">
                                                                <table>
                                                                    <tr>
                                                                        <td>Shape:</td>
                                                                        <td class="billing-payment__table--right">
                                                                            <strong>{{ $cart->shape->name }}</strong>
                                                                        </td>
                                                                    </tr>
                                                                    @if (empty($cart->product))
                                                                        <tr>
                                                                            <td>Design:</td>
                                                                            <td class="billing-payment__table--right">
                                                                                <strong>{{ $cart->design->name }}</strong>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                    <tr>
                                                                        <td>Size:</td>
                                                                        <td class="billing-payment__table--right">
                                                                            <strong>{{ $cart->size->name }}</strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Quantity:</td>
                                                                        <td class="billing-payment__table--right">
                                                                            <strong>{{ $cart->quantity_id }}</strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Material:</td>
                                                                        <td class="billing-payment__table--right">
                                                                            <strong>{{ $cart->material->name }}</strong>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        @endforeach
                                                        <div class="col-12 billing-payment--border-bottom">
                                                            <h4 class="float-left">Subtotal</h4>
                                                            <h4 class="float-right billing-payment__right-side">
                                                                $
                                                                <span class="subtotal_total">{{ number_format($cart_totals['subtotal'], 2) }}</span>
                                                            </h4>
                                                        </div>
                                                        <div class="col-12 billing-payment--border-bottom">
                                                            <h4 class="float-left">Tax</h4>
                                                            <h4 class="float-right billing-payment__right-side">
                                                                $
                                                                <span class="tax_total">{{ number_format($cart_totals['tax'], 2) }}</span>
                                                            </h4>
                                                        </div>
                                                        <div class="col-12 billing-payment--border-bottom">
                                                        <h4 class="float-left">Shipping</h4>
                                                        </div>
                                                        <section class="shipping_rates col-md-12">
                                                        </section>
                                                        <div class="col-12 billing-payment--border-bottom {{ !empty($cart_totals) && isset($cart_totals['discount']) && $cart_totals['discount'] > 0
                                                        ? '' : 'd-none' }}">
                                                            <h4 class="float-left">Discount</h4>
                                                            <h4 class="float-right billing-payment__right-side">-
                                                                $
                                                                <span class="discount_total">{{ number_format($cart_totals['discount'], 2) }}</span>
                                                            </h4>
                                                            <h5 class="float-left">
                                                                <span>{{ isset($cart_totals['discount_code_text']) ? $cart_totals['discount_code_text'] : '' }}</span>
                                                            </h5>
                                                        </div>
                                                        <div class="col-12">
                                                            <h6 class="float-left">Total</h6>
                                                            <h6 class="float-right billing-payment__right-side">
                                                                $ <span class="total">{{ number_format($cart_totals['total'], 2) }}</span>
                                                            </h6>

                                                            <input type="hidden" name="payment_method"
                                                                   value="Paypal">
                                                            <input type="hidden" name="coupon_id"
                                                                   value="{{ $cart_totals['coupon_id'] }}">
                                                            <input type="hidden" name="coupon_code"
                                                                   value="{{ $cart_totals['coupon_code'] }}">
                                                            <input type="hidden" name="subtotal_total"
                                                                   value="{{ $cart_totals['subtotal'] }}">
                                                            <input type="hidden" name="tax_total"
                                                                   value="{{ $cart_totals['tax'] }}">
                                                            <input type="hidden" name="tax_percentage"
                                                                   value="">
                                                            <input type="hidden" id="shipping_total" name="shipping_total"
                                                                   value="{{ $cart_totals['shipping'] }}">
                                                            <input type="hidden" name="discount_total"
                                                                   value="{{ $cart_totals['discount'] }}">
                                                            <input type="hidden" name="total"
                                                                   value="{{ $cart_totals['total'] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row row_payment" hidden>
                                            <div class="col">
                                                <h3>
                                                    <i class="fas fa-chevron-down"></i>
                                                    Payment
                                                </h3>
                                                <i class="line"></i>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 form-group">
                                                <div id="paypal-button-container"></div>
                                            </div>
                                            <!-- <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Card Name <span>*</span></label>
                                                    <input type="text" name="card_name">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Card Number <span>*</span></label>
                                                    <input type="text" name="card_number">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>Expiration Date <span>*</span></label>
                                                    <input type="text" name="card_expiration_month" style="width: 49%;"
                                                           placeholder="MM">
                                                    <input type="text" name="card_expiration_year" style="width: 46%;"
                                                           placeholder="YY">
                                                </div>
                                            </div>
                                            <div class="col-md-12 form-group">
                                                <div>
                                                    <label>CVC <span>*</span></label>
                                                    <input type="text" name="card_cvc">
                                                </div>
                                            </div> -->
                                            <div class="col-md-12">
                                                <button hidden class="billing-payment__place-order" type="button">Proceed
                                                </button>
                                            </div>

                                            <div class="col-md-12">
                                                <button id="next-checkout" class="billing-payment__place-order" type="button">Proceed
                                                </button>

                                                <button id="edit-details" hidden class="billing-payment__place-order" type="button">Edit Details
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('extrascripts')
<script>
    function csrf_token() {
    return "{{csrf_token()}}";
    }
</script>

@auth
 <script>
    function check_auth() {
    return 1;
    }
 </script>   
@endauth

@guest
 <script>
    function check_auth() {
    return 0;
    }
 </script>   
@endauth

<script src="https://www.paypal.com/sdk/js?client-id={{config('services.ppsmartbutton.client_id')}}&components=buttons&currency=USD"></script>

{{-- <script src="https://www.paypal.com/sdk/js?client-id=AQ1i_04eC8grkm0vP3WV8a5zNERX35LHJCBX0t_92kMdMPcV1Fyqo4pq0VRPne7V4r7ikMU0hEf56Nu_&components=buttons&currency=USD"></script> --}}

{{-- <script src="https://www.paypal.com/sdk/js?client-id=AVOvN2EuiuPvaiR1Zenb2piaEDAlxY-oCx4eJRhcgLPtlEJL-6SsvBcOTfZnO7m_jcu8L1A1QSCreCr4&components=buttons&currency=USD"></script> --}}
<script type="text/javascript" src="{{ asset('public/js/libraries/front_checkout.js') }}"></script>

@endpush