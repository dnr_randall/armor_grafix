<h2>Order Details</h2>
<ul class="detail-container">
    <li class="shape-detail-container">
        <i class="check"></i>
        <label>Shape:</label>
        <span class="shape-name"></span>
    </li>
    <li class="size-detail-container ">
        <i class="check"></i>
        <label>Size & Quantity:</label>
        <span>
            <span class="size-name"></span>
            <span class="quantity-name"></span>
        </span>
    </li>
    <li class="design-detail-container ">
        <i class="check"></i>
        <label>Design:</label>
        <span>
            <span class="design-name"></span>
        </span>
    </li>
    <li class="material-detail-container ">
        <i class="check"></i>
        <label>Material:</label>
        <span>
            <span class="material-name"></span>
            <span class="include-tamper-evident"></span>
        </span>
    </li>
</ul>
<input type="hidden" name="price" value="0">
<input type="hidden" name="add_to_cart_type">
<input type="hidden" name="is_customized" value="1">
<button type="submit" class="checkout-btn" style="display: none;">Checkout</button>

<a href="javascript:void(0)" class="js-design-label-next next-step-btn">Next</a>
<a href="javascript:void(0)" class="js-design-label-back prev-step-btn">Back</a>
<a href="javascript:void(0)" class="back-to-parent-shape" style="display: none;">Back</a>

<div class="custom-text">
    {!! $item->page_sections()->where('section', 'bottom_section_content')->first()->content ?? '' !!}

    <a href="{!! $item->page_sections()->where('section', 'bottom_section_button_link')->first()->content ?? '' !!}">
        <i class="checklist"></i>
        {!! $item->page_sections()->where('section', 'bottom_section_button_label')->first()->content ?? '' !!}
    </a>
</div>
