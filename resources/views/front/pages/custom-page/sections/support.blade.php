<section class="product-details for-page-support">
    <div class="product-details__wrapper container container-md-important">
        <div class="product-details__wrapper--row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-5" data-aos="fade-right" data-aos-duration="2000">
                        <h2>{!! $item->page_sections()->where('section', 'support_title')->first()->content ?? '' !!}</h2>
                        <h1>{!! $item->page_sections()->where('section', 'support_note')->first()->content ?? '' !!}</h1>
                        <br>
                        <div class="contact-form">
                            {{  Form::open([
                                'method' => 'POST',
                                'id' => 'create-contact',
                                'route' => ['contact.store'],
                                'class' => '',
                                ])
                            }}
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="{{--form-control--}} " placeholder="Name"
                                       name="name">
                                @if($errors->has('name'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="text" class="{{--form-control--}} " placeholder="Email"
                                       name="email">
                                @if($errors->has('email'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('subject') }}</span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                                <input id="subject" type="text" class="{{--form-control--}} " placeholder="Subject"
                                       name="subject">
                                @if($errors->has('subject'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('subject') }}</span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea id="message" placeholder="Message" name="message"
                                          class="{{--form-control--}}"></textarea>
                                @if($errors->has('message'))
                                    <span class="help-block animation-slideDown">{{ $errors->first('message') }}</span>
                                @endif
                            </div>
                            <div class="">
                                <button type="submit" class="submit">SEND</button>
                            </div>
                            {{ Form::close() }}
                        </div>

                        {{-- <div class="product-details__cta">
                            {!! $item->page_sections()->where('section', 'section_1_content')->first()->content ?? '' !!}
                        </div> --}}
                    </div>

                    <div class="col-md-3" data-aos="fade-right" data-aos-duration="2000">
                        <div class="contact-details">
                            <h3>Contact Info</h3>

                            <div class="contact-details-items">
                                Phone: <a href="tel:{{ $seo_meta['phone']}}">{{ $seo_meta['phone']}}</a><br>
                                Email: <a href="{{ $seo_meta['email']}}">{{ $seo_meta['email']}}</a><br>
                                Mailing Address: {{ $seo_meta['address']}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4"
                      data-aos="fade-left"
                      data-aos-duration="2000">
                        <div class="product-details__zoom">
                            {{-- <img src="{{asset('public/images/sections/product-details/main-photo.png')}}"> --}}
                            <div class="mapouter">
                                <div class="gmap_canvas">
                                    {!! $item->page_sections()->where('section', 'google_map_link')->first()->content ?? '' !!}
                                    <a href="https://www.bitgeeks.net/embed-google-map/"></a></div>
                                <style>.mapouter {
                                        position: relative;
                                        text-align: right;
                                        height: 567px;
                                        width: 720px;
                                    }

                                    .gmap_canvas {
                                        overflow: hidden;
                                        background: none !important;
                                        height: 567px;
                                        width: 720px;
                                        text-align: left;
                                    }
                                    .gmap_canvas iframe {
                                        width: 100% !important;
                                    }

                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/libraries/contacts.js') }}"></script>
@endpush