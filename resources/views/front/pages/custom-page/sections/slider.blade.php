<section class="banner">
    <div class="banner__slick">

            <div class="banner__item image-background">
                <img src="{{ url('public/images/banner.jpg') }}">
                <div class="banner__content container">
                    <div class="banner__content--row row">
                        <div class="col-md-12">
                            <div class="banner__content">

                                    <h1> Title here
                                        Repairs & Services </h1>
                                    <a href="javascript:void(0)" class="btn btn--primary"> Button </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="banner__item image-background">
                    <img src="{{ url('public/images/banner.jpg') }}">
                    <div class="banner__content container">
                        <div class="banner__content--row row">
                            <div class="col-md-12">
                                <div class="banner__content">

                                        <h1> Title here 2
                                            Repairs & Services </h1>
                                        <a href="javascript:void(0)" class="btn btn--primary"> Button </a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

    </div>
</section>