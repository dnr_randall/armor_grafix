<section class="cart-details">
	<div class="cart-details__wrapper container container-lg2-important">
		<div class="cart-details__wrapper--row row">
			<div class="col-md-12">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 offset-md-1">
							<h1>
								Shopping Cart
							</h1>
						</div>
						<div class="col-md-10 offset-md-1">
							<div class="cart-details__table-items">
								<div class="row cart-details__table--columns cart-details__table--header">
									<div class="col-md-6">
										<span>Product</span>
									</div>
									<div class="col-md-2">
										<span>Price</span>
									</div>
									<div class="col-md-2">
										<span>Quantity</span>
									</div>
									<div class="col-md-2">
										<span>Total</span>
									</div>
								</div>
								<div class="details-item-wrap">
								@if (!empty($carts) && count($carts))
									@foreach($carts as $cart)
										<div class="row cart-details__table--columns cart-details__table--data">
											<div class="col-md-6">
												<span class="mobile-cart-title">Product</span>
												<div class="row">
													<div class="col-md-1 d-flex justify-content-center align-items-center">
														<a href="javascript:void(0)" class="delete-cart-btn" data-cart-id="{{ $cart->id }}" data-cart-route="{{ route('carts.delete', $cart->id) }}">
															<b><i class="remove"></i>X</b>
														</a>
													</div>
													<div class="col-md-2 text-center no-padding-important product-thumbnail-mobile">
														@php
															$image = '';
															if (!empty($cart->product)) {
																$default_image = $cart->product->product_images()->orderBy('is_default', 'desc')->first();
																if (!empty($default_image)) {
																	$image = $default_image->file;
																}
															} else {
																$default_image = $cart->design;
																if (!empty($default_image)) {
																	$image = $default_image->image;
																}
															}

                                                            $qty = !empty($cart->size) ?
                                                                $cart->size
                                                                ->quantities()
                                                               	->first() : NULL;
														@endphp
														<div class="img-holder">
															<div class="img-item">
																<img src="{{ asset($image) }}">
															</div>
														</div>
													</div>
													<div class="col-md-9 cart-details__table__item-info cart-details-mobile">
{{--														<h2>Create Your Label -<b>Lorem Ipsum</b></h2>--}}
														<h2><b>{{ !empty($cart->product) ? $cart->product->name : 'Custom Label' }}</b></h2>
														<table>
															<tr>
																<td>Shape:</td>
																<td>{{ $cart->shape->name }}</td>
															</tr>
															@if (empty($cart->product))
																<tr>
																	<td>Design:</td>
																	<td>{{ $cart->design->name }}</td>
																</tr>
															@endif
															<tr>
																<td>Size:</td>
																<td>{{ $cart->size->name }}</td>
															</tr>
															<tr>
																<td>Quantity:</td>
																<td>{{ $cart->quantity_id }} ({{ $cart->quantity ? $cart->quantity->label : '25,000' }} Label / Roll)</td>
															</tr>
															<tr>
																<td>Material:</td>
																<td>{{ $cart->material->name }}</td>
															</tr>
														</table>
													</div>
												</div>
											</div>
											<div class="col-md-2 cart-price-section">
												<span class="mobile-cart-title">Price</span>
												<h2>
													$ {{ number_format($cart->price / $cart->quantity_id,2)}}
												</h2>
											</div>
											<div class="col-md-2 cart-quantity-section">
												<span class="mobile-cart-title">Quantity</span>
												{{  Form::open([
													'method' => 'PUT',
													'id' => 'edit-product-from-cart',
													'route' => ['carts.update', $cart->id],
													'class' => 'edit-product-from-cart',
													/*'files' => TRUE*/
													])
												}}
													<div class="form-group {{ $errors->has('quantity_id') ? ' has-error' : '' }}">
														<input data-price="{{ $cart->price }}" class="form-control" type="text" name="quantity_id" value="{{ $cart->quantity_id }}">
														@if($errors->has('quantity_id'))
															<span class="help-block animation-slideDown">{{ $errors->first('quantity_id') }}</span>
														@endif
													</div>
													<button type="submit"><i class="refresh"></i></button>
												{{ Form::close() }}
											</div>
											<div class="col-md-2 cart-total-section">
												<span class="mobile-cart-title">Total</span>
												<h2>
													$ {{ number_format($cart->price,2) }}
													<!-- $ {{ number_format($cart_totals['total'],2) }} -->
												</h2>
											</div>
										</div>
									@endforeach
								@else
									<div class="row cart-details__table--columns cart-details__table--data">
										<div class="alert alert-warning">
											No cart found!
										</div>
									</div>
								@endif
								</div>
							</div>
						</div>
						<div class="col-md-4 offset-md-1 no-padding-important">
							{{  Form::open([
								'method' => 'POST',
								'id' => 'validate-coupon-code',
								'route' => ['coupon_codes.validate'],
								'class' => '',
								/*'files' => TRUE*/
								])
							}}
								<div class="cart-details__coupon form-group">
                                    <div>
                                        <input type="text" name="coupon_code" placeholder="Enter coupon code">
                                        <button type="submit">Apply Coupon</button>
                                    </div>
                                </div>
							{{ Form::close() }}
						</div>
						
						<div class="col-md-3 offset-md-3">
							<div class="cart-details__total">
								<div class="row">
									<div class="col-12">
										<h2>Subtotal</h2>
										<h2>$ {{ number_format($cart_totals['subtotal'],2)  }}</h2>
									</div>
									<div class="col-12">
										<i class="line"></i>
									</div>
									@if (!empty($cart_totals) && isset($cart_totals['shipping']) && $cart_totals['shipping'] > 0)
										<div class="col-12">
											<h2>Shipping</h2>
											<h2>$ {{ $cart_totals['shipping'] }}</h2>
										</div>
										<div class="col-12">
											<i class="line"></i>
										</div>
									@endif
									@if (!empty($cart_totals) && isset($cart_totals['discount']) && $cart_totals['discount'] > 0)
										<div class="col-12">
											<h2>Discount</h2>
											<h2>- $ {{ $cart_totals['discount'] }}</h2>
										</div>
										<div class="col-12">
											<span>{{ isset($cart_totals['discount_code_text']) ? $cart_totals['discount_code_text'] : '' }}</span>
											<h2></h2>
										</div>
										<div class="col-12">
											<i class="line"></i>
										</div>
									@endif
									@if (!empty($cart_totals) && isset($cart_totals['tax']) && $cart_totals['tax'] > 0)
										<div class="col-12">
											<h2>Tax</h2>
											<h2>$ {{ $cart_totals['tax'] }}</h2>
										</div>
										<div class="col-12">
											<i class="line"></i>
										</div>
									@endif
									<div class="col-12">
										<h2>Total</h2>
										<h2>$ {{ number_format($cart_totals['total'],2) }}</h2>
									</div>
								</div>
							</div>
						
							<div class="cart-details__final-buttons">

								@auth
									<a href="{{ url('checkout') }}" class="dark">Proceed To Checkout <i class="white-caret-right"></i></a>
								@endauth

								@if (!auth()->check())
									<a href="{{ url('customer/login?redi=true') }}" class="dark">Proceed To Checkout <i class="white-caret-right"></i></a>
									<a href="{{ url('checkout') }}">Checkout As Guest</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@push('extrascripts')
<script type="text/javascript" src="{{ asset('public/js/admin.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/js/libraries/front_carts.js') }}"></script>
@endpush