{{-- HP BANNER --}}
<section id="hpBanner">
    <h2 class="hidden">HP BANNER SECTION</h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 np">
                <div class="home-slider">
                    @foreach($home_slides as $home_slide)
                         <div class="c8 eg1 m41 e5" style="background: url('{{ asset($home_slide->background_image) }}')">
                            <div class="c10 m49">
                                <div class="c13 m43" copied>
                                    {!! $home_slide->short_description !!}
                                </div>
                            </div>
                            <div class="c9 m42">
                                {!! $home_slide->title !!}
                            </div>
                            <div class="c14 m46">
                                <a href="{!! url($home_slide->button_link_1) !!}">
                                    <div class="c15 m47" copied>
                                        {!! $home_slide->button_label_1 !!}
                                    </div>
                                </a>
                                <a href="{!! url($home_slide->button_link_2) !!}">
                                    <div class="c16 m48" copied>
                                        {!! $home_slide->button_label_2 !!} &nbsp; <span><i class="fa fa-play"></i></span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="tagline-center">
                    {!! $home_slides[0]->bottom_short_description !!}
                </div>
            </div>
        </div>
    </div>
    <div class="banner-smi-wrap">
        <a href="https://www.facebook.com/NanoGrafixCorporation" target="_blank"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.youtube.com/channel/UC4d1---JOzBVDHl39y8dWig" target="_blank"><i class="fab fa-youtube"></i></a>
        <a href="https://www.linkedin.com/company/nanografix" target="_blank"><i class="fab fa-linkedin-in"></i></a>
        {{-- <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="https://twitter.com" target="_blank"><i class="fab fa-twitter"></i></a>  --}}
    </div>  
</section>  
{{-- END OF HP BANNER --}}