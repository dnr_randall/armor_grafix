<section class="product-details">
	<div class="product-details__wrapper container container-md-important">
		<div class="product-details__wrapper--row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6"
				            data-aos="fade-right"
				            data-aos-duration="2000">
						<div class="product-details__zoom product-image-slider-large product-nav">
							@foreach($product->product_images()->orderBy('is_default', 'desc')->get() as $image_key => $image)
								<a href="{{ asset($image->file) }}">
									<img src="{{ asset($image->file) }}" class="zoom {{ $image_key == 0 ? 'active' : '' }}">
								</a>
							@endforeach
						</div>
						<div class="product-details__list">
							<ul class="product-image-slider-small product-nav">
								@foreach($product->product_images()->orderBy('is_default', 'desc')->get() as $image_key => $image)
									<li class="{{ $image_key == 0 ? 'active' : '' }}">
										<img src="{{ asset($image->file) }}">
									</li>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-md-6"
				            data-aos="fade-left"
				            data-aos-duration="2000">
						<h2>{!! $product->shape->name !!}</h2>
						<h1>{!! $product->name !!}</h1>
						@php
							$qty = !empty($product->size) ?
								$product->size
								->quantities()
								->first() : NULL;
						@endphp
						<label>Starting at: <span class="price-big">${!! number_format(($product->price), 2) !!}</span> / Roll</label>
						<div class="description-container">
							{!! $product->description !!}
						</div>
						<div class="product-details__buttons">
							{{  Form::open([
								'method' => 'POST',
								'id' => 'add-product-to-cart',
								'route' => ['carts.store'],
								'class' => '',
								/*'files' => TRUE*/
								])
						  	}}
							<input type="hidden" name="product_id" value="{{ $product->id }}">
							<input type="hidden" name="shape_id" value="{{ $product->shape->id }}">
							<input type="hidden" name="size_id" value="{{ $product->size->id }}">
							<input type="hidden" name="material_id" value="{{ $product->material->id }}">
							<input type="hidden" name="price" value="{{ $product->price }}">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group {{ $errors->has('quantity_id') ? ' has-error' : '' }}">
										<label for="">Quantity : 
										<input type="text" name="quantity_id" value="1">
										<div class="form-group form-actions">
											<button type="submit" class="btn btn-sm btn-primary">Add to cart <i class="fa fa-caret-right"></i></button>
										</div>
										</label> 
									</br>
										@if($errors->has('quantity_id'))
											<span class="help-block animation-slideDown">{{ $errors->first('quantity_id') }}</span>
										@endif
									</div>
								</div>
							</div>
							{{ Form::close() }}
						</div>
						<p class="product-details__sku"><strong>SKU:</strong> {!! $product->sku !!}</p>
						<i class="line"></i>
						<div class="product-details__cta">
							{!! $item->page_sections()->where('section', 'product_details_notes')->first()->content ?? '' !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@push('extrascripts')
<script>
    $('.description-container').readmore({
        speed: 75,
        moreLink: '<a href="javascript:void(0)" style="display: inline-block !important;width: auto !important;">Read more</a>',
        lessLink: '<a href="javascript:void(0)" style="display: inline-block !important;width: auto !important;">See less</a>',
        collapsedHeight: 65,
    });
</script>
<script type="text/javascript" src="{{ asset('public/js/libraries/front_carts.js') }}"></script>
@endpush