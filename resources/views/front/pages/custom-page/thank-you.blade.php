@extends('front.layouts.base')

@section('content')

@include('front.layouts.sections.header')
<main role="main" class="page-thankyou">
    <div class="page-thankyou--section">
        <div class="container">
            <h1>Thank you!</h1>
            <h2>Your order was completed successfully.</h2>

            <div class="note-one">
                <i class="far fa-envelope"></i>
                <p>An email receipt including the details about your order has been sent to the email address provided. Please keep it for your records.</p>
            </div>

            <div class="continue-shopping">
                <a href="{{url('products')}}">Continue Shopping</a>
            </div>
        </div>
    </div>
</main>
{{-- @include('front.layouts.sections.footer') --}}

@endsection

