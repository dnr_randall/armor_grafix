@include('front.layouts.sections.header')
<main role="main" class="page-design-label">
	@include('front.pages.custom-page.sections.design-label-steps')
	@include('front.pages.custom-page.sections.cta-request')
</main>
@include('front.layouts.sections.footer')

@push('extrascripts')
	<script>


		$('.next-step-btn').on('click',function()
		{
			if($(this).hasClass("thisisstep2"))
			{

			}
			else 
			{
				if($('.design-detail-container').hasClass('active') || $('.size-detail-container').hasClass('active'))
				{

				}
				else
				{
					$(".next-step-btn").removeClass('disabled');
					$('.div_label_image').attr('hidden',true);
					$("#price_label").html('');
					$("#label_roll").html('');
					$(".sec-bulk-prices").html('');
				}
			}
		});

		$('.size_id').on('click',function ()
		{
			$('.div_label_image').attr('hidden',true);
			var size_id = $(this).val();
			$('#div_'+size_id+'').attr('hidden',false);
			// alert();
		});
	</script>
    <script>
		new Tab('.page-design-label');
	</script>
<script type="text/javascript" src="{{ asset('public/js/libraries/front_carts.js') }}"></script>
@endpush
