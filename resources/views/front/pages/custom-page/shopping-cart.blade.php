@include('front.layouts.sections.header')
<main role="main" class="page-shopping-cart">
	@include('front.pages.custom-page.sections.cart-details')
</main>
@include('front.layouts.sections.footer')
