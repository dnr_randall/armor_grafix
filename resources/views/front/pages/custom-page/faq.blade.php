@include('front.layouts.sections.header')
<main role="main" class="page-faq">
	@php
		$image = $item->page_sections()->where('section', 'banner_image')->first()->content;
	@endphp
	@subbanner(['image'=>asset($image)])
	@slot('h1')
		Frequently Asked Questions
	@endslot
	@slot('p')
		{!! $item->page_sections()->where('section', 'banner_description')->first()->content ?? '' !!}
	@endslot
	@endsubbanner
	@include('front.pages.custom-page.sections.cta-faq')
	@include('front.pages.custom-page.sections.faq-items')
	@include('front.pages.custom-page.sections.faq-links')
</main>
@include('front.layouts.sections.footer')
