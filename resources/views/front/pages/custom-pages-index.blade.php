@extends('front.layouts.base')

@section('content')
    @if (!empty($page))
        <?php $item = $page; ?>
        {{-- need to be updated to live slugs or by page type --}}
        @if ($page['slug'] == 'home')
            @include('front.pages.custom-page.home')
            
        @elseif ($page['slug'] == 'contact-us')
            @include('front.pages.custom-page.contact-us')
            
        @elseif ($page['slug'] == 'about-us')
            @include('front.pages.custom-page.about-us')

        @elseif ($page['slug'] == 'products')
            @if (!empty($product))
                @include('front.pages.custom-page.product-details')
            @else
                @include('front.pages.custom-page.products')
            @endif

        @elseif ($page['slug'] == 'product-details')
            @include('front.pages.custom-page.product-details')

        {{-- @elseif ($page['slug'] == 'thank-you')
            @include('front.pages.custom-page.thank-you') --}}
            
        @elseif ($page['slug'] == 'design-label')
            @include('front.pages.custom-page.design-label')
            
        @elseif ($page['slug'] == 'gallery')
            @include('front.pages.custom-page.gallery')
            
        @elseif ($page['slug'] == 'faq')
            @include('front.pages.custom-page.faq')
            
        @elseif ($page['slug'] == 'shopping-cart')
            @include('front.pages.custom-page.shopping-cart')
            
        @elseif ($page['slug'] == 'checkout')
            @include('front.pages.custom-page.checkout')
            
        @elseif ($page['slug'] == 'support')
            @include('front.pages.custom-page.support')

        @elseif ($page['slug'] == 'email-order-sample')
            @include('front.pages.custom-page.email-order-sample')

        @else
            @include('front.pages.custom-page.default-page')
        @endif
    @else
        @include('front.pages.custom-page.home')
    @endif
@endsection
