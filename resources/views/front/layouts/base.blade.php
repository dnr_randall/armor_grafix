<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{!! $seo_meta['title'] !!}</title>

    <meta name="description" content="{!! $seo_meta['description'] !!}">
    <meta name="author" content="{!! $seo_meta['author'] !!}">
    <meta name="robots" content="{!! $seo_meta['robots'] !!}">
    <meta name="keywords" content="{!! $seo_meta['keywords'] !!}">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{!! $seo_meta['title'] !!}">
    <meta property="og:description" content="{!! $seo_meta['description'] !!}">
    <meta property="og:url" content="{!! url('') !!}">
    <meta property="og:site_name" content="{!! $seo_meta['name'] !!}">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    {{--Icons--}}
    <!-- <link rel="shortcut icon" href="{{ asset('public/images/icons/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon57.png') }}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon72.png') }}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon76.png') }}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon114.png') }}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon144.png') }}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ asset('public/images/icons/icon180.png') }}" sizes="180x180">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&display=swap" rel="stylesheet"> -->
    <link rel="shortcut icon" href="{{ asset('public/images/favicon/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('public/images/favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('public/images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('public/css/app.css') }}">

    {{--<link rel="stylesheet" href="{{ asset('public/node_modules/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('public/node_modules/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}">--}}
    @stack('extrastylesheets')

    <script src="{{asset('public/js/modernizr.min.js')}}"></script>

</head>
<body>
@yield('content')
<script type="text/javascript">
    var sBaseURI = '{{ url('/') }}';
</script>
<script src="{{ asset('public/js/app.js') }}"></script>
{{--<script type="text/javascript" src="{{asset('public/node_modules/jquery.lazy-master/jquery.lazy-master/jquery.lazy.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('public/node_modules/jquery.lazy-master/jquery.lazy-master/jquery.lazy.plugins.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('public/node_modules/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>--}}

<script type="text/javascript">
</script>
@stack('extrascripts')

@if (session()->has('flash_message'))
    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            swal({
                title: "{!! session('flash_message.title') !!}",
                text: "{!! session('flash_message.message') !!}",
                type: "{!! session('flash_message.type') !!}",
                allowEscapeKey: true,
                allowOutsideClick: true,
            }, function () {
            });
        });
    </script>
@endif
</body>
</html>