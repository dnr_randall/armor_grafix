@if(!isset($has_no_sub_banner))
    <div class="footer container-fluid c68 c68i1 m98"
         data-aos="fade-up"
         data-aos-duration="800">
        <div class="row">
            <div class="col-12 footer__cta c69">
                <div class="row">
                    <div class="col-sm-3 c71" data-src="{{ asset($seo_meta['need_help_section_image']) ?? asset('/public/img/ms.j.jpg') }}"
                         style="background:url('{{ asset($seo_meta['need_help_section_image']) ?? asset('/public/img/ms.j.jpg') }}');"></div>
                    {!! $seo_meta['need_help_section_content'] !!}
                </div>
            </div>
        </div>
    </div>
@endif
<div class="container-fluid c88 eg1">
    <div class="row">
        <div class="col-md-3">
            <div class="c89"><a href="{{ url('/') }}"><img src="{{asset('/public/img/header-logo.png')}}"></a></div>
            <div class="c90">
                {{ $seo_meta['copyright']}} </br> Web Design by <a href="https://dogandrooster.com/" class="c91">Dog and Rooster, Inc.</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="s1">Contact Us</div>
            <div class="c90 v2">
                <p>
                    Phone: <a href="tel:{{ $seo_meta['phone']}}">{{ $seo_meta['phone']}}</a><br>
                    Email: <a href="mailto:{{ $seo_meta['email']}}">{{ $seo_meta['email']}}</a><br>
                    Mailing Address: {{ $seo_meta['address']}}
                </p>
            </div>
        </div>
        <div class="col-md-3 holographic-labels">
            <div class="s1">Main Links</div>
            <div class="s2"><a href="{{url('home')}}">Home</a></div>
            <div class="s2"><a href="{{url('products')}}">Products</a></div>
            {{-- <div class="s2"><a href="{{url('faq')}}">FAQ</a></div> --}}
            <div class="s2"><a href="{{url('gallery')}}">Gallery</a></div>
            <div class="s2"><a href="{{url('support')}}">Contact us</a></div>
            <div class="s2"><a href="{{url('design-label')}}">Create A Custom Label</a></div>
        </div>
        <div class="col-md-3 smi">
            <div class="c93 m59"></div>
            <div class="s1">Follow Us</div>
            <div class="g15"><a href="{{ $seo_meta['facebook_link'] }}" target="_blank"><img src="{{asset('/public/img/footer-fb-purple.png')}}"></a></div>
            <div class="g15"><a href="{{ $seo_meta['youtube_link'] }}" target="_blank"><img src="{{asset('/public/img/footer-youtube-purple.png')}}"></a></div>
            <div class="g15"><a href="{{ $seo_meta['linkedin_link'] }}" target="_blank"><img src="{{asset('/public/img/footer-linkedin-purple.png')}}"></a></div>
        </div>
    </div>
</div>