<div class="container-fluid c1 m13 np">
    <div class="row np">
        <div class="col-md-12 np c27 m24">
            <div class="c3">
                <a class="navbar-brand c2" href="{{url('/')}}">
                    <img class="m11" src="{{asset('/public/img/header-logo.png')}}" alt="Logo">
                </a>
            </div>
            <div class="g1 m14">
                <div class="c6 m6">
                    <div class="g3 m15 e1 c95">
                        <i class="fa fa-bars"></i>
                    </div>
                    <div class="c96 m31 e2">

                    <div class="c103">
                            <a href="{{url('products')}}">
                                <div class="c97 m30 g13 g1">
                                    <div class="c99"><img class="m21" src="{{asset('/public/img/nav-dot-new.png')}}" alt="">
                                    </div>
                                    <div class="c98">Shop Online</div>
                                </div>
                            </a>
                        </div>
                        <div class="c103">
                            <a href="{{url('design-label')}}">
                                <div class="c97 m30 g13 g1">
                                    <div class="c99"><img class="m21" src="{{asset('/public/img/nav-dot-new.png')}}" alt="">
                                    </div>
                                    <div class="c98">Start Designing Your Label</div>
                                </div>
                            </a>
                        </div>

                        <div>
                            <a href="{{url('gallery')}}" class="m17 g13 g16">Gallery</a>
                        </div>
                        <div>
                            <a href="{{url('support')}}" class="m27 g13 g16">Support</a>
                        </div>
                        
                        <div class="m33 c104">
                            <div class="c105 m26">
                                <img src="{{asset('/public/img/usericon.jpg')}}" alt="">
                            </div>
                            @if (auth()->check())
                                <div class="c106">
                                    <a href="{{ route('customer.dashboard') }}" class="m34 g13 g16 c107">ACCOUNT</a>
                                </div>
                                <div class="c22">
                                    <a href="#" js-login-dropdown-target="register">
                                        <i id="js-login-dropdown-id-trigger" class="fa fa-caret-down"></i>
                                    </a>
                                </div>
                                <a id="js-login-dropdown-id-register" href="{{ route('customer.logout') }}"
                                   class="header__login-dropdown" js-login-dropdown-id="register">
                                    Logout
                                </a>
                            @else
                                <div class="c106">
                                    <a href="{{ route('customer.login') }}" class="m34 g13 g16 c107">Login</a>
                                </div>
                                <div class="c22">
                                    <a href="#" js-login-dropdown-target="register">
                                        <i id="js-login-dropdown-id-trigger" class="fa fa-caret-down"></i>
                                    </a>
                                </div>
                                <a id="js-login-dropdown-id-register" href="{{ route('customer.register') }}"
                                   class="header__login-dropdown" js-login-dropdown-id="register">
                                    Register
                                </a>
                            @endif
                        </div>
                        <div class="m29">
                            <a href="tel:{{ $seo_meta['phone']}}">
                                <div class="c100 m32">
                                    <img src="{{asset('/public/img/telephoneicon.jpg')}}" alt="">
                                </div>
                                <div class="c101">
                                    {{ $seo_meta['phone']}}
                                </div>
                            </a>
                        </div>
                        <div class="m36 c109">
                            <a href="#">
                                <div class="c108">
                                    <img src="{{asset('/public/img/carticon.jpg')}}" alt="">
                                </div>
                                <div class="c110 g13">
                                    Go to Shopping Cart
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="m7">
                    <a href="{{url('products')}}" class="c102">
                        <div class="c4 m22">
                            <div class="c18 m23"><img class="m21" src="{{asset('/public/img/nav-dot-new.png')}}" alt="">
                            </div>
                            <div class="c19 m20">Shop Online</div>
                        </div>
                    </a>
                </div>
                <div class="m7">
                    <a href="{{url('design-label')}}" class="c102">
                        <div class="c4 m22">
                            <div class="c18 m23"><img class="m21" src="{{asset('/public/img/nav-dot-new.png')}}" alt="">
                            </div>
                            <div class="c19 m20">Start Designing Your Label</div>
                        </div>
                    </a>
                </div>
                <a href="{{url('gallery')}}" class="m2 g13 mg1">Gallery</a>
                <a href="{{url('support')}}" class="m5 g13 mg1">Support</a>
                
                <a href="tel:{{ $seo_meta['phone']}}" class="g2 m9 g13">
                    <div class="g3">
                        <img src="{{asset('/public/img/telephoneicon.jpg')}}" alt="">
                    </div>
                </a>
                <div class="c7 g2 m8">
                    <div class="c20 m26">
                        <img src="{{asset('/public/img/usericon.jpg')}}" alt="">
                    </div>
                    @if (auth()->check())
                        <div class="c21 m25">
                            <a href="{{ route('customer.dashboard') }}">ACCOUNT</a>
                        </div>
                        <div class="c22">
                            <a href="#" js-login-dropdown-target="register">
                                <i id="js-login-dropdown-id-trigger" class="fa fa-caret-down"></i>
                            </a>
                        </div>
                        <a id="js-login-dropdown-id-register" href="{{ route('customer.logout') }}"
                           class="header__login-dropdown" js-login-dropdown-id="register">
                            Logout
                        </a>
                    @else
                        <div class="c21 m25">
                            <a href="{{ route('customer.login') }}">LOGIN</a>
                        </div>
                        <div class="c22">
                            <a href="#" js-login-dropdown-target="register">
                                <i id="js-login-dropdown-id-trigger" class="fa fa-caret-down"></i>
                            </a>
                        </div>
                        <a id="js-login-dropdown-id-register" href="{{ route('customer.register') }}"
                           class="header__login-dropdown" js-login-dropdown-id="register">
                            Register
                        </a>
                    @endif
                </div>
                <a href="{{url('shopping-cart')}}" class="g2 c5 m10 g13 e4">
                    <div class="g3 notification-container {{ !empty($cart_count) && $cart_count ? '' : 'empty' }}"
                         data-count="{{ !empty($cart_count) ? $cart_count : '' }}">
                        <img src="{{asset('/public/img/carticon.jpg')}}" alt="">
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="five-lines-background"></div>