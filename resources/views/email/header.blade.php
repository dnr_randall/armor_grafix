<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{!! $seo_meta['title'] !!}</title>
    <meta name="viewport" content="width=device-width"/>

{{--    <link rel="stylesheet" href="{{ asset('public/css/app.css') }}">--}}
    <style>

        .email-order-content{
        .information{
            margin: 0 auto;
            width: 600px;
            background: #ffffff;
            padding: 50px 20px;
            border: 2px dashed #8a558d;

        h3.thank-you{
            text-align: center;
            text-transform: uppercase;
            font-weight: 700;
            color: #6d6d6d;
        }

        img{
            text-align: center;
            margin: 0 auto;
            display: block;
        //filter: brightness(0) invert(1);
        }

        .introduction{
            margin-top: 30px;
            color: #8a558d;
        }

        &__order-details,
        &__product-information,
        &__billing-details,
        &__shipping-details{
             padding: 20px 0;
             border-top: 1px solid #905e93;
             color: #8a558d;

        h3{
            text-align: center;
            font-weight: 700;
            font-size: 18px;
            color: #6d6d6d;
            text-transform: uppercase;
            margin-bottom: 18px;
        }

        &--order-number,
        &--order-date,
        &--subtotal,
        &--tax,
        &--shipping,
        &--total,
        &--payment-method,
        &--shipping-method,
        &--notes{
             display: inline-block;
             width: 48%;
             vertical-align: top;
             margin-bottom:15px;

        h4{
            font-size: 16px;

        span{
            font-weight: 700;
            text-transform: uppercase;
        }
        }
        }

        &--name,
        &--shape,
        &--size,
        &--material,
        &--quantity,
        &--price,
        &--comments,
        &--tamper{
             margin-bottom:15px;

        h4{
            font-size: 16px;

        span{
            font-weight: 700;
            text-transform: uppercase;
        }
        }
        }

        &--to,
        &--company,
        &--address,
        &--email,
        &--phone{
             margin-bottom:15px;

        h4{
            font-size: 16px;

        span{
            font-weight: 700;
            text-transform: uppercase;
        }
        }
        }
        }
        }

        footer{
            padding: 20px;
            margin: 0 auto;
            width: 600px;
            background: #8a558d;

        p{
            text-align: center;
            color: #fff;
            font-size: 15px;
            margin-bottom: 0;
        }
        }
        }
    </style>

    <style type="text/css">

        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0 !important;
            }

            body[yahoo] .button a {
                background-color: #1ec1b8;
                padding: 15px 25px !important;
            }
        }

        @media only screen and (min-device-width: 601px) {
            .content {
                width: 600px !important;
            }

            .col387 {
                width: 387px !important;
            }
        }
    </style>
</head>