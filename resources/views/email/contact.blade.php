<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{!! $seo_meta['title'] !!}</title>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0 !important;
            }

            body[yahoo] .button a {
                background-color: #1ec1b8;
                padding: 15px 25px !important;
            }
        }

        @media only screen and (min-device-width: 601px) {
            .content {
                width: 600px !important;
            }

            .col387 {
                width: 387px !important;
            }
        }
    </style>
</head>
<body bgcolor="#32323a" style="font-family: Roboto, sans-serif; margin: 0; padding: 0;" yahoo="fix" class="email-order-content">
    
    <div class="information" style="margin: 0 auto; width: 600px; background: #ffffff; padding: 50px 20px; border: 2px dashed #8a558d;">
        <img class="m11" src="{{asset('/public/img/header-logo.png')}}" alt="Logo" style="text-align: center; margin: 0 auto; display: block;">
        <div class="introduction" style="font-family: Roboto, sans-serif; margin-top: 30px; color: #8a558d;">
            {{-- <h3 style="font-family: Roboto, sans-serif; font-size: 1.75rem;">Dear {{ $data['user']['name'] }},</h3>
            <p>We have received your order and we will respond without delay.</p> --}}
            <b>Hi {{ isset($data['user_data']) ? $data['user_data']['name'] : '' }},</b>
            <br>
            <span>Thanks for contacting us at {{ $seo_meta['name'] }}.</span>
        </div>

        <div class="information__product-information" style="padding: 20px 0; border-top: 1px solid #905e93; color: #8a558d;">
            <h3 style="text-align: center; font-weight: 700; font-family: Roboto, sans-serif; font-size: 18px; color: #6d6d6d; text-transform: uppercase; margin-bottom: 18px;">Contact Information</h3>
            
            <div class="information__product-information--name" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Name:</span> {{ isset($data['user_data']) ? $data['user_data']['name'] : '' }}</h4>
            </div>
            <div class="information__product-information--shape" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Email:</span> {{ isset($data['user_data']) && $data['user_data']['email'] ? $data['user_data']['email'] : '' }}</h4>
            </div>
            <div class="information__product-information--size" style="margin-bottom: 15px;">
                <h4 style="font-size: 16px; font-family: Roboto, sans-serif;"><span style="font-weight: 700; text-transform: uppercase;">Message:</span> {!! isset($data['user_data']) && $data['user_data']['message'] ? $data['user_data']['message'] : '' !!}</h4>
            </div>

        </div>



    </div>


<footer style="padding: 20px; margin: 0 auto; width: 600px; background: #8a558d;">
    <p class="company-name" style="font-family: Roboto, sans-serif; text-align: center; color: #fff; font-size: 15px; margin-bottom: 0;">{{ $seo_meta['name'] }}</p>
    {{-- <p class="address" style="font-family: Roboto, sans-serif; text-align: center; color: #fff; font-size: 15px; margin-bottom: 0;">12526 High Bluff Drive, Suite 300, San Diego, CA 92130, USA</a> --}}
        <p class="address" style="font-family: Roboto, sans-serif; text-align: center; color: #fff; font-size: 15px; margin-bottom: 0;">{{ $seo_meta['address'] }}</a>
</footer>

</body>
</html>