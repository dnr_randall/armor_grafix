window.$ = window.jQuery = require('jquery');
require('popper.js');
require('bootstrap');
require('select2');
require('jquery-validation');
require('slick-carousel');
require('magnific-popup');
window.swal = require('sweetalert');
require('readmore-js');
require('magnify');
require('owl.carousel2');
require('jquery-lazy');
var AOS = require('aos');
AOS.init({
    easing: 'ease-in-out-sine'
});

// vendor
require('./static/platform/platform');
require('./static/platform/ajaxq');
require('./static/platform/centralAjax');
require('./static/platform/config');

require('./static/vendors/jquery.easing.min.js');
require('./static/vendors/scrolling-nav.js');

require('./static/custom/image_on_error.js');
require('./static/custom/menu_nav.js');
require('./static/custom/jquery_validation.js');
require('./static/custom/jquery.chain-height.js');

require('./components/sections/all-sections.js');
require('./components/home.js');

require('./static/tab');

require('./static/custom/custom-slick-carousel.js');
require('./static/custom/custom-zoom.js');