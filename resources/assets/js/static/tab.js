function Tab(el) {
    this.el = el;
    this.$el = $(el);
    this.tabs = [];
    this.getTabs();
    this.initialize();
}

Tab.prototype.initialize = function() {
    var self = this;
    this.$el.find('[data-as=tab-navigator]').each(function (i, navigator) {
        $(navigator).on('click', function (e) {
            e.preventDefault();
            var navigate = $(this).data('navigate');

            if (navigate === 'next') {
            } else if (navigate === 'prev' || navigate === 'previous') {
            } else if (!isNaN(navigate)) {
                self.show(self.tabs[parseInt(navigate) - 1]);
            }
        });
    });
};

Tab.prototype.getTabs = function() {
    var self = this;
    this.$el.children().each(function (i, el) {
        self.tabs.push($(el).attr('class'));
    });
};

Tab.prototype.show = function(tab) {
    this.hideTabs();
    $(`.${tab}`).show();
};

Tab.prototype.hideTabs = function() {
    this.tabs.forEach(function (tab) {
        $(`.${tab}`).hide();
    })
};

window.Tab = Tab;