var uiFeaturedLargeSlider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    infinite: true,
    centerMode: false,
    swipe: true,
    asNavFor: '.featured-nav',
};
var uiFeaturedSmallSlider = {
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.featured-nav',
    speed: 500,
    arrows: true,
    dots: false,
    focusOnSelect:true,
    centerMode: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    prevArrow: '.featured-slider-prev',
    nextArrow: '.featured-slider-next',
    responsive: [
        {
            breakpoint: 1700,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
};
$('.featured-slider-large').slick(uiFeaturedLargeSlider);
$('.featured-slider-small').slick(uiFeaturedSmallSlider);

var uiProductImageLargeSlider = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    infinite: false,
    centerMode: false,
    swipe: false,
    asNavFor: '.product-nav',
};
var uiProductImageSmallSlider = {
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.product-nav',
    speed: 500,
    arrows: false,
    dots: false,
    focusOnSelect:true,
    centerMode: false,
    infinite: false,
    prevArrow: '.product-slider-prev',
    nextArrow: '.product-slider-next'
};
$('.product-image-slider-large').slick(uiProductImageLargeSlider);
$('.product-image-slider-small').slick(uiProductImageSmallSlider);

$('.home-slider').slick({
    // slidesToShow: 1,
    // slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: true,
    fade: true,
    infinite: true,
    centerMode: true,
    swipe: true,
    adaptiveHeight: true
});

$('.whychooseus-slick').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    dots: true,
    infinite: true,
    arrows: true,
    responsive: [
        {
            breakpoint: 1137,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 883,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 570,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});