$("[js-login-dropdown-target]").each(function(){
	var that=$(this);
	var target=$("[js-login-dropdown-id='"+that.attr("js-login-dropdown-target")+"']");
	that.on("mouseenter",function(){
		target.addClass("active-show");
	});
	target.on("mouseout",function(){
		target.removeClass("active-show");
	});
	$(document).on("click",function(e){
		if(e.target.id!=that.find("i").attr("id")&&e.target.id!=target.attr("id")){
			target.removeClass("active-show");
		}
	});
});