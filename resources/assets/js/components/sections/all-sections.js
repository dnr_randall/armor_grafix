// components
		require('./header.js');

	// initiated on about-us
		require('./sub-banner.js');
		require('./left-image.js');
		require('./right-image.js');
		require('./about-content.js');

	// initiated on products
		require('./sub-banner.js');
		require('./products-gallery.js');

	// initiated on product-details
		require('./product-details.js');
		require('./featured-labels.js');

	// initiated on design-label
		require('./label-shape.js');
		require('./size-quantity.js');
		require('./design-choices.js');
		require('./material-selection.js');
		require('./cta-request.js');

	// initiated on gallery
		require('./sub-banner.js');
		require('./gallery-items.js');

	// initiated on faq
		require('./sub-banner.js');
		require('./cta-faq.js');
		require('./faq-items.js');
		require('./faq-links.js');

	// initiated on shopping-cart
		require('./cart-details.js');

	// initiated on checkout
		require('./billing-payment.js');