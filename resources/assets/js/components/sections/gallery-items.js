// js file for gallery-items


$(document).ready(function(){
	// $(".js-modal-close").click(function(){
	// 	$(".js-modal-out").hide()
	// });
    $(".js-modal-close-btn").click(function(){
        $(".js-modal-out").hide();
        $('html, body').css('overflow', 'auto');
    });
	$(".js-gallery a").each(function(){
		$(this).find("img").closest("a").css({
			"background-image":"url('"+$(this).find("img").attr("src")+"')"
		});
		$(this).click(function(e){
			e.preventDefault();
			$(".js-modal-out").show().css({
				"z-index":"70"
			});
            $('html, body').css('overflow', 'hidden');
            $("html, body").on('keyup', function (e) {
            	if (e.keyCode === 27) {
                    $(".js-modal-close-btn").trigger('click');
                }
			});
		});
	});
	$(".js-click-for-modal").each(function(){
		var item=$(this).find("> div");
		item.each(function(e){
			$(this).attr("js-modal-id",e);
			var modalImages=$(this).find("img")
			var sDesc = $(this).find(".description").html();
			modalImages.each(function(){
				var src=$(this).attr("src");
				$(".js-modal-target").append(`<img src="`+src+`"><p class="description d-none">` + sDesc + `</p>`);
			});
		});
	});
	var adjustmentMove=167;
	var showMax=7;
	$(".js-modal-slider").each(function(){
		var photos=$(this).find(".js-modal-slider-photos");
		var firstChild=photos.find("> :first-child");
		photos.css({
			"background-image":`url('`+firstChild.attr("src")+`')`
		}).attr("js-msn-onshow","1");

		photos.find("> img").each(function(n){
            var desc = $(this).next('.description');
			$(this).after("<div js-msnid='"+(n)+"'></div>");
            $("[js-msnid='"+n+"']").append($(this));
            $("[js-msnid='"+n+"']").append(desc);

			$("[js-msnid='"+n+"']").css({
				"background-image":`url('`+$(this).attr("src")+`')`
			});
		});
		var navigation=$(this).find(".js-modal-slider-navigation");
		navigation.append(photos.html());

		photos.attr("js-msn-onshow","0");

		countPhoto=((navigation.find("> div").length)-1);
		$(this).find(".js-modal-slider-next").click(function(){
			var onShow=Math.abs(photos.attr("js-msn-onshow"));
			onShow+=1;
			onShow=(onShow>countPhoto)?0:onShow;
			var showChild=photos.find("[js-msnid='"+onShow+"'] > img");
			photos.css({
				"background-image":`url('`+showChild.attr("src")+`')`
			}).attr("js-msn-onshow",onShow);
            var sDesc = photos.find("[js-msnid='"+onShow+"']").find(".description").html();
            photos.siblings('.slider-description').html(sDesc)
			navigation.find("[js-msnid='"+onShow+"']").click();
			if(onShow==0){
				navigation.css({
					"left":"0px"
				});
				navigation.attr("js-status",0);
				navigation.attr("js-left-property",0);
			}
		});
		$(this).find(".js-modal-slider-prev > i").click(function(){
			var onShow=Math.abs(photos.attr("js-msn-onshow"));
			onShow-=1;
			onShow=(onShow<0)?(countPhoto):onShow;
			var showChild=photos.find("[js-msnid='"+onShow+"'] > img");
			photos.css({
				"background-image":`url('`+showChild.attr("src")+`')`
			}).attr("js-msn-onshow",onShow);
            var sDesc = photos.find("[js-msnid='"+onShow+"']").find(".description").html();
            photos.siblings('.slider-description').html(sDesc)
			navigation.find("[js-msnid='"+onShow+"']").click();
			if(onShow==countPhoto){
				navigation.css({
					"left":-Math.abs(adjustmentMove*(countPhoto-(showMax-1)))+"px"
				});
				navigation.attr("js-status",countPhoto-(showMax-1));
				navigation.attr("js-left-property",-Math.abs(adjustmentMove*(countPhoto-(showMax-1))));
			}
		});

		navigation.attr("js-left-property","0").css("left","0px").attr("js-status",0);
		navigation.find("> div").each(function(){
			$(this).click(function(){
				var valueId=$(this).attr("js-msnid");
				onShow=(valueId==countPhoto+1)?0:valueId;
				var showChild=photos.find("[js-msnid='"+onShow+"'] > img");
				photos.css({
					"background-image":`url('`+showChild.attr("src")+`')`
				}).attr("js-msn-onshow",onShow);
                var sDesc = photos.find("[js-msnid='"+onShow+"']").find(".description").html();
                photos.siblings('.slider-description').html(sDesc)
				// if(onShow>3&&onShow<8){
				var t_val_min=Math.abs(navigation.attr("js-status"))+4;
				var t_val_max=Math.abs(navigation.attr("js-status"))+6;
				var t_val_min_left=Math.abs(navigation.attr("js-status"));
				var t_val_max_left=Math.abs(navigation.attr("js-status"))+2;
				if( parseInt(navigation.attr("js-status"))<(countPhoto-6)
					){
					if(onShow>(t_val_min-1)&&onShow<(t_val_max+1)){
						var currentLeft=parseInt(navigation.attr("js-left-property"));
						var newCurrentLeft=currentLeft-adjustmentMove;
						navigation.attr("js-left-property",newCurrentLeft);
						navigation.attr("js-status",
							parseInt(navigation.attr("js-status"))+1
							);
						navigation.css({
							"left":newCurrentLeft+"px"
						});
					}
				}
				if( parseInt(navigation.attr("js-status"))>0
					){
					if(onShow>(t_val_min_left-1)&&onShow<(t_val_max_left+1)){
						var currentLeft=parseInt(navigation.attr("js-left-property"));
						var newCurrentLeft=currentLeft+adjustmentMove;
						navigation.attr("js-left-property",newCurrentLeft);
						navigation.attr("js-status",
							parseInt(navigation.attr("js-status"))-1
							);
						navigation.css({
							"left":newCurrentLeft+"px"
						});
					}
				}
			});
		});
		$(".js-show-modal-show-items").each(function(){
			$(this).find(".js-modal-item").each(function(){
				$(this).click(function(e){
					e.preventDefault();
					onShow=$(this).closest("[js-modal-id]").attr("js-modal-id");
					var showChild=photos.find("[js-msnid='"+onShow+"'] > img");
					photos.css({
						"background-image":`url('`+showChild.attr("src")+`')`
					}).attr("js-msn-onshow",onShow);
                    var sDesc = photos.find("[js-msnid='"+onShow+"']").find(".description").html();
                    photos.siblings('.slider-description').html(sDesc)
					navigation.find("[js-msnid='"+onShow+"']").click();
					var showNavPos=-Math.abs(adjustmentMove*(onShow-(showMax-4)));
					onShowVal=((onShow-3)<0)?0:(onShow-3);
					onShowVal=(onShowVal>9)?9:onShowVal;
					if(onShow<4){
						navigation.attr("js-status",onShowVal);
						navigation.attr("js-left-property",0);
						navigation.css({"left":"0px"});
					}else if(onShow>(countPhoto-5)){
						showNavPos=-Math.abs(adjustmentMove*(countPhoto-(showMax-1)));
						navigation.attr("js-status",onShowVal);
						navigation.attr("js-left-property",showNavPos);
						navigation.css({"left":showNavPos+"px"});
					}else{
						navigation.attr("js-status",onShowVal);
						navigation.attr("js-left-property",showNavPos);
						navigation.css({"left":showNavPos+"px"});
					}
				});
			});

		});
	});
});