<?php

use Illuminate\Database\Seeder;

class HomeSlidesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('home_slides')->delete();
        
        \DB::table('home_slides')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Holographic Tamper Evident Labels.',
                'background_image' => 'public/uploads/home_slide_images/banner2-1564187672.jpg',
                'short_description' => 'Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit',
                'button_label_1' => 'What is Tamper Evident?',
                'button_link_1' => 'http://52.24.144.212/armor_grafix/faq',
                'button_label_2' => 'Design Your Label',
                'button_link_2' => 'http://52.24.144.212/armor_grafix/design-label',
                'bottom_short_description' => 'Quick & Easy Label Creation Process!',
                'is_active' => 1,
                'created_at' => '2019-07-26 17:34:32',
                'updated_at' => '2019-07-26 17:34:32',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Holographic Tamper Evident Labels 2.',
                'background_image' => 'public/uploads/home_slide_images/banner2-1564187672.jpg',
                'short_description' => 'Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit',
                'button_label_1' => 'What is Tamper Evident?',
                'button_link_1' => 'http://52.24.144.212/armor_grafix/faq',
                'button_label_2' => 'Design Your Label',
                'button_link_2' => 'http://52.24.144.212/armor_grafix/design-label',
                'bottom_short_description' => 'Quick & Easy Label Creation Process!',
                'is_active' => 1,
                'created_at' => '2019-07-26 17:34:32',
                'updated_at' => '2019-07-26 17:34:32',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}