<?php

use Illuminate\Database\Seeder;

class GalleriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('galleries')->delete();
        
        \DB::table('galleries')->insert(array (
            0 => 
            array (
                'id' => 1,
                'image' => 'public/images/sections/gallery-items/gallery-photo-1.jpg',
                'description' => '1-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'image' => 'public/images/sections/gallery-items/gallery-photo-2.jpg',
                'description' => '2-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'image' => 'public/images/sections/gallery-items/gallery-photo-3.jpg',
                'description' => '3-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'image' => 'public/images/sections/gallery-items/gallery-photo-4.jpg',
                'description' => '4-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'image' => 'public/images/sections/gallery-items/gallery-photo-5.jpg',
                'description' => '5-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'image' => 'public/images/sections/gallery-items/gallery-photo-6.jpg',
                'description' => '6-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'image' => 'public/images/sections/gallery-items/gallery-photo-7.jpg',
                'description' => '7-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'image' => 'public/images/sections/gallery-items/gallery-photo-8.jpg',
                'description' => '8-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'image' => 'public/images/sections/gallery-items/gallery-photo-9.jpg',
                'description' => '9-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'image' => 'public/images/sections/gallery-items/gallery-photo-10.jpg',
                'description' => '10-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'image' => 'public/images/sections/gallery-items/gallery-photo-11.jpg',
                'description' => '11-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'image' => 'public/images/sections/gallery-items/gallery-photo-12.jpg',
                'description' => '12-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'image' => 'public/images/sections/gallery-items/gallery-photo-13.jpg',
                'description' => '13-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'image' => 'public/images/sections/gallery-items/gallery-photo-14.jpg',
                'description' => '14-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'image' => 'public/images/sections/gallery-items/gallery-photo-15.jpg',
                'description' => '15-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'image' => 'public/images/sections/gallery-items/gallery-photo-16.jpg',
                'description' => '16-Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'is_active' => 1,
                'created_at' => '2019-06-21 22:23:29',
                'updated_at' => '2019-06-21 22:23:29',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}