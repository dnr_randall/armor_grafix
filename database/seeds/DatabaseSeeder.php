<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionGroupsTableSeeder::class);
        $this->call(UserHasRolesTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(SystemSettingsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PageTypesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(PageSectionsTableSeeder::class);
//        $this->call(CitiesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(GalleriesTableSeeder::class);
        $this->call(ShapesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(SeoMetasTableSeeder::class);
        $this->call(UserHasPermissionsTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(QuantitiesTableSeeder::class);
        $this->call(DesignsTableSeeder::class);
        $this->call(MaterialsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductImagesTableSeeder::class);
        $this->call(CouponCodesTableSeeder::class);
        $this->call(CartsTableSeeder::class);
        $this->call(OrderAddressDetailsTableSeeder::class);
        $this->call(OrderCouponDetailsTableSeeder::class);
        $this->call(OrderItemDetailsTableSeeder::class);
        $this->call(OrderLogsTableSeeder::class);
        $this->call(OrderPaymentDetailsTableSeeder::class);
        $this->call(OrderShippingDetailsTableSeeder::class);
        $this->call(OrderStatusTableSeeder::class);
        $this->call(OrderTaxDetailsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(WhyChooseUsSectionsTableSeeder::class);
        $this->call(HomeSlidesTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(UserAddressDetailsTableSeeder::class);
    }
}
