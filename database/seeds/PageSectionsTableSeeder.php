<?php

use Illuminate\Database\Seeder;

class PageSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('page_sections')->delete();
        
        \DB::table('page_sections')->insert(array (
            0 => 
            array (
                'id' => 1,
                'page_id' => 15,
                'section' => 'google_map_link',
                'content' => '<iframe width="720" height="567" id="gmap_canvas"
src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed"
frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>',
                'type' => 'textarea',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-23 19:40:43',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'page_id' => 15,
                'section' => 'section_1_content',
                'content' => '<div class="row">
<div class="col-md-7">
<p><strong>Have special instructions and/or restrictions?</strong><br />
Lorem ipsum dolor sit amet, augue voluptatem diam porttitor eire dolor.</p>

<div class="row">
<div class="col-md-5"><a href="design-label">Designing A Label?</a> <i class="caret-left"></i></div>

<div class="col-md-7"><a href="design-label">What is A Custom Label?</a> <i class="caret-left"></i></div>

<div class="col-md-12">
<p>Have more questions? <a href="faq">Visit Our FAQs</a></p>
</div>
</div>
</div>

<div class="col-md-5 product-details__cta__buttons"><a href="design-label">Design Your Own Label</a> <a href="support">Request A Custom Label</a></div>
</div>
',
                'type' => 'ckeditor',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-23 19:40:43',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'page_id' => 2,
                'section' => 'banner_image',
                'content' => 'public/images/sections/sub-banner/about-us/banner-photo.jpg',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'page_id' => 2,
                'section' => 'banner_description',
                'content' => 'Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'type' => 'textarea',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'page_id' => 2,
                'section' => 'section_1_image',
                'content' => 'public/images/sections/left-image/photo.jpg',
                'type' => 'file',
                'position' => 3,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'page_id' => 2,
                'section' => 'section_1_title',
                'content' => 'Lorem Ipsum Melis Dolor',
                'type' => 'input',
                'position' => 4,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'page_id' => 2,
                'section' => 'section_1_content',
                'content' => '<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper. 
</p>
<p>
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a velit, ridiculus vestibulum, eget a egestas commodo non porttitor, wisi mauris non. Vestibulum a enim eros sem. Nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.
</p>',
                'type' => 'ckeditor',
                'position' => 5,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'page_id' => 2,
                'section' => 'section_2_title',
                'content' => 'Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut',
                'type' => 'textarea',
                'position' => 6,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'page_id' => 2,
                'section' => 'section_2_image',
                'content' => 'public/images/sections/right-image/photo.jpg',
                'type' => 'file',
                'position' => 7,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'page_id' => 2,
                'section' => 'section_2_content',
                'content' => '<div class="row">
<div class="col-md-12 right-image__middle">
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper. 
<br>
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a
</p>
</div>
</div><div class="row right-image__bottom">
<div class="col-md-6">
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper. 
</p>
<p>
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a velit, ridiculus vestibulum, eget a egestas commodo non porttitor, wisi mauris non. Vestibulum a enim eros sem. Nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.
</p>
</div>
<div class="col-md-6">
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper. 
</p>
<a href="products">
View All Holographic Labels 
</a>
</div>
</div>',
                'type' => 'ckeditor',
                'position' => 8,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'page_id' => 12,
                'section' => 'banner_image',
                'content' => 'public/images/sections/sub-banner/faq/banner.jpg',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'page_id' => 12,
                'section' => 'banner_description',
                'content' => '<span>Advice and answers from the Nano Team</span><br>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident.',
                'type' => 'ckeditor',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'page_id' => 12,
                'section' => 'block_1_title',
                'content' => 'About Us',
                'type' => 'textarea',
                'position' => 3,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'page_id' => 12,
                'section' => 'block_1_button_link',
                'content' => 'http://52.24.144.212/armor_grafix/about-us',
                'type' => 'textarea',
                'position' => 4,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'page_id' => 12,
                'section' => 'block_1_button_label',
                'content' => 'Learn More',
                'type' => 'input',
                'position' => 5,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'page_id' => 12,
                'section' => 'block_2_title',
                'content' => 'Support Number',
                'type' => 'textarea',
                'position' => 6,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'page_id' => 12,
                'section' => 'block_2_button_link',
                'content' => 'tel:1-800-123-4567',
                'type' => 'textarea',
                'position' => 7,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'page_id' => 12,
                'section' => 'block_2_button_label',
                'content' => '1-800-123-4567',
                'type' => 'input',
                'position' => 8,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'page_id' => 12,
                'section' => 'block_3_title',
                'content' => 'Support Email',
                'type' => 'textarea',
                'position' => 9,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'page_id' => 12,
                'section' => 'block_3_button_link',
                'content' => 'mailto:support@armorgrafix.com',
                'type' => 'textarea',
                'position' => 10,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'page_id' => 12,
                'section' => 'block_3_button_label',
                'content' => 'support@armorgrafix.com',
                'type' => 'input',
                'position' => 11,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'page_id' => 12,
                'section' => 'button_section_button_1_link',
                'content' => 'http://52.24.144.212/armor_grafix/about-us',
                'type' => 'textarea',
                'position' => 12,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'page_id' => 12,
                'section' => 'button_section_button_1_label',
                'content' => 'Learn More About Us',
                'type' => 'input',
                'position' => 13,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'page_id' => 12,
                'section' => 'button_section_button_2_link',
                'content' => 'http://52.24.144.212/armor_grafix/products',
                'type' => 'textarea',
                'position' => 14,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'page_id' => 12,
                'section' => 'button_section_button_2_label',
                'content' => 'View Labels',
                'type' => 'input',
                'position' => 15,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-04-18 13:58:49',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'page_id' => 8,
                'section' => 'banner_image',
                'content' => 'public/uploads/page_section_images/banner-1562781933.jpg',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:05:33',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'page_id' => 8,
                'section' => 'banner_description',
                'content' => '<span>lorem sup ipsum elite dolor!</span><br />
Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem.',
                'type' => 'ckeditor',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'page_id' => 4,
                'section' => 'image',
                'content' => 'public/img/stickertapes.png',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'page_id' => 5,
                'section' => 'image',
                'content' => 'public/img/stickertapes.png',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'page_id' => 6,
                'section' => 'image',
                'content' => 'public/img/stickertapes.png',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'page_id' => 7,
                'section' => 'image',
                'content' => 'public/img/stickertapes.png',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'page_id' => 10,
                'section' => 'step_1_instruction',
                'content' => '<h1>Choose Your Sticker Shape</h1>
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed
vestibulum, lorem eros in nisl.
</p>',
                'type' => 'ckeditor',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'page_id' => 10,
                'section' => 'step_2_instruction',
                'content' => ' <h5>Select Your Label Size & Quanity</h5>
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed
vestibulum, lorem eros in nisl.
</p>',
                'type' => 'ckeditor',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'page_id' => 10,
                'section' => 'step_3_instruction',
                'content' => '<h5>Choose Your Design</h5>
<p>
Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed
vestibulum, lorem eros in nisl.
</p>',
                'type' => 'ckeditor',
                'position' => 3,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'page_id' => 10,
                'section' => 'step_4_instruction',
                'content' => '<h5>Select Material Type</h5>
<p class="text-center">
There are no cost difference between these options.
</p>',
                'type' => 'ckeditor',
                'position' => 4,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'page_id' => 10,
                'section' => 'bottom_section_content',
                'content' => '<h3>Have Special Label Request?</h3>
<p>
Lorem ipsum nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.
</p>',
                'type' => 'ckeditor',
                'position' => 5,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'page_id' => 10,
                'section' => 'bottom_section_button_label',
                'content' => 'Request A Custom Label',
                'type' => 'input',
                'position' => 6,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'page_id' => 10,
                'section' => 'bottom_section_button_link',
                'content' => 'http://52.24.144.212/armor_grafix/support',
                'type' => 'textarea',
                'position' => 7,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'page_id' => 11,
                'section' => 'banner_image',
                'content' => 'public/images/sections/sub-banner/gallery/banner.jpg',
                'type' => 'file',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:05:33',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'page_id' => 11,
                'section' => 'banner_description',
                'content' => '<span>Explore our past Projects!</span><br>
Porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.',
                'type' => 'ckeditor',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'page_id' => 8,
                'section' => 'product_details_notes',
                'content' => '<div class="row">
<div class="col-md-7">
<p>
<strong>Have special instructions and/or restrictions?</strong><br>
Lorem ipsum dolor sit amet, augue voluptatem diam porttitor eire dolor.
</p>
<div class="row">
<div class="col-md-5">
<a href="http://52.24.144.212/armor_grafix/design-label">Designing A Label?</a>
<i class="caret-left"></i>
</div>
<div class="col-md-7">
<a href="http://52.24.144.212/armor_grafix/faq">What is A Custom Label?</a>
<i class="caret-left"></i>
</div>
<div class="col-md-12">
<p>Have more questions? <a href="http://52.24.144.212/armor_grafix/faq">Visit Our FAQs</a></p>
</div>
</div>
</div>
<div class="col-md-5 product-details__cta__buttons">
<a href="http://52.24.144.212/armor_grafix/design-label">Design Your Own Label</a>
</div>
</div>',
                'type' => 'ckeditor',
                'position' => 3,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'page_id' => 1,
                'section' => 'section_1_banner_step_1_title',
                'content' => 'Select Your Design',
                'type' => 'input',
                'position' => 1,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'page_id' => 1,
                'section' => 'section_1_banner_step_1_icon',
                'content' => 'public/img/pencilrulericon.jpg',
                'type' => 'file',
                'position' => 2,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'page_id' => 1,
                'section' => 'section_1_banner_step_2_title',
                'content' => 'Choose Your Material Type',
                'type' => 'input',
                'position' => 3,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'page_id' => 1,
                'section' => 'section_1_banner_step_2_icon',
                'content' => 'public/img/pencilrulericon.jpg',
                'type' => 'file',
                'position' => 4,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'page_id' => 1,
                'section' => 'section_1_banner_step_3_title',
                'content' => 'Choose Your Shape & Size',
                'type' => 'input',
                'position' => 5,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'page_id' => 1,
                'section' => 'section_1_banner_step_3_icon',
                'content' => 'public/img/shapesicon.jpg',
                'type' => 'file',
                'position' => 6,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'page_id' => 1,
                'section' => 'section_1_banner_step_4_title',
                'content' => 'Enter Desired Quantity',
                'type' => 'input',
                'position' => 7,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'page_id' => 1,
                'section' => 'section_1_banner_step_4_icon',
                'content' => 'public/img/plusminusicon.jpg',
                'type' => 'file',
                'position' => 8,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'page_id' => 1,
                'section' => 'section_1_banner_step_5_title',
                'content' => 'Checkout',
                'type' => 'input',
                'position' => 8,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'page_id' => 1,
                'section' => 'section_1_banner_step_5_icon',
                'content' => 'public/img/shoppingcartcheckicon.jpg',
                'type' => 'file',
                'position' => 10,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'page_id' => 1,
                'section' => 'why_choose_us_section_title',
                'content' => 'Why Choose Us?',
                'type' => 'input',
                'position' => 11,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'page_id' => 1,
                'section' => 'why_choose_us_section_button_1_label',
                'content' => 'Request A Free Sample',
                'type' => 'input',
                'position' => 12,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'page_id' => 1,
                'section' => 'why_choose_us_section_button_1_link',
                'content' => 'http://52.24.144.212/armor_grafix/support',
                'type' => 'textarea',
                'position' => 13,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'page_id' => 1,
                'section' => 'why_choose_us_section_button_2_label',
                'content' => 'Start Designing Your Label',
                'type' => 'input',
                'position' => 14,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'page_id' => 1,
                'section' => 'why_choose_us_section_button_2_link',
                'content' => 'http://52.24.144.212/armor_grafix/design-label',
                'type' => 'textarea',
                'position' => 15,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'page_id' => 1,
                'section' => 'why_choose_us_section_content',
                'content' => '<div class="c36">
Have a special instructions and restrictions? Lorem ipsum dolor sit amet consectetur
adipisicing elit. <a href="http://52.24.144.212/armor_grafix/design-label" class="c59"><span>Create a custom Holographic Label&nbsp; <i
class="fa fa-caret-right" aria-hidden="true"></i></span></a>
</div>',
                'type' => 'ckeditor',
                'position' => 16,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'page_id' => 1,
                'section' => 'featured_holographic_section_image',
                'content' => 'public/img/stickertapes.png',
                'type' => 'file',
                'position' => 17,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'page_id' => 1,
                'section' => 'featured_holographic_section_subtitle',
                'content' => 'Featured',
                'type' => 'textarea',
                'position' => 18,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'page_id' => 1,
                'section' => 'featured_holographic_section_title',
                'content' => 'Holographic Labels',
                'type' => 'textarea',
                'position' => 19,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'page_id' => 1,
                'section' => 'featured_holographic_section_content',
                'content' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nihil veniam ad ut
architecto.',
                'type' => 'textarea',
                'position' => 20,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'page_id' => 1,
                'section' => 'featured_holographic_section_subcontent',
                'content' => 'Lorem ipsum dolor, sit amet consectetur
adipisicing elit. Vel illum perspiciatis ea omnis, maxime fugit sunt enim, culpa
magni ullam obcaecati est sed officia voluptate! Mollitia sapiente et odio
reiciendis!',
                'type' => 'textarea',
                'position' => 21,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'page_id' => 1,
                'section' => 'featured_holographic_section_button_1_label',
                'content' => 'Create A Custom Label',
                'type' => 'input',
                'position' => 22,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'page_id' => 1,
                'section' => 'featured_holographic_section_button_1_link',
                'content' => 'http://52.24.144.212/armor_grafix/design-label',
                'type' => 'textarea',
                'position' => 23,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'page_id' => 1,
                'section' => 'featured_holographic_section_button_2_label',
                'content' => 'View All Holographic Labels',
                'type' => 'input',
                'position' => 24,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'page_id' => 1,
                'section' => 'featured_holographic_section_button_2_link',
                'content' => 'http://52.24.144.212/armor_grafix/products',
                'type' => 'textarea',
                'position' => 25,
                'created_at' => '2019-04-03 20:12:53',
                'updated_at' => '2019-07-10 11:04:30',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}