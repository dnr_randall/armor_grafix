<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Create Role',
                'permission_group_id' => 1,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Read Role',
                'permission_group_id' => 1,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Update Role',
                'permission_group_id' => 1,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Delete Role',
                'permission_group_id' => 1,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Create Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Read Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Update Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Delete Permission',
                'permission_group_id' => 2,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Create Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Read Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Update Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Delete Permission Group',
                'permission_group_id' => 3,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Create User',
                'permission_group_id' => 4,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Read User',
                'permission_group_id' => 4,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Update User',
                'permission_group_id' => 4,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Delete User',
                'permission_group_id' => 4,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Create System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Read System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Update System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Delete System Setting',
                'permission_group_id' => 5,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Create Page',
                'permission_group_id' => 6,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Read Page',
                'permission_group_id' => 6,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Update Page',
                'permission_group_id' => 6,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Delete Page',
                'permission_group_id' => 6,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Create Post',
                'permission_group_id' => 7,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Read Post',
                'permission_group_id' => 7,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Update Post',
                'permission_group_id' => 7,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Delete Post',
                'permission_group_id' => 7,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Create Home Slide',
                'permission_group_id' => 8,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Read Home Slide',
                'permission_group_id' => 87,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Update Home Slide',
                'permission_group_id' => 8,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Delete Home Slide',
                'permission_group_id' => 8,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Create Contact',
                'permission_group_id' => 9,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Read Contact',
                'permission_group_id' => 89,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Update Contact',
                'permission_group_id' => 9,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'Delete Contact',
                'permission_group_id' => 9,
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Create FAQ',
                'permission_group_id' => 10,
                'created_at' => '2019-06-21 19:05:23',
                'updated_at' => '2019-06-21 19:05:23',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Read FAQ',
                'permission_group_id' => 10,
                'created_at' => '2019-06-21 19:05:23',
                'updated_at' => '2019-06-21 19:05:23',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'Update FAQ',
                'permission_group_id' => 10,
                'created_at' => '2019-06-21 19:05:23',
                'updated_at' => '2019-06-21 19:05:23',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Delete FAQ',
                'permission_group_id' => 10,
                'created_at' => '2019-06-21 19:05:23',
                'updated_at' => '2019-06-21 19:05:23',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Create Gallery',
                'permission_group_id' => 11,
                'created_at' => '2019-06-21 22:12:11',
                'updated_at' => '2019-06-21 22:12:11',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Read Gallery',
                'permission_group_id' => 11,
                'created_at' => '2019-06-21 22:12:11',
                'updated_at' => '2019-06-21 22:12:11',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Update Gallery',
                'permission_group_id' => 11,
                'created_at' => '2019-06-21 22:12:11',
                'updated_at' => '2019-06-21 22:12:11',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Delete Gallery',
                'permission_group_id' => 11,
                'created_at' => '2019-06-21 22:12:11',
                'updated_at' => '2019-06-21 22:12:11',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'Create Shape',
                'permission_group_id' => 12,
                'created_at' => '2019-06-24 22:09:54',
                'updated_at' => '2019-06-24 22:09:54',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Read Shape',
                'permission_group_id' => 12,
                'created_at' => '2019-06-24 22:09:54',
                'updated_at' => '2019-06-24 22:09:54',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Update Shape',
                'permission_group_id' => 12,
                'created_at' => '2019-06-24 22:09:54',
                'updated_at' => '2019-06-24 22:09:54',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Delete Shape',
                'permission_group_id' => 12,
                'created_at' => '2019-06-24 22:09:54',
                'updated_at' => '2019-06-24 22:09:54',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Create Size',
                'permission_group_id' => 13,
                'created_at' => '2019-06-26 21:30:57',
                'updated_at' => '2019-06-26 21:30:57',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Read Size',
                'permission_group_id' => 13,
                'created_at' => '2019-06-26 21:30:57',
                'updated_at' => '2019-06-26 21:30:57',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Update Size',
                'permission_group_id' => 13,
                'created_at' => '2019-06-26 21:30:57',
                'updated_at' => '2019-06-26 21:30:57',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Delete Size',
                'permission_group_id' => 13,
                'created_at' => '2019-06-26 21:30:57',
                'updated_at' => '2019-06-26 21:30:57',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Create Quantity',
                'permission_group_id' => 14,
                'created_at' => '2019-06-26 22:48:42',
                'updated_at' => '2019-06-26 22:48:42',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Read Quantity',
                'permission_group_id' => 14,
                'created_at' => '2019-06-26 22:48:42',
                'updated_at' => '2019-06-26 22:48:42',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Update Quantity',
                'permission_group_id' => 14,
                'created_at' => '2019-06-26 22:48:42',
                'updated_at' => '2019-06-26 22:48:42',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Delete Quantity',
                'permission_group_id' => 14,
                'created_at' => '2019-06-26 22:48:42',
                'updated_at' => '2019-06-26 22:48:42',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Create Design',
                'permission_group_id' => 15,
                'created_at' => '2019-06-27 18:07:26',
                'updated_at' => '2019-06-27 18:07:26',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Read Design',
                'permission_group_id' => 15,
                'created_at' => '2019-06-27 18:07:26',
                'updated_at' => '2019-06-27 18:07:26',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Update Design',
                'permission_group_id' => 15,
                'created_at' => '2019-06-27 18:07:26',
                'updated_at' => '2019-06-27 18:07:26',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'Delete Design',
                'permission_group_id' => 15,
                'created_at' => '2019-06-27 18:07:26',
                'updated_at' => '2019-06-27 18:07:26',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Create Material',
                'permission_group_id' => 16,
                'created_at' => '2019-06-28 18:10:00',
                'updated_at' => '2019-06-28 18:10:00',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'Read Material',
                'permission_group_id' => 16,
                'created_at' => '2019-06-28 18:10:00',
                'updated_at' => '2019-06-28 18:10:00',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Update Material',
                'permission_group_id' => 16,
                'created_at' => '2019-06-28 18:10:00',
                'updated_at' => '2019-06-28 18:10:00',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Delete Material',
                'permission_group_id' => 16,
                'created_at' => '2019-06-28 18:10:00',
                'updated_at' => '2019-06-28 18:10:00',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Create Product',
                'permission_group_id' => 17,
                'created_at' => '2019-07-02 18:55:50',
                'updated_at' => '2019-07-02 18:55:50',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Read Product',
                'permission_group_id' => 17,
                'created_at' => '2019-07-02 18:55:50',
                'updated_at' => '2019-07-02 18:55:50',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Update Product',
                'permission_group_id' => 17,
                'created_at' => '2019-07-02 18:55:50',
                'updated_at' => '2019-07-02 18:55:50',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Delete Product',
                'permission_group_id' => 17,
                'created_at' => '2019-07-02 18:55:50',
                'updated_at' => '2019-07-02 18:55:50',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Create Coupon Code',
                'permission_group_id' => 18,
                'created_at' => '2019-07-10 23:27:20',
                'updated_at' => '2019-07-10 23:27:20',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Read Coupon Code',
                'permission_group_id' => 18,
                'created_at' => '2019-07-10 23:27:20',
                'updated_at' => '2019-07-10 23:27:20',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Update Coupon Code',
                'permission_group_id' => 18,
                'created_at' => '2019-07-10 23:27:20',
                'updated_at' => '2019-07-10 23:27:20',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Delete Coupon Code',
                'permission_group_id' => 18,
                'created_at' => '2019-07-10 23:27:20',
                'updated_at' => '2019-07-10 23:27:20',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Create Tax',
                'permission_group_id' => 19,
                'created_at' => '2019-07-18 23:15:31',
                'updated_at' => '2019-07-18 23:15:31',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Read Tax',
                'permission_group_id' => 19,
                'created_at' => '2019-07-18 23:15:31',
                'updated_at' => '2019-07-18 23:15:31',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Update Tax',
                'permission_group_id' => 19,
                'created_at' => '2019-07-18 23:15:31',
                'updated_at' => '2019-07-18 23:15:31',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'Delete Tax',
                'permission_group_id' => 19,
                'created_at' => '2019-07-18 23:15:31',
                'updated_at' => '2019-07-18 23:15:31',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Create Order',
                'permission_group_id' => 20,
                'created_at' => '2019-07-22 18:14:23',
                'updated_at' => '2019-07-22 18:14:23',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Read Order',
                'permission_group_id' => 20,
                'created_at' => '2019-07-22 18:14:23',
                'updated_at' => '2019-07-22 18:14:23',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'Update Order',
                'permission_group_id' => 20,
                'created_at' => '2019-07-22 18:14:23',
                'updated_at' => '2019-07-22 18:14:23',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Delete Order',
                'permission_group_id' => 20,
                'created_at' => '2019-07-22 18:14:23',
                'updated_at' => '2019-07-22 18:14:23',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Create Why Choose Us Section',
                'permission_group_id' => 21,
                'created_at' => '2019-07-25 22:36:44',
                'updated_at' => '2019-07-25 22:36:44',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Read Why Choose Us Section',
                'permission_group_id' => 21,
                'created_at' => '2019-07-25 22:36:44',
                'updated_at' => '2019-07-25 22:36:44',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Update Why Choose Us Section',
                'permission_group_id' => 21,
                'created_at' => '2019-07-25 22:36:44',
                'updated_at' => '2019-07-25 22:36:44',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Delete Why Choose Us Section',
                'permission_group_id' => 21,
                'created_at' => '2019-07-25 22:36:44',
                'updated_at' => '2019-07-25 22:36:44',
            ),
        ));
        
        
    }
}