<?php

use Illuminate\Database\Seeder;

class DesignsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('designs')->delete();
        
        \DB::table('designs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Security Text',
                'image' => 'public/images/sections/design-choices/ct-security-text.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Security Crossword',
                'image' => 'public/images/sections/design-choices/ct-security-crossword.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Valid',
                'image' => 'public/images/sections/design-choices/ct-valid.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Authentic',
                'image' => 'public/images/sections/design-choices/ct-authentic.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'THC',
                'image' => 'public/images/sections/design-choices/ct-thc.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Cannabis',
                'image' => 'public/images/sections/design-choices/ct-cannabis.jpg',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:55:44',
                'updated_at' => '2019-06-27 19:55:44',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Security Text',
                'image' => 'public/images/sections/design-choices/rd-security-text.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:12',
                'updated_at' => '2019-06-27 23:34:12',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Security Crossword',
                'image' => 'public/images/sections/design-choices/rd-security-crossword.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:20',
                'updated_at' => '2019-06-27 23:34:20',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Valid',
                'image' => 'public/images/sections/design-choices/rd-valid.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:27',
                'updated_at' => '2019-06-27 23:34:27',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Authentic',
                'image' => 'public/images/sections/design-choices/rd-authentic.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:32',
                'updated_at' => '2019-06-27 23:34:32',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'THC',
                'image' => 'public/images/sections/design-choices/rd-thc.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:37',
                'updated_at' => '2019-06-27 23:34:37',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Cannabis',
                'image' => 'public/images/sections/design-choices/rd-cannabis.png',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:34:37',
                'updated_at' => '2019-06-27 23:34:37',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Security Text',
                'image' => 'public/images/sections/design-choices/rc-security-text.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:18',
                'updated_at' => '2019-06-27 23:41:18',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Security Crossword',
                'image' => 'public/images/sections/design-choices/rc-security-crossword.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:24',
                'updated_at' => '2019-06-27 23:41:24',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Valid',
                'image' => 'public/images/sections/design-choices/rc-valid.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:30',
                'updated_at' => '2019-06-27 23:41:30',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Authentic',
                'image' => 'public/images/sections/design-choices/rc-authentic.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:36',
                'updated_at' => '2019-06-27 23:41:36',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'THC',
                'image' => 'public/images/sections/design-choices/rc-thc.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:44',
                'updated_at' => '2019-06-27 23:41:44',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Cannabis',
                'image' => 'public/images/sections/design-choices/rc-cannabis.png',
                'shape_id' => 3,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:41:51',
                'updated_at' => '2019-06-27 23:41:51',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Security Text',
                'image' => 'public/images/sections/design-choices/sq-security-text.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:45:28',
                'updated_at' => '2019-06-27 23:45:28',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Security Crossword',
                'image' => 'public/images/sections/design-choices/sq-security-crossword.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:45:37',
                'updated_at' => '2019-06-27 23:45:37',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Valid',
                'image' => 'public/images/sections/design-choices/sq-valid.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:45:43',
                'updated_at' => '2019-06-27 23:45:43',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Authentic',
                'image' => 'public/images/sections/design-choices/sq-authentic.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:45:49',
                'updated_at' => '2019-06-27 23:45:49',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'THC',
                'image' => 'public/images/sections/design-choices/sq-thc.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:45:56',
                'updated_at' => '2019-06-27 23:45:56',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Cannabis',
                'image' => 'public/images/sections/design-choices/sq-cannabis.png',
                'shape_id' => 4,
                'is_active' => 1,
                'created_at' => '2019-06-27 23:46:02',
                'updated_at' => '2019-06-27 23:46:02',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}