<?php

use Illuminate\Database\Seeder;

class QuantitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('quantities')->delete();
        
        \DB::table('quantities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '150',
                'price' => '120.00',
                'size_id' => 1,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:40:41',
                'updated_at' => '2019-06-27 19:47:45',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '150',
                'price' => '130.00',
                'size_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:40:44',
                'updated_at' => '2019-06-27 19:47:58',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}