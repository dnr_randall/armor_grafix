<?php

use Illuminate\Database\Seeder;

class WhyChooseUsSectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('why_choose_us_sections')->delete();
        
        \DB::table('why_choose_us_sections')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Counterfeiting',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Enim pariatur veritatis vero incidunt.',
                'background_image' => 'public/img/bigroundbg.png',
                'icon' => 'public/img/printdollar-white.png',
                'hover_icon' => 'public/img/printdollar-blue.png',
                'is_active' => 1,
                'created_at' => '2019-07-25 23:10:11',
                'updated_at' => '2019-07-25 23:10:11',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Tamper Evident',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Enim pariatur veritatis vero incidunt.',
                'background_image' => 'public/img/bigroundbg.png',
                'icon' => 'public/img/securedcheck-white.png',
                'hover_icon' => 'public/img/securedcheck-blue.png',
                'is_active' => 1,
                'created_at' => '2019-07-25 23:10:11',
                'updated_at' => '2019-07-25 23:10:11',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Track and Trace',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Enim pariatur veritatis vero incidunt.',
                'background_image' => 'public/img/bigroundbg.png',
                'icon' => 'public/img/wazework-white.png',
                'hover_icon' => 'public/img/wazework-blue.png',
                'is_active' => 1,
                'created_at' => '2019-07-25 23:10:11',
                'updated_at' => '2019-07-25 23:10:11',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Hologram',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Enim pariatur veritatis vero incidunt.',
                'background_image' => 'public/img/bigroundbg.png',
                'icon' => 'public/img/floatingcube-white.png',
                'hover_icon' => 'public/img/floatingcube-blue.png',
                'is_active' => 1,
                'created_at' => '2019-07-25 23:10:11',
                'updated_at' => '2019-07-25 23:10:11',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}