<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sizes')->delete();
        
        \DB::table('sizes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '2 x 2',
                'shape_id' => 2,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:21:46',
                'updated_at' => '2019-06-27 19:46:30',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '2 x 1',
                'shape_id' => 5,
                'is_active' => 1,
                'created_at' => '2019-06-27 19:21:49',
                'updated_at' => '2019-06-27 19:46:41',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}