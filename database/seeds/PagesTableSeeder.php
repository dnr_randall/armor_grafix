<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Home',
                'slug' => 'home',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 1,
                'page_type_id' => 1,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'About Us',
                'slug' => 'about-us',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 2,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Page Not Found',
                'slug' => '404',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 3,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => '2019-06-27 18:56:56',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Login',
                'slug' => 'customer/login',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 4,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Register',
                'slug' => 'customer/register',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 5,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Forgot Password',
                'slug' => 'customer/password/email',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 6,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Reset Password',
                'slug' => 'customer/password/reset',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 7,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Products',
                'slug' => 'products',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 8,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Product Details',
                'slug' => 'product-details',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 9,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => '2019-06-27 18:56:56',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Design Label',
                'slug' => 'design-label',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 10,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Gallery',
                'slug' => 'gallery',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 11,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'FAQ',
                'slug' => 'faq',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 12,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Shopping Cart',
                'slug' => 'shopping-cart',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 13,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Checkout',
                'slug' => 'checkout',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 14,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Support',
                'slug' => 'support',
                'content' => '',
                'banner_image' => '',
                'banner_description' => '',
                'is_active' => 1,
                'seo_meta_id' => 15,
                'page_type_id' => 2,
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}