<?php

use Illuminate\Database\Seeder;

class PermissionGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_groups')->delete();
        
        \DB::table('permission_groups')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Roles',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Permissions',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Permission Groups',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Users',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'System Settings',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Pages',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Posts',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Home Slides',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Contacts',
                'created_at' => '2019-06-21 19:05:22',
                'updated_at' => '2019-06-21 19:05:22',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'FAQS',
                'created_at' => '2019-06-21 19:05:23',
                'updated_at' => '2019-06-21 19:05:23',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Galleries',
                'created_at' => '2019-06-21 22:12:11',
                'updated_at' => '2019-06-21 22:12:11',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Shapes',
                'created_at' => '2019-06-24 22:09:54',
                'updated_at' => '2019-06-24 22:09:54',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Sizes',
                'created_at' => '2019-06-26 21:30:57',
                'updated_at' => '2019-06-26 21:30:57',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Quantities',
                'created_at' => '2019-06-26 22:48:42',
                'updated_at' => '2019-06-26 22:48:42',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Designs',
                'created_at' => '2019-06-27 18:07:26',
                'updated_at' => '2019-06-27 18:07:26',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Materials',
                'created_at' => '2019-06-28 18:10:00',
                'updated_at' => '2019-06-28 18:10:00',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Products',
                'created_at' => '2019-07-02 18:55:50',
                'updated_at' => '2019-07-02 18:55:50',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Coupon Codes',
                'created_at' => '2019-07-10 23:27:20',
                'updated_at' => '2019-07-10 23:27:20',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Taxes',
                'created_at' => '2019-07-18 23:15:31',
                'updated_at' => '2019-07-18 23:15:31',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Orders',
                'created_at' => '2019-07-22 18:14:23',
                'updated_at' => '2019-07-22 18:14:23',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Why Choose Us Sections',
                'created_at' => '2019-07-25 22:36:44',
                'updated_at' => '2019-07-25 22:36:44',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}