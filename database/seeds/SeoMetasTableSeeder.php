<?php

use Illuminate\Database\Seeder;

class SeoMetasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('seo_metas')->delete();
        
        \DB::table('seo_metas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'meta_title' => 'Home',
                'meta_keywords' => 'Home',
                'meta_description' => 'Home',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'meta_title' => 'About Us',
                'meta_keywords' => 'About Us',
                'meta_description' => 'About Us',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'meta_title' => 'Page Not Found',
                'meta_keywords' => 'Page Not Found',
                'meta_description' => 'Page Not Found',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'meta_title' => 'Login',
                'meta_keywords' => 'Login',
                'meta_description' => 'Login',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'meta_title' => 'Register',
                'meta_keywords' => 'Register',
                'meta_description' => 'Register',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'meta_title' => 'Forgot Password',
                'meta_keywords' => 'Forgot Password',
                'meta_description' => 'Forgot Password',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'meta_title' => 'Reset Password',
                'meta_keywords' => 'Reset Password',
                'meta_description' => 'Reset Password',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'meta_title' => 'Products',
                'meta_keywords' => 'Products',
                'meta_description' => 'Products',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'meta_title' => 'Product Details',
                'meta_keywords' => 'Product Details',
                'meta_description' => 'Product Details',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'meta_title' => 'Design Label',
                'meta_keywords' => 'Design Label',
                'meta_description' => 'Design Label',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'meta_title' => 'Gallery',
                'meta_keywords' => 'Gallery',
                'meta_description' => 'Gallery',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'meta_title' => 'FAQ',
                'meta_keywords' => 'FAQ',
                'meta_description' => 'FAQ',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'meta_title' => 'Shopping Cart',
                'meta_keywords' => 'Shopping Cart',
                'meta_description' => 'Shopping Cart',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'meta_title' => 'Checkout',
                'meta_keywords' => 'Checkout',
                'meta_description' => 'Checkout',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'meta_title' => 'Support',
                'meta_keywords' => 'Support',
                'meta_description' => 'Support',
                'created_at' => '2019-06-27 18:56:56',
                'updated_at' => '2019-06-27 18:56:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}