<?php

use Illuminate\Database\Seeder;

class MaterialsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('materials')->delete();
        
        \DB::table('materials')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Transparent',
                'image' => 'public/images/sections/material-selection/photo-1.jpg',
                'description' => 'Transparent tooltip description',
                'is_active' => 1,
                'created_at' => '2019-06-28 18:29:26',
                'updated_at' => '2019-06-28 18:29:26',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Metalized',
                'image' => 'public/images/sections/material-selection/photo-2.jpg',
                'description' => 'Metalized tooltip description',
                'is_active' => 1,
                'created_at' => '2019-06-28 18:29:40',
                'updated_at' => '2019-06-28 18:29:40',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Semi-Metalized',
                'image' => 'public/images/sections/material-selection/photo-3.jpg',
                'description' => 'Semi-Metalized tooltip description',
                'is_active' => 1,
                'created_at' => '2019-06-28 18:29:55',
                'updated_at' => '2019-06-28 18:29:55',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}