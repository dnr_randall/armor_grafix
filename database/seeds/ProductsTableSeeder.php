<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Holographic Security Label 1',
                'slug' => 'holographic-security-label-1',
                'sku' => '12345-1',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 1',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Holographic Security Label 2',
                'slug' => 'holographic-security-label-2',
                'sku' => '12345-2',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 2',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Holographic Security Label 3',
                'slug' => 'holographic-security-3',
                'sku' => '12345-3',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 3',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Holographic Security Label 4',
                'slug' => 'holographic-security-4',
                'sku' => '12345-4',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 4',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Holographic Security Label 5',
                'slug' => 'holographic-security-5',
                'sku' => '12345-5',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 5',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Holographic Security Label 6',
                'slug' => 'holographic-security-6',
                'sku' => '12345-6',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 6',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Holographic Security Label 7',
                'slug' => 'holographic-security-7',
                'sku' => '12345-7',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 7',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Holographic Security Label 8',
                'slug' => 'holographic-security-8',
                'sku' => '12345-8',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 8',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Holographic Security Label 9',
                'slug' => 'holographic-security-9',
                'sku' => '12345-9',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 9',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Holographic Security Label 10',
                'slug' => 'holographic-security-10',
                'sku' => '12345-10',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 10',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Holographic Security Label 11',
                'slug' => 'holographic-security-label-11',
                'sku' => '12345-11',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 11',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Holographic Security Label 12',
                'slug' => 'holographic-security-label-12',
                'sku' => '12345-12',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 12',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Holographic Security Label 13',
                'slug' => 'holographic-security-label-13',
                'sku' => '12345-13',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 13',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Holographic Security Label 14',
                'slug' => 'holographic-security-label-14',
                'sku' => '12345-14',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 14',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Holographic Security Label 15',
                'slug' => 'holographic-security-label-15',
                'sku' => '12345-15',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 15',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Holographic Security Label 16',
                'slug' => 'holographic-security-label-16',
                'sku' => '12345-16',
                'short_description' => 'Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup 16',
                'description' => '<p>
Lorem ipsum elite dolor sup melis vellisum lorem et suliop dolor sup integrates several security features:
</p>
<ul class="product-details__bullets-info">
<li>Lorem ipsum dolor sit amet, mus posuere erat suspendisse.</li>
<li>Dicta vel lorem mauris magna. Elementum quam, nibh libero mauris fusce.</li>
<li>Eros semper non in pellentesque vestibulum fusce, maecenas rutrum pretium dolor.</li>
</ul>',
                'shape_id' => 5,
                'material_id' => 2,
                'size_id' => 2,
                'is_featured' => 1,
                'is_active' => 1,
                'created_at' => '2019-07-04 17:34:17',
                'updated_at' => '2019-07-04 17:36:46',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}