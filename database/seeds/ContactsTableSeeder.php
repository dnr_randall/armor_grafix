<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Miko Suarez',
                'email' => 'programmer1@dogandrooster.com',
                'phone' => '',
                'company' => '',
                'subject' => '',
                'message' => 'test',
                'created_at' => '2019-07-10 11:07:12',
                'updated_at' => '2019-07-10 11:07:12',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}