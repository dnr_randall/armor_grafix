<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('faqs')->delete();
        
        \DB::table('faqs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Lorem ipsum dolor sit amet, fastidii disputando theophrastus?',
                'content' => '<p>Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.<br />
<br />
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a velit, ridiculus vestibulum, eget a egestas commodo non porttitor, wisi mauris non. Vestibulum a enim eros sem. Nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.</p>
',
                'is_active' => 1,
                'created_at' => '2019-06-21 19:31:28',
                'updated_at' => '2019-06-21 19:31:28',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Lorem ipsum dolor sit amet, fastidii disputando theophrastus?',
                'content' => '<p>Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.<br />
<br />
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a velit, ridiculus vestibulum, eget a egestas commodo non porttitor, wisi mauris non. Vestibulum a enim eros sem. Nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.</p>
',
                'is_active' => 1,
                'created_at' => '2019-06-21 19:32:53',
                'updated_at' => '2019-06-21 19:32:53',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Lorem ipsum dolor sit amet, fastidii disputando theophrastus?',
                'content' => '<p>Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor blandit nullam urna quis proident. Ipsum nec sed vestibulum, lorem eros in nisl. Arcu amet vitae in nunc dolorem. Dapibus congue ut ullamcorper.<br />
<br />
Ligula ut sed mi, eget dui fringilla purus et orci vitae. Duis lectus lorem a velit, ridiculus vestibulum, eget a egestas commodo non porttitor, wisi mauris non. Vestibulum a enim eros sem. Nibh ac sodales vestibulum ut sit, dolor lobortis eu lectus donec, suspendisse purus, libero arcu cras eget neque sit leo.</p>
',
                'is_active' => 1,
                'created_at' => '2019-06-21 19:33:06',
                'updated_at' => '2019-06-21 19:33:06',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}