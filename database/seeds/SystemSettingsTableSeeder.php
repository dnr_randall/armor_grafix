<?php

use Illuminate\Database\Seeder;

class SystemSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('system_settings')->delete();
        
        \DB::table('system_settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'SS0001',
                'name' => 'Website Name',
                'value' => 'Nano Grafix',
                'type' => 'input',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'SS0002',
                'name' => 'Website Email',
                'value' => 'test1@dogandrooster.net',
                'type' => 'input',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'SS0003',
                'name' => 'Website Phone',
                'value' => '1 800 123 4567',
                'type' => 'input',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'SS0004',
                'name' => 'Website Address',
                'value' => '2130 Fulton St, San Francisco, CA 94117, USA',
                'type' => 'textarea',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'SS0005',
                'name' => 'Default Meta Title',
                'value' => 'Default Meta Title',
                'type' => 'input',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'SS0006',
                'name' => 'Default Meta Keywords',
                'value' => 'Default Meta Keywords',
                'type' => 'input',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'SS0007',
                'name' => 'Default Meta Description',
            'value' => 'Nano Grafix Website. ACL Integrated (Access Control List).',
                'type' => 'textarea',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'SS0008',
                'name' => 'Need Help Section Image',
                'value' => 'public/img/ms.j.jpg',
                'type' => 'file',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'SS0009',
                'name' => 'Need Help Section Content',
            'value' => '<div class="col-sm-9 c72" style="background:url(\'http://52.24.144.212/armor_grafix/public/img/transbg3.png\');">
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-4 c75 m91">
<div class="f-aw c77 m92">Need Help?</div>
<div class="c74 m94">Lorem ipsum dolor sit amet, augue voluptatem diam, porttitor
blandit nullam urna quis proident.
</div>
</div>
<div class="col-md-4 c76 ml-auto m87">
<div class="row">
<div class="col-md-12">
<div class="row m90">
<div class="col-md-3 c78 m89"><img class="c79"
src="http://52.24.144.212/armor_grafix/public/img/telephoneicon.jpg">
</div>
<div class="col-md-9 f-aw c80 m88">1 800 123 4567</div>
</div>
</div>
<a href="http://52.24.144.212/armor_grafix/design-label">
<div class="col-12 c81 m95">
Create A Custom Label
</div>
</a>
<a href="http://52.24.144.212/armor_grafix/support">
<div class="col-12 c82 m96">
Request A Free Label Sample
</div>
</a>
</div>
</div>
</div>
</div>',
                'type' => 'ckeditor',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'SS0010',
                'name' => 'Facebook Link',
                'value' => 'https://www.facebook.com',
                'type' => 'textarea',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'SS0011',
                'name' => 'Instagram Link',
                'value' => 'https://www.instagram.com',
                'type' => 'textarea',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'SS0012',
                'name' => 'Twitter Link',
                'value' => 'https://twitter.com',
                'type' => 'textarea',
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 13:14:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}