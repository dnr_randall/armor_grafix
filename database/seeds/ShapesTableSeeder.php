<?php

use Illuminate\Database\Seeder;

class ShapesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('shapes')->delete();
        
        \DB::table('shapes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Special Shape',
                'slug' => 'special-shape',
                'image' => 'public/images/sections/label-shape/photo-1.jpg',
                'is_popular' => 1,
                'parent_id' => 0,
                'is_active' => 1,
                'created_at' => '2019-06-24 22:34:37',
                'updated_at' => '2019-06-24 22:34:37',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Circle',
                'slug' => 'circle',
                'image' => 'public/images/sections/label-shape/photo-2.jpg',
                'is_popular' => 0,
                'parent_id' => 0,
                'is_active' => 1,
                'created_at' => '2019-06-24 22:34:37',
                'updated_at' => '2019-06-24 22:34:37',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Rectangle',
                'slug' => 'rectangle',
                'image' => 'public/images/sections/label-shape/photo-3.jpg',
                'is_popular' => 0,
                'parent_id' => 0,
                'is_active' => 1,
                'created_at' => '2019-06-24 22:34:37',
                'updated_at' => '2019-06-24 22:34:37',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Square',
                'slug' => 'square',
                'image' => 'public/images/sections/label-shape/photo-4.jpg',
                'is_popular' => 0,
                'parent_id' => 0,
                'is_active' => 1,
                'created_at' => '2019-06-24 22:34:37',
                'updated_at' => '2019-06-24 22:34:37',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Special Shape 1',
                'slug' => 'special-shape-1',
                'image' => 'public/images/sections/label-shape/photo-1.jpg',
                'is_popular' => 0,
                'parent_id' => 1,
                'is_active' => 1,
                'created_at' => '2019-06-26 22:52:42',
                'updated_at' => '2019-06-26 22:52:42',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}