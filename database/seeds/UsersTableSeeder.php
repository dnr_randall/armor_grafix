<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'status' => 0,
                'email' => 'super_admin@dogandrooster.com',
                'user_name' => 'super_admin',
                'password' => '$2y$10$B6wXxIpfO46OOPHHCmDg1.1u76674efWiloGqjLsJ23o49NPORsm.',
                'first_name' => 'Super',
                'middle_name' => '',
                'last_name' => 'Admin',
                'phone' => '',
                'profile_image' => '',
                'is_active' => 1,
                'last_login' => '2018-03-01 13:33:36',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2018-03-01 13:38:29',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'status' => 0,
                'email' => 'test1@dogandrooster.net',
                'user_name' => 'webmaster',
                'password' => '$2y$10$mgHycnaMCK58lM.cVeiaAOjnQ325fhaW0.aa11Ekjl217W9q.PO7K',
                'first_name' => 'Webmaster',
                'middle_name' => '',
                'last_name' => 'Webmaster',
                'phone' => '',
                'profile_image' => '',
                'is_active' => 1,
                'last_login' => '2019-08-07 23:01:05',
                'token' => NULL,
                'remember_token' => 'EYgJp7pu0qCiTB7cVXXCDuXbll19uc3C3ruZLCuF4xIWDFjBGBWUGQdSrw6O',
                'created_at' => '2018-03-01 13:14:04',
                'updated_at' => '2019-08-07 23:01:05',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'status' => 0,
                'email' => 'customer_user@dogandrooster.com',
                'user_name' => 'customer_user',
                'password' => '$2y$10$xyFBObNk3vF63cq/iqkT4ePP74HG.FOJ7p6kABeyqWW5aqf2MDbwu',
                'first_name' => 'Customer',
                'middle_name' => '',
                'last_name' => 'One',
                'phone' => '',
                'profile_image' => '',
                'is_active' => 1,
                'last_login' => '2018-03-01 13:14:05',
                'token' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-03-01 13:14:05',
                'updated_at' => '2018-03-01 14:04:25',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'status' => 0,
                'email' => 'programmer1@dogandrooster.com',
                'user_name' => 'dnrmiko',
                'password' => '$2y$10$QK.9xV/9M6ybRzEU8j2skuPb7n.Pbe3od6xy3jLhKo9QFIQYLtLvK',
                'first_name' => 'Miko',
                'middle_name' => '',
                'last_name' => 'Suarez',
                'phone' => '',
                'profile_image' => '',
                'is_active' => 1,
                'last_login' => '2019-07-10 10:54:49',
                'token' => NULL,
                'remember_token' => 'Ur7SxSvfmlHpk428bNXBBnh0V73t5dqm8cvdv1uP3t3IaiyeBo7DlBXmLswH',
                'created_at' => '2019-07-10 10:51:20',
                'updated_at' => '2019-07-10 10:54:49',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}