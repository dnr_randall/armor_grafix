<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('session_id');
            $table->integer('product_id');
            $table->integer('shape_id');
            $table->integer('size_id');
            $table->integer('material_id');
            $table->integer('design_id');
            $table->integer('quantity_id');
            $table->decimal('price', 10, 2);
            $table->text('comments');
            $table->integer('include_tamper_evident');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}