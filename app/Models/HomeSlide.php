<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HomeSlide
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class HomeSlide extends Model
{
    use SoftDeletes;

    protected $table = 'home_slides';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'background_image',
        'short_description',
        'button_label_1',
        'button_link_1',
        'button_label_2',
        'button_link_2',
        'bottom_short_description',
        'is_active',
    ];
}