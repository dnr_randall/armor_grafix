<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Design
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Design extends Model
{
    use SoftDeletes;

    protected $table = 'designs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'shape_id',
        'is_active',
    ];

    public function shape()
    {
        return $this->belongsTo('App\Models\Shape', 'shape_id');
    }
}