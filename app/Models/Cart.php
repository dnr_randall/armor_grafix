<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cart
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Cart extends Model
{
    use SoftDeletes;

    protected $table = 'carts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'session_id',
        'product_id',
        'shape_id',
        'size_id',
        'material_id',
        'design_id',
        'quantity_id',
        'price',
        'comments',
        'include_tamper_evident',
        'is_customized',
    ];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id','product_id');
    }

    public function shape()
    {
        return $this->hasOne('App\Models\Shape', 'id','shape_id');
    }

    public function size()
    {
        return $this->hasOne('App\Models\Size', 'id','size_id');
    }

    public function design()
    {
        return $this->hasOne('App\Models\Design', 'id','design_id');
    }

    public function material()
    {
        return $this->hasOne('App\Models\Material', 'id','material_id');
    }

    public function quantity()
    {
        return $this->hasOne('App\Models\Quantity', 'id','quantity_id');
    }
}