<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserAddressDetail
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class UserAddressDetail extends Model
{
    use SoftDeletes;

    protected $table = 'user_address_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'ext',
        'company',
        'address',
        'address_2',
        'city',
        'state',
        'zip',
        'country',
        'type',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}