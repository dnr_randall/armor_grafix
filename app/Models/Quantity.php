<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Quantity
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Quantity extends Model
{
    use SoftDeletes;

    protected $table = 'quantities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'size_id',
        'label',
        'is_active',
    ];

    public function size()
    {
        return $this->belongsTo('App\Models\Size', 'size_id');
    }

    public function getPrice($size,$qty,$all = false) {
        $rounded_off = $this->where([
            'size_id' => $size,
            "is_active" => 1
        ])
        ->where('name' ,'<=', $qty)
        ->orderBy('name','desc')
        ->first();

        if($rounded_off)
            if($all)
                return $rounded_off; 
            else
                return $rounded_off->price;
        else {
            $qty = $this->where([
                'size_id' => $size,
                "is_active" => 1
            ])->first();
            if($all)
                return $qty ? $qty : 0; 
            else
                return $qty ? $qty->price : 0;
        }
    }
}