<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WhyChooseUsSection
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class WhyChooseUsSection extends Model
{
    use SoftDeletes;

    protected $table = 'why_choose_us_sections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'background_image',
        'icon',
        'hover_icon',
        'is_active',
    ];
}