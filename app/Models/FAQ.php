<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FAQ
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class FAQ extends Model
{
    use SoftDeletes;

    protected $table = 'faqs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'is_active',
    ];
}