<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'sku',
        'short_description',
        'description',
        'shape_id',
        'material_id',
        'size_id',
        'price',
        'label',
        'weight',
        'dimension',
        'is_featured',
        'is_active',
    ];

    public function product_images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function shape()
    {
        return $this->hasOne('App\Models\Shape', 'id','shape_id');
    }

    public function size()
    {
        return $this->hasOne('App\Models\Size', 'id','size_id');
    }

    public function material()
    {
        return $this->hasOne('App\Models\Material', 'id','material_id');
    }
}