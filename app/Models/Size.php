<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Size
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Size extends Model
{
    use SoftDeletes;

    protected $table = 'sizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'shape_id',
        'is_active',
    ];

    public function shape()
    {
        return $this->belongsTo('App\Models\Shape', 'shape_id');
    }

    public function quantities()
    {
        return $this->hasMany('App\Models\Quantity', 'size_id', 'id')
        ->selectRaw('*, (price / label) as min_price_each')->orderBy('min_price_each', 'asc');
    }
}