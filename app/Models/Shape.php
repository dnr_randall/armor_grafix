<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Shape
 * @package App\Models
 * @author Randall Anthony Bondoc
 */
class Shape extends Model
{
    use SoftDeletes;

    protected $table = 'shapes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'image',
        'is_popular',
        'parent_id',
        'is_active',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Shape', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Shape', 'parent_id', 'id');
    }

    public function sizes()
    {
        return $this->hasMany('App\Models\Size', 'shape_id', 'id');
    }

    public function designs()
    {
        return $this->hasMany('App\Models\Design', 'shape_id', 'id');
    }
}