<?php

namespace App\Http\Controllers;

use App\Repositories\CartRepository;
use App\Repositories\CheckoutRepository;
use Illuminate\Http\Request;

/** Paypal Details classes **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;

/**
 * Class CheckoutController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class CheckoutController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Checkout Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles section services
    |
    */

    /**
     * Create a new controller instance.
     *
     * @param CartRepository $cart_repository
     * @param CheckoutRepository $checkout_repository
     *
     */

    private $api_context;
    public function __construct(CartRepository $cart_repository,
                                CheckoutRepository $checkout_repository
    )
    {
        /*
         * Model namespace
         * using $this->cart_model can also access $this->cart_model->where('id', 1)->get();
         * */

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of section services with other data (related tables).
         * */
        $this->cart_repository = $cart_repository;
        $this->checkout_repository = $checkout_repository;

        $this->api_context = new ApiContext(
            new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret'))
        );
        $this->api_context->setConfig(config('paypal.settings'));

//        $this->middleware(['isAdmin']);
    }

    /*
    |--------------------------------------------------------------------------
    | Checkout
    |--------------------------------------------------------------------------
    */

    public function checkout(Request $request)
    {
        $input = $request->all();
        // dd($request->all());
        // if ($input['payment_method'] == '') {
        //     /* just continue process no payment */
                $carts = $this->cart_repository->getAll();
                $this->checkout_repository->processCartToOrder($request->all(), $carts);
        // } else if ($input['payment_method'] == 'paypal') {
            // Amount received as request is validated here.
                    // $request->validate(['amount' => 'required|numeric']);
                    // $pay_amount = $request->amount;
            // We create the payer and set payment method, could be any of "credit_card", "bank", "paypal", "pay_upon_invoice", "carrier", "alternate_payment". 
                    $payer = new Payer();
                    $payer->setPaymentMethod('paypal');
            // Create and setup items being paid for.. Could multiple items like: 'item1, item2 etc'.
                    $item = new Item();
                    $item->setName('Paypal Payment')->setCurrency('USD')->setQuantity(1)->setPrice($request->total);
            // Create item list and set array of items for the item list.
                    $itemList = new ItemList();
                    $itemList->setItems(array($item));
            // Create and setup the total amount.
                    $amount = new Amount();
                    $amount->setCurrency('USD')->setTotal($request->total);
            // Create a transaction and amount and description.
                    $transaction = new Transaction();
                    $transaction->setAmount($amount)->setItemList($itemList)
                    ->setDescription('Laravel Paypal Payment Tutorial');
                    //You can set custom data with '->setCustom($data)' or put it in a session.
            // Create a redirect urls, cancel url brings us back to current page, return url takes us to confirm payment.
                    $redirect_urls = new RedirectUrls();
                    $redirect_urls->setReturnUrl(route('paypal.confirm'))
                    ->setCancelUrl(url()->current());
            // We set up the payment with the payer, urls and transactions.
                    // Note: you can have different itemLists, then different transactions for it.
                    $payment = new Payment();
                    $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));
            // Put the payment creation in try and catch in case of exceptions.
                    try {
                        $payment->create($this->api_context);
                    } catch (PayPalConnectionException $ex){
                        return back()->withError('Some error occur, sorry for inconvenient');
                    } catch (Exception $ex) {
                        return back()->withError('Some error occur, sorry for inconvenient');
                    }
            // We get 'approval_url' a paypal url to go to for payments.
                    foreach($payment->getLinks() as $link) {
                        if($link->getRel() == 'approval_url') {
                            $redirect_url = $link->getHref();
                            break;
                        }
                    }
            // You can set a custom data in a session
                    // $request->session()->put('key', 'value');;
            // We redirect to paypal tp make payment
                    if(isset($redirect_url)) {
                        return redirect($redirect_url);
                    }
            // If we don't have redirect url, we have unknown error.
                    return redirect()->back()->withError('Unknown error occurred');
        // } else if ($input['payment_method'] == 'stripe') {

        // } else if ($input['payment_method'] == 'authorize') {

        // }
        
    }

    public function paypalConfirm(Request $request)
    {
        // If query data not available... no payments was made.
        if (empty($request->query('paymentId')) || empty($request->query('PayerID')) || empty($request->query('token')))
            return redirect('/checkout')->withError('Payment was not successful.');
        // We retrieve the payment from the paymentId.
                $payment = Payment::get($request->query('paymentId'), $this->api_context);
        // We create a payment execution with the PayerId
                $execution = new PaymentExecution();
                $execution->setPayerId($request->query('PayerID'));
        // Then we execute the payment.
                $result = $payment->execute($execution, $this->api_context);
        // Get value store in array and verified data integrity
                // $value = $request->session()->pull('key', 'default');
        // Check if payment is approved
                if ($result->getState() != 'approved') {
                    return redirect()->to('shopping-cart')
                    ->with('flash_message', [
                        'title' => '',
                        'message' => 'Ooops Something went wrong.',
                        'type' => 'error'
                    ]);
                }
                return redirect()->to('shopping-cart')
                ->with('flash_message', [
                    'title' => '',
                    'message' => 'Order Successfully created',
                    'type' => 'success'
                ]);
    }

    public function checkout_validation(Request $request)
    {

        if(isset($request->is_different_shipping_address))
        {
        $this->validate($request, [
                'billing_first_name' => 'required',
                'billing_last_name' => 'required',
                'billing_address' => 'required',
                'billing_state' => 'required',
                'billing_city' => 'required',
                'billing_zip' => 'required',
                'billing_phone' => 'required',
                'billing_email' => 'required',
                // 'billing_country' => 'required',

                // 'shipping_country' => 'required',
                'shipping_first_name' => 'required',
                'shipping_last_name' => 'required',
                'shipping_address' => 'required',
                'shipping_state' => 'required',
                'shipping_city' => 'required',
                'shipping_zip' => 'required',

                'shipping_method' => 'required'
                ]);
        }
        else
        {

        $this->validate($request, [
                'billing_first_name' => 'required',
                'billing_last_name' => 'required',
                'billing_address' => 'required',
                'billing_state' => 'required',
                'billing_city' => 'required',
                'billing_zip' => 'required',
                'billing_phone' => 'required',
                'billing_email' => 'required',
                // 'billing_country' => 'required',
                'shipping_method' => 'required'
                ]);

        }


        return response()->json($request);
    }

    public function checkout_store_transaction(Request $request, $transaction_id)
    {
        $carts = $this->cart_repository->getAll();
        $order = $this->checkout_repository->processCartToOrder($request->all(), $carts, $transaction_id);


        // return redirect()->route('checkout.success', [$order->reference_no]);

        return response()->json($order->reference_no);
    }

    public function checkout_success($order_ref)
    {

        // $carts = $this->cart_repository->getAll();
        // $this->checkout_repository->processCartToOrder($request->all(), $carts);
        $check = \App\Models\Order::where('reference_no',$order_ref)->get();

        if(count($check) >= 1)
        {
                return view('front.pages.custom-page.thank-you');
        }
        else
        {
                abort('404', '404');
        }

    }
}
