<?php

namespace App\Http\Controllers;

use App\Models\Shape;
use Illuminate\Http\Request;
use App\Models\Design;
use App\Repositories\DesignRepository;
use Illuminate\Validation\Rule;

/**
 * Class DesignController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class DesignController extends Controller
{
    /**
     * Design model instance.
     *
     * @var Design
     */
    private $design_model;

    /**
     * DesignRepository repository instance.
     *
     * @var DesignRepository
     */
    private $design_repository;

    /**
     * Create a new controller instance.
     *
     * @param Design $design_model
     * @param DesignRepository $design_repository
     * @param Shape $shape_model
     */
    public function __construct(Design $design_model, DesignRepository $design_repository,
                                Shape $shape_model)
    {
        /*
         * Model namespace
         * using $this->design_model can also access $this->design_model->where('id', 1)->get();
         * */
        $this->design_model = $design_model;
        $this->shape_model = $shape_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of designs with other data (related tables).
         * */
        $this->design_repository = $design_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $shape_id
     *
     * @return \Illuminate\Http\Response
     */

    public function index($shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Read Design')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($shape_id);
        $designs = $shape->designs()->get();

        return view('admin.pages.design.index', compact('designs', 'shape'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $shape_id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Design')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($shape_id);

        return view('admin.pages.design.create', compact('shape'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $shape_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Design')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('designs')->where(function ($query) use($request, $shape_id) {
                    return $query
                        ->where('name', $request->get('name'))
                        ->where('shape_id', $shape_id)
                        ->whereNull('deleted_at');
                }),
            ],
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
        $input['shape_id'] = $shape_id;
        $design = $this->design_model->create($input);

        if ($request->hasFile('image')) {
            $file_upload_path = $this->design_repository->uploadFile($request->file('image'), $design);
            $design->fill(['image' => $file_upload_path])->save();
        }

        return redirect()->route('admin.designs.index', $shape_id)->with('flash_message', [
            'title' => '',
            'message' => 'Design ' . $design->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Design')) {
            abort('401', '401');
        }

        $design = $this->design_model->findOrFail($id);
        $shape = $design->shape;

        return view('admin.pages.design.edit', compact('design', 'shape'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  int $shape_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Update Design')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('sizes')->where(function ($query) use($request, $id, $shape_id) {
                    return $query
                        ->where('id', '!=', $id)
                        ->where('name', $request->get('name'))
                        ->where('shape_id', $shape_id)
                        ->whereNull('deleted_at');
                }),
            ],
            'image' => 'required_if:remove_image,==,1|mimes:jpg,jpeg,png',
        ]);

        $design = $this->design_model->findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $file_upload_path = $this->design_repository->uploadFile($request->file('image'), $design);
            $input['image'] = $file_upload_path;
        }
        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $design->fill($input)->save();

        return redirect()->route('admin.designs.index', $shape_id)->with('flash_message', [
            'title' => '',
            'message' => 'Design ' . $design->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Design')) {
            abort('401', '401');
        }

        $design = $this->design_model->findOrFail($id);
        $design->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Design successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
