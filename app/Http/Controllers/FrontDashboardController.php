<?php

namespace App\Http\Controllers;
use App\Http\Traits\SystemSettingTrait;
use App\Repositories\PageRepository;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class FrontDashboardController extends Controller
{
    use SystemSettingTrait;

    /**
     * @param PageRepository $page_repository
     */
    public function __construct(PageRepository $page_repository)
    {
        $this->page_repository = $page_repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = $this->page_repository->getActivePageBySlug('customer/dashboard');
        if (!empty($page)) {
            $seo_meta = $this->getSeoMeta($page);
        }

        return view('front.pages.dashboard.dashboard', compact('page'));
    }
}
