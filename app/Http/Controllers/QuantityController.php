<?php

namespace App\Http\Controllers;

use App\Models\Size;
use Illuminate\Http\Request;
use App\Models\Quantity;
use App\Repositories\QuantityRepository;
use Illuminate\Validation\Rule;

/**
 * Class QuantityController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class QuantityController extends Controller
{
    /**
     * Quantity model instance.
     *
     * @var Quantity
     */
    private $quantity_model;

    /**
     * QuantityRepository repository instance.
     *
     * @var QuantityRepository
     */
    private $quantity_repository;

    /**
     * Create a new controller instance.
     *
     * @param Quantity $quantity_model
     * @param QuantityRepository $quantity_repository
     * @param Size $size_model
     */
    public function __construct(Quantity $quantity_model, QuantityRepository $quantity_repository,
                                Size $size_model
    )
    {
        /*
         * Model namespace
         * using $this->quantity_model can also access $this->quantity_model->where('id', 1)->get();
         * */
        $this->quantity_model = $quantity_model;
        $this->size_model = $size_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of quantities with other data (related tables).
         * */
        $this->quantity_repository = $quantity_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $size_id
     *
     * @return \Illuminate\Http\Response
     */

    public function index($size_id)
    {
        if (!auth()->user()->hasPermissionTo('Read Quantity')) {
            abort('401', '401');
        }

        $size = $this->size_model->findOrFail($size_id);
        $quantities = $size->quantities()->get();

        return view('admin.pages.quantity.index', compact('size', 'quantities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $size_id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($size_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Quantity')) {
            abort('401', '401');
        }

        $size = $this->size_model->findOrFail($size_id);

        return view('admin.pages.quantity.create', compact('size'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $size_id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $size_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Quantity')) {
            abort('401', '401');
        }
        $messages = [
            'name.required' => 'Quantity is required',
            'label.required' => 'Label per roll is required'
        ];
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'label' => 'required'
            ],$messages
        );

        $input = $request->all();
        $input['size_id'] = $size_id;
        $quantity = $this->quantity_model->create($input);

        return redirect()->route('admin.quantities.index', $size_id)->with('flash_message', [
            'title' => '',
            'message' => 'Quantity ' . $quantity->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Quantity')) {
            abort('401', '401');
        }

        $quantity = $this->quantity_model->findOrFail($id);
        $size = $quantity->size;

        return view('admin.pages.quantity.edit', compact('quantity', 'size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  int $size_id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $size_id)
    {
        if (!auth()->user()->hasPermissionTo('Update Quantity')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('quantities')->where(function ($query) use($request, $id, $size_id) {
                    return $query
                        ->where('id', '!=', $id)
                        ->where('name', $request->get('name'))
                        ->where('size_id', $size_id)
                        ->whereNull('deleted_at');
                }),
            ],
            'price' => 'required',
        ]);

        $quantity = $this->quantity_model->findOrFail($id);
        $input = $request->all();
        $quantity->fill($input)->save();

        return redirect()->route('admin.quantities.index', $size_id)->with('flash_message', [
            'title' => '',
            'message' => 'Quantity ' . $quantity->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Quantity')) {
            abort('401', '401');
        }

        $quantity = $this->quantity_model->findOrFail($id);
        $quantity->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Quantity successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
