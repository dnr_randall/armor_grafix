<?php

namespace App\Http\Controllers;

use App\Models\WhyChooseUsSection;
use App\Repositories\WhyChooseUsSectionRepository;
use Illuminate\Http\Request;

/**
 * Class WhyChooseUsSectionController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class WhyChooseUsSectionController extends Controller
{
    /**
     * WhyChooseUsSection model instance.
     *
     * @var WhyChooseUsSection
     */
    private $why_choose_us_section_model;

    /**
     * WhyChooseUsSectionRepository repository instance.
     *
     * @var WhyChooseUsSectionRepository
     */
    private $why_choose_us_section_repository;

    /**
     * Create a new controller instance.
     *
     * @param WhyChooseUsSection $why_choose_us_section_model
     * @param WhyChooseUsSectionRepository $why_choose_us_section_repository
     */
    public function __construct(WhyChooseUsSection $why_choose_us_section_model, WhyChooseUsSectionRepository $why_choose_us_section_repository)
    {
        /*
         * Model namespace
         * using $this->why_choose_us_section_model can also access $this->why_choose_us_section_model->where('id', 1)->get();
         * */
        $this->why_choose_us_section_model = $why_choose_us_section_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of why_choose_us_sections with other data (related tables).
         * */
        $this->why_choose_us_section_repository = $why_choose_us_section_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Why Choose Us Section')) {
            abort('401', '401');
        }

        $why_choose_us_sections = $this->why_choose_us_section_model->get();

        return view('admin.pages.why_choose_us_section.index', compact('why_choose_us_sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Why Choose Us Section')) {
            abort('401', '401');
        }

        return view('admin.pages.why_choose_us_section.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Why Choose Us Section')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'title' => 'required|unique:why_choose_us_sections,title,NULL,id,deleted_at,NULL',
            // 'description' => 'required',
            'background_image' => 'mimes:jpg,jpeg,png',
            'icon' => 'mimes:jpg,jpeg,png',
            'hover_icon' => 'mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
        $why_choose_us_section = $this->why_choose_us_section_model->create($input);

        if ($request->hasFile('background_image')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('background_image'), 'background_image');
            $why_choose_us_section->fill(['background_image' => $file_upload_path])->save();
        }

        if ($request->hasFile('icon')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('icon'), 'icon');
            $why_choose_us_section->fill(['icon' => $file_upload_path])->save();
        }

        if ($request->hasFile('hover_icon')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('hover_icon'), 'hover_icon');
            $why_choose_us_section->fill(['hover_icon' => $file_upload_path])->save();
        }

        return redirect()->route('admin.why_choose_us_sections.index')->with('flash_message', [
            'title' => '',
            'message' => 'Why Choose Us Section ' . $why_choose_us_section->title . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.why_choose_us_sections.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Why Choose Us Section')) {
            abort('401', '401');
        }

        $why_choose_us_section = $this->why_choose_us_section_model->findOrFail($id);

        return view('admin.pages.why_choose_us_section.edit', compact('why_choose_us_section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Why Choose Us Section')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'title' => 'required|unique:why_choose_us_sections,title,' . $id . ',id,deleted_at,NULL',
            // 'description' => 'required',
            'background_image' => 'mimes:jpg,jpeg,png',
            'icon' => 'mimes:jpg,jpeg,png',
            'hover_icon' => 'mimes:jpg,jpeg,png',
        ]);
        $why_choose_us_section = $this->why_choose_us_section_model->findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('background_image')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('background_image'), 'background_image');
            $input['background_image'] = $file_upload_path;
        }
        if ($request->has('remove_background_image') && $request->get('remove_background_image')) {
            $input['background_image'] = '';
        }

        if ($request->hasFile('icon')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('icon'), 'icon');
            $input['icon'] = $file_upload_path;
        }
        if ($request->has('remove_icon') && $request->get('remove_icon')) {
            $input['icon'] = '';
        }

        if ($request->hasFile('hover_icon')) {
            $file_upload_path = $this->why_choose_us_section_repository->uploadFile($request->file('hover_icon'), 'hover_icon');
            $input['hover_icon'] = $file_upload_path;
        }
        if ($request->has('remove_hover_icon') && $request->get('remove_hover_icon')) {
            $input['hover_icon'] = '';
        }

        $why_choose_us_section->fill($input)->save();

        return redirect()->route('admin.why_choose_us_sections.index')->with('flash_message', [
            'title' => '',
            'message' => 'Why Choose Us Section ' . $why_choose_us_section->title . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Why Choose Us Section')) {
            abort('401', '401');
        }

        $why_choose_us_section = $this->why_choose_us_section_model->findOrFail($id);
        $why_choose_us_section->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Why Choose Us Section successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
