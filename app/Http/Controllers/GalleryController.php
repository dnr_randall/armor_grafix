<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Repositories\GalleryRepository;

/**
 * Class GalleryController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class GalleryController extends Controller
{
    /**
     * Gallery model instance.
     *
     * @var Gallery
     */
    private $gallery_model;

    /**
     * GalleryRepository repository instance.
     *
     * @var GalleryRepository
     */
    private $gallery_repository;

    /**
     * Create a new controller instance.
     *
     * @param Gallery $gallery_model
     * @param GalleryRepository $gallery_repository
     */
    public function __construct(Gallery $gallery_model, GalleryRepository $gallery_repository)
    {
        /*
         * Model namespace
         * using $this->gallery_model can also access $this->gallery_model->where('id', 1)->get();
         * */
        $this->gallery_model = $gallery_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of galleries with other data (related tables).
         * */
        $this->gallery_repository = $gallery_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Gallery')) {
            abort('401', '401');
        }

        $galleries = $this->gallery_model->get();

        return view('admin.pages.gallery.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Gallery')) {
            abort('401', '401');
        }

        return view('admin.pages.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Gallery')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
//        $input['slug'] = str_slug($input['name']);

        $gallery = $this->gallery_model->create($input);

        if ($request->hasFile('image')) {
            $file_upload_path = $this->gallery_repository->uploadFile($request->file('image'), $gallery);
            $gallery->fill(['image' => $file_upload_path])->save();
        }

        return redirect()->route('admin.galleries.index')->with('flash_message', [
            'title' => '',
            'message' => 'Gallery ' . $gallery->title . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.galleries.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Gallery')) {
            abort('401', '401');
        }

        $gallery = $this->gallery_model->findOrFail($id);

        return view('admin.pages.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Gallery')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'description' => 'required',
            'image' => 'required_if:remove_image,==,1|mimes:jpg,jpeg,png',
        ], [
            'image.required_if' => 'The :attribute field is required if :attribute is removed.'
        ]);

        $gallery = $this->gallery_model->findOrFail($id);
        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
//        $input['slug'] = str_slug($input['name']);

        if ($request->hasFile('image')) {
            $file_upload_path = $this->gallery_repository->uploadFile($request->file('image'), $gallery);
            $input['image'] = $file_upload_path;
        }
        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $gallery->fill($input)->save();

        return redirect()->route('admin.galleries.index')->with('flash_message', [
            'title' => '',
            'message' => 'Gallery ' . $gallery->title . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Gallery')) {
            abort('401', '401');
        }

        $gallery = $this->gallery_model->findOrFail($id);
        $gallery->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Gallery successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
