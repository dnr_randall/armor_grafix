<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FAQ;
use App\Repositories\FAQRepository;

/**
 * Class FAQController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class FAQController extends Controller
{
    /**
     * FAQ model instance.
     *
     * @var FAQ
     */
    private $faq_model;

    /**
     * FAQRepository repository instance.
     *
     * @var FAQRepository
     */
    private $faq_repository;

    /**
     * Create a new controller instance.
     *
     * @param FAQ $faq_model
     * @param FAQRepository $faq_repository
     */
    public function __construct(FAQ $faq_model, FAQRepository $faq_repository)
    {
        /*
         * Model namespace
         * using $this->faq_model can also access $this->faq_model->where('id', 1)->get();
         * */
        $this->faq_model = $faq_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of faqs with other data (related tables).
         * */
        $this->faq_repository = $faq_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read FAQ')) {
            abort('401', '401');
        }

        $faqs = $this->faq_model->get();

        return view('admin.pages.faq.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create FAQ')) {
            abort('401', '401');
        }

        return view('admin.pages.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create FAQ')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
//        $input['slug'] = str_slug($input['name']);

        $faq = $this->faq_model->create($input);

        return redirect()->route('admin.faqs.index')->with('flash_message', [
            'title' => '',
            'message' => 'FAQ ' . $faq->title . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.faqs.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update FAQ')) {
            abort('401', '401');
        }

        $faq = $this->faq_model->findOrFail($id);

        return view('admin.pages.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update FAQ')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $faq = $this->faq_model->findOrFail($id);
        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
//        $input['slug'] = str_slug($input['name']);

        $faq->fill($input)->save();

        return redirect()->route('admin.faqs.index')->with('flash_message', [
            'title' => '',
            'message' => 'FAQ ' . $faq->title . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete FAQ')) {
            abort('401', '401');
        }

        $faq = $this->faq_model->findOrFail($id);
        $faq->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'FAQ successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
