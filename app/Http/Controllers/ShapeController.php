<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shape;
use App\Repositories\ShapeRepository;

/**
 * Class ShapeController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ShapeController extends Controller
{
    /**
     * Shape model instance.
     *
     * @var Shape
     */
    private $shape_model;

    /**
     * ShapeRepository repository instance.
     *
     * @var ShapeRepository
     */
    private $shape_repository;

    /**
     * Create a new controller instance.
     *
     * @param Shape $shape_model
     * @param ShapeRepository $shape_repository
     */
    public function __construct(Shape $shape_model, ShapeRepository $shape_repository)
    {
        /*
         * Model namespace
         * using $this->shape_model can also access $this->shape_model->where('id', 1)->get();
         * */
        $this->shape_model = $shape_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of shapes with other data (related tables).
         * */
        $this->shape_repository = $shape_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Shape')) {
            abort('401', '401');
        }

        $shapes = $this->shape_model->where('parent_id', 0)->get();

        return view('admin.pages.shape.index', compact('shapes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int $parent_id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = 0)
    {
        if (!auth()->user()->hasPermissionTo('Create Shape')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->find($parent_id);

        return view('admin.pages.shape.create', compact('shape'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $parent_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $parent_id = 0)
    {
        if (!auth()->user()->hasPermissionTo('Create Shape')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:shapes,name,NULL,id,deleted_at,NULL',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_popular'] = isset($input['is_popular']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);
        $input['parent_id'] = $parent_id;

        $shape = $this->shape_model->create($input);

        if ($request->hasFile('image')) {
            $file_upload_path = $this->shape_repository->uploadFile($request->file('image'), $shape);
            $shape->fill(['image' => $file_upload_path])->save();
        }

        if ($parent_id) {
            return redirect()->route('admin.shapes.show', $parent_id)->with('flash_message', [
                'title' => '',
                'message' => 'Shape ' . $shape->name . ' successfully added.',
                'type' => 'success'
            ]);
        }
        return redirect()->route('admin.shapes.index')->with('flash_message', [
            'title' => '',
            'message' => 'Shape ' . $shape->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!auth()->user()->hasPermissionTo('Read Shape')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($id);
        $shapes = $shape->children()->get();

        return view('admin.pages.shape.show', compact('shape', 'shapes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Shape')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($id);

        return view('admin.pages.shape.edit', compact('shape'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  int $parent_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $parent_id = 0)
    {
        if (!auth()->user()->hasPermissionTo('Update Shape')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:shapes,name,' . $id . ',id,deleted_at,NULL',
            'image' => 'required_if:remove_image,==,1|mimes:jpg,jpeg,png',
        ]);

        $shape = $this->shape_model->findOrFail($id);
        $input = $request->all();
//        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_popular'] = isset($input['is_popular']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);
        $input['parent_id'] = $parent_id;

        if ($request->hasFile('image')) {
            $file_upload_path = $this->shape_repository->uploadFile($request->file('image'), $shape);
            $input['image'] = $file_upload_path;
        }
        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $shape->fill($input)->save();

        if ($parent_id) {
            return redirect()->route('admin.shapes.show', $parent_id)->with('flash_message', [
                'title' => '',
                'message' => 'Shape ' . $shape->name . ' successfully updated.',
                'type' => 'success'
            ]);
        }
        return redirect()->route('admin.shapes.index')->with('flash_message', [
            'title' => '',
            'message' => 'Shape ' . $shape->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Shape')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($id);
        $shape->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Shape successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
