<?php

namespace App\Http\Controllers;

use App\Models\Shape;
use Illuminate\Http\Request;
use App\Models\Size;
use App\Repositories\SizeRepository;
use Illuminate\Validation\Rule;

/**
 * Class SizeController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class SizeController extends Controller
{
    /**
     * Size model instance.
     *
     * @var Size
     */
    private $size_model;

    /**
     * SizeRepository repository instance.
     *
     * @var SizeRepository
     */
    private $size_repository;

    /**
     * Create a new controller instance.
     *
     * @param Size $size_model
     * @param SizeRepository $size_repository
     * @param Shape $shape_model
     */
    public function __construct(Size $size_model, SizeRepository $size_repository,
                                Shape $shape_model
    )
    {
        /*
         * Model namespace
         * using $this->size_model can also access $this->size_model->where('id', 1)->get();
         * */
        $this->size_model = $size_model;
        $this->shape_model = $shape_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of sizes with other data (related tables).
         * */
        $this->size_repository = $size_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $shape_id
     *
     * @return \Illuminate\Http\Response
     */

    public function index($shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Read Size')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($shape_id);
        $sizes = $shape->sizes()->get();

        return view('admin.pages.size.index', compact('shape', 'sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $shape_id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Size')) {
            abort('401', '401');
        }

        $shape = $this->shape_model->findOrFail($shape_id);

        return view('admin.pages.size.create', compact('shape'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $shape_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Create Size')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('sizes')->where(function ($query) use($request, $shape_id) {
                    return $query
                        ->where('name', $request->get('name'))
                        ->where('shape_id', $shape_id)
                        ->whereNull('deleted_at');
                }),
            ]
        ]);

        $input = $request->all();
        $input['shape_id'] = $shape_id;

        if ($request->hasFile('image')) {
            $file_upload_path = $this->size_repository->uploadFile($request->file('image'));
            $input['image'] = $file_upload_path;
        }

        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $size = $this->size_model->create($input);

        return redirect()->route('admin.sizes.index', $shape_id)->with('flash_message', [
            'title' => '',
            'message' => 'Size ' . $size->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Size')) {
            abort('401', '401');
        }

        $size = $this->size_model->findOrFail($id);
        $shape = $size->shape;

        return view('admin.pages.size.edit', compact('size', 'shape'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @param  int $shape_id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $shape_id)
    {
        if (!auth()->user()->hasPermissionTo('Update Size')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => [
                'required',
                Rule::unique('sizes')->where(function ($query) use($request, $id, $shape_id) {
                    return $query
                        ->where('id', '!=', $id)
                        ->where('name', $request->get('name'))
                        ->where('shape_id', $shape_id)
                        ->whereNull('deleted_at');
                }),
            ]
        ]);

        $size = $this->size_model->findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $file_upload_path = $this->size_repository->uploadFile($request->file('image'));
            $input['image'] = $file_upload_path;
        }

        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $size->fill($input)->save();

        return redirect()->route('admin.sizes.index', $shape_id)->with('flash_message', [
            'title' => '',
            'message' => 'Size ' . $size->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Size')) {
            abort('401', '401');
        }

        $size = $this->size_model->findOrFail($id);
        $size->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Size successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
