<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Quantity;
use App\Repositories\CartRepository;
use Illuminate\Http\Request;
use FedEx;

/**
 * Class CartController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class CartController extends Controller
{
    /**
     * Cart model instance.
     *
     * @var Cart
     */
    private $cart_model;

    /**
     * CartRepository repository instance.
     *
     * @var CartRepository
     */
    private $cart_repository;

    /**
     * Create a new controller instance.
     *
     * @param Cart $cart_model
     * @param CartRepository $cart_repository
     * @param Quantity $quantity_model
     */
    public function __construct(Cart $cart_model, CartRepository $cart_repository,
                                Quantity $quantity_model
    )
    {
        /*
         * Model namespace
         * using $this->cart_model can also access $this->cart_model->where('id', 1)->get();
         * */
        $this->cart_model = $cart_model;
        $this->quantity_model = $quantity_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of carts with other data (related tables).
         * */
        $this->cart_repository = $cart_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (auth()->check()) {
            if (!auth()->user()->hasAnyRole('Customer')) {
                abort('401', '401');
            }
        }
        $this->validate($request, [
            'quantity_id' => 'required',
        ]);

        $input = $request->all();
        $input['user_id'] = auth()->check() ? auth()->user()->id : 0;
        $input['session_id'] = auth()->check() ? 0 : session()->getId();
        $input['include_tamper_evident'] = isset($input['include_tamper_evident']) ? 1 : 0;
        
        if($request->has('is_customized'))
            $input['price'] = $input['quantity_id'] * $this->quantity_model->getPrice($input['size_id'],$input['quantity_id']);
        else
            $input['price'] = $input['quantity_id'] * $input['price'];
            
        $cart = $this->cart_model->create($input);

        if (isset($input['add_to_cart_type'])) {
            if ($input['add_to_cart_type'] == 'checkout') {
                return redirect()->to('shopping-cart')->with('flash_message', [
                    'title' => '',
                    'message' => 'Cart successfully added.',
                    'type' => 'success'
                ]);
            } else if ($input['add_to_cart_type'] == 'another') {
                return redirect()->back()->with('flash_message', [
                    'title' => '',
                    'message' => 'Cart successfully added.',
                    'type' => 'success'
                ]);
            }
        } else {
            return redirect()->back()->with('flash_message', [
                'title' => '',
                'message' => 'Cart successfully added.',
                'type' => 'success'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (auth()->check()) {
            if (!auth()->user()->hasAnyRole('Customer')) {
                abort('401', '401');
            }
        }

        $this->validate($request, [
            'quantity_id' => 'required',
        ]);

        $cart = $this->cart_model->findOrFail($id);

        if (auth()->check()) {
            if (auth()->user()->id != $cart->user_id) {
                abort('401', '401');
            }
        } else {
            if (session()->getId() != $cart->session_id) {
                abort('401', '401');
            }
        }

        $input = $request->all();

        if($cart->is_customized)
            $input['price'] = $input['quantity_id'] * $this->quantity_model->getPrice($cart->size_id,$input['quantity_id']);
        else
            $input['price'] =  $input['quantity_id'] * ($cart->price / $cart->quantity_id);
        $cart->fill($input)->save();

        return redirect()->back()->with('flash_message', [
            'title' => '',
            'message' => 'Cart successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (auth()->check()) {
            if (!auth()->user()->hasAnyRole('Customer')) {
                abort('401', '401');
            }
        }

        $cart = $this->cart_model->findOrFail($id);

        if (auth()->check()) {
            if (auth()->user()->id != $cart->user_id) {
                abort('401', '401');
            }
        } else {
            if (session()->getId() != $cart->session_id) {
                abort('401', '401');
            }
        }

        $cart->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Cart successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }

    public function priceCheck(Request $request)
    {
        if (auth()->check()) {
            if (!auth()->user()->hasAnyRole('Customer')) {
                abort('401', '401');
            }
        }

        $this->validate($request, [
            // 'quantity_id' => 'required',
            'size_id' => 'required',
        ]);
        // $qty = $this->quantity_model->getPrice($request->size_id,$request->quantity_id,true);
        return [
            'prices' => $this->quantity_model->where([
                "size_id" => $request->size_id,
                "is_active" => 1,
            ])->orderBy('name','asc')->get(),
            // 'price' =>  number_format($qty->price,2),
            // 'label' =>  number_format($qty->label,2)
        ];
    }

    public function shippingRate(Request $request)
    {
        if (auth()->check()) {
            if (!auth()->user()->hasAnyRole('Customer')) {
                abort('401', '401');
            }
        }
        $rateRequest = FedEx::rateRequest();

        $shipment = new \Arkitecht\FedEx\Structs\RequestedShipment();
        $weight = 100;
        $shipment->TotalWeight = new \Arkitecht\FedEx\Structs\Weight(\Arkitecht\FedEx\Enums\WeightUnits::VALUE_LB, $weight);

        $shipment->Recipient = new \Arkitecht\FedEx\Structs\Party();
        $shipment->Recipient->Address = new \Arkitecht\FedEx\Structs\Address(
            null,
            null,
            null,
            $request->zip_code,
            null, 'US');

        $shipment->Shipper = new \Arkitecht\FedEx\Structs\Party();
        $shipment->Shipper->Address = new \Arkitecht\FedEx\Structs\Address(
            '12526 High Bluff Drive, Suite 300', //street address
            'San Diego', //city
            'CA', //states
            '92130', //zip
            null, 'US'); //country code

        $lineItem = new \Arkitecht\FedEx\Structs\RequestedPackageLineItem();
        $lineItem->Weight = new \Arkitecht\FedEx\Structs\Weight(\Arkitecht\FedEx\Enums\WeightUnits::VALUE_LB, $weight);
        $lineItem->GroupPackageCount = 1;
        $shipment->PackageCount = 1;

        $shipment->RequestedPackageLineItems = [
            $lineItem
        ];

        

        $rateRequest->Version = FedEx::rateService()->version;

        $rateRequest->setRequestedShipment($shipment);

        $rate = FedEx::rateService();

        $response = $rate->getRates($rateRequest);

        // $track = FedEx::trackReply();

        $rates = [];
        
        if ($response->HighestSeverity == 'SUCCESS' || $response->HighestSeverity == 'NOTE') {
            foreach ($response->RateReplyDetails as $rate) {
                $rates[$rate->ServiceType] = $rate->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
            }
        }

        // return $response;
        return $rates;

    }

}
