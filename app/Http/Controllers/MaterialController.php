<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Material;
use App\Repositories\MaterialRepository;

/**
 * Class MaterialController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class MaterialController extends Controller
{
    /**
     * Material model instance.
     *
     * @var Material
     */
    private $material_model;

    /**
     * MaterialRepository repository instance.
     *
     * @var MaterialRepository
     */
    private $material_repository;

    /**
     * Create a new controller instance.
     *
     * @param Material $material_model
     * @param MaterialRepository $material_repository
     */
    public function __construct(Material $material_model, MaterialRepository $material_repository)
    {
        /*
         * Model namespace
         * using $this->material_model can also access $this->material_model->where('id', 1)->get();
         * */
        $this->material_model = $material_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of materials with other data (related tables).
         * */
        $this->material_repository = $material_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Material')) {
            abort('401', '401');
        }

        $materials = $this->material_model->get();

        return view('admin.pages.material.index', compact('materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Material')) {
            abort('401', '401');
        }

        return view('admin.pages.material.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Material')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:materials,name,NULL,id,deleted_at,NULL',
            // 'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $input = $request->all();
        $material = $this->material_model->create($input);

        if ($request->hasFile('image')) {
            $file_upload_path = $this->material_repository->uploadFile($request->file('image'), $material);
            $material->fill(['image' => $file_upload_path])->save();
        }

        return redirect()->route('admin.materials.index')->with('flash_message', [
            'title' => '',
            'message' => 'Material ' . $material->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.materials.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Material')) {
            abort('401', '401');
        }

        $material = $this->material_model->findOrFail($id);

        return view('admin.pages.material.edit', compact('material'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Material')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:materials,name,' . $id . ',id,deleted_at,NULL',
            // 'description' => 'required',
            'image' => 'required_if:remove_image,==,1|mimes:jpg,jpeg,png',
        ]);

        $material = $this->material_model->findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $file_upload_path = $this->material_repository->uploadFile($request->file('image'), $material);
            $input['image'] = $file_upload_path;
        }
        if ($request->has('remove_image') && $request->get('remove_image')) {
            $input['image'] = '';
        }

        $material->fill($input)->save();

        return redirect()->route('admin.materials.index')->with('flash_message', [
            'title' => '',
            'message' => 'Material ' . $material->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Material')) {
            abort('401', '401');
        }

        $material = $this->material_model->findOrFail($id);
        $material->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Material successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }
}
