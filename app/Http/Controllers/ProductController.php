<?php

namespace App\Http\Controllers;

use App\Models\Material;
use App\Models\Shape;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Repositories\ProductRepository;
use File;

/**
 * Class ProductController
 * @package App\Http\Controllers
 * @author Randall Anthony Bondoc
 */
class ProductController extends Controller
{
    /**
     * Product model instance.
     *
     * @var Product
     */
    private $product_model;

    /**
     * ProductRepository repository instance.
     *
     * @var ProductRepository
     */
    private $product_repository;

    /**
     * Create a new controller instance.
     *
     * @param Product $product_model
     * @param ProductRepository $product_repository
     * @param Shape $shape_model
     * @param Material $material_model
     */
    public function __construct(Product $product_model, ProductRepository $product_repository,
                                Shape $shape_model,
                                Material $material_model
    )
    {
        /*
         * Model namespace
         * using $this->product_model can also access $this->product_model->where('id', 1)->get();
         * */
        $this->product_model = $product_model;
        $this->shape_model = $shape_model;
        $this->material_model = $material_model;

        /*
         * Repository namespace
         * this class may include methods that can be used by other controllers, like getting of products with other data (related tables).
         * */
        $this->product_repository = $product_repository;

//        $this->middleware(['isAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if (!auth()->user()->hasPermissionTo('Read Product')) {
            abort('401', '401');
        }

        $products = $this->product_model->get();

        return view('admin.pages.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->hasPermissionTo('Create Product')) {
            abort('401', '401');
        }

        $shapes = $this->shape_model->where('parent_id', 0)->get();
        $materials = $this->material_model->get();

        return view('admin.pages.product.create', compact('shapes', 'materials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if (!auth()->user()->hasPermissionTo('Create Product')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:products,name,NULL,id,deleted_at,NULL',
            'sku' => 'required|unique:products,sku,NULL,id,deleted_at,NULL',
            'description' => 'required',
        ]);

        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_featured'] = isset($input['is_featured']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $product = $this->product_model->create($input);

        /* product_images */
        $product->product_images()->forceDelete();

        if (isset($input['product_images'])) {
            foreach ($input['product_images'] as $product_image) {
                $product_image = json_decode(urldecode($product_image));
                $new_product_image_data = [
                    'name' => $product_image->name,
                    'size' => $product_image->size,
                    'file' => $product_image->file,
                    'is_default' => $product_image->is_default
                ];
                $new_product_image = $product->product_images()->create($new_product_image_data);
            }
        }
        /* product_images */

        return redirect()->route('admin.products.index')->with('flash_message', [
            'title' => '',
            'message' => 'Product ' . $product->name . ' successfully added.',
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('admin.products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->hasPermissionTo('Update Product')) {
            abort('401', '401');
        }

        $product = $this->product_model->findOrFail($id);
        $shapes = $this->shape_model->where('parent_id', 0)->get();
        $materials = $this->material_model->get();

        return view('admin.pages.product.edit', compact('product', 'shapes', 'materials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->hasPermissionTo('Update Product')) {
            abort('401', '401');
        }

        $this->validate($request, [
            'name' => 'required|unique:products,name,' . $id . ',id,deleted_at,NULL',
            'sku' => 'required|unique:products,sku,' . $id . ',id,deleted_at,NULL',
            'description' => 'required',
        ]);

        $product = $this->product_model->findOrFail($id);
        $input = $request->all();
        $input['is_active'] = isset($input['is_active']) ? 1 : 0;
        $input['is_featured'] = isset($input['is_featured']) ? 1 : 0;
        /* if slug is hidden, generate slug automatically */
        $input['slug'] = str_slug($input['name']);

        $product->fill($input)->save();

        /* product_images */
        $product->product_images()->forceDelete();

        if (isset($input['product_images'])) {
            foreach ($input['product_images'] as $product_image) {
                $product_image = json_decode(urldecode($product_image));
                $new_product_image_data = [
                    'name' => $product_image->name,
                    'size' => $product_image->size,
                    'file' => $product_image->file,
                    'is_default' => $product_image->is_default
                ];
                $new_product_image = $product->product_images()->create($new_product_image_data);
            }
        }
        /* product_images */

        return redirect()->route('admin.products.index')->with('flash_message', [
            'title' => '',
            'message' => 'Product ' . $product->name . ' successfully updated.',
            'type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->hasPermissionTo('Delete Product')) {
            abort('401', '401');
        }

        $product = $this->product_model->findOrFail($id);
        $product->delete();

        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $response['message'][] = 'Product successfully deleted.';
        $response['data']['id'] = $id;
        $response['status'] = TRUE;

        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        /*multiple*/
        $files = $request->file('file');

        $this->validate($request, [
            'file.*.file' => 'mimes:jpg,jpeg,png',
        ]);

        $file_paths = [];
        foreach ($files as $file) {
            $file_paths[] = $this->product_repository->uploadFileDropzone($file);
        }

        return response()->json($file_paths, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        $response = [
            'status' => FALSE,
            'data' => [],
            'messages' => [],
        ];

        $file = $request->get('orig-src');

//        $product_images = $this->product_image_model->where('file', $file)->get();
//        if (!empty($product_images)) {
//            foreach ($product_images as $product_image) {
//                $product_image->delete();
//            }
//        }

        if (File::exists($file)) {
            File::delete($file);
            $response['status'] = TRUE;
        }

        return response()->json($response, 200);
    }
}
