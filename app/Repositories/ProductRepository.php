<?php

namespace App\Repositories;

use App\Models\Product;
use File;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class ProductRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/Product;
     */
    public function getById($id)
    {
        $item = Product::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get single instance
     *
     * @param  $slug
     *
     * @return App/Models/Product;
     */
    public function getActiveBySlug($slug)
    {
        $item = Product::where('is_active', 1)->where('slug', $slug)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all instance
     *
     * @return App/Models/Product;
     */
    public function getAll()
    {
        $items = Product::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all instance
     *
     * @return App/Models/Product;
     */
    public function getAllActive()
    {
        $items = Product::where('is_active', 1)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all instance
     *
     * @param int $limit
     *
     * @return App/Models/Product;
     */
    public function getAllActivePaginate($limit = 12)
    {
        $items = Product::where('is_active', 1)->paginate($limit);
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all instance
     *
     * @param int $id
     *
     * @return App/Models/Product;
     */
    public function getAllActiveFeatured($id)
    {
        $items = Product::where('is_active', 1)->where('is_featured', 1)->where('id', '!=', $id)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\Product;
     */
    public function getData($item)
    {
        if (!empty($item)) {

        }

        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file)
    {
        $extension = $file->getClientOriginalExtension();
        $file_name = substr((pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)), 0, 30) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/product_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0777);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        return $file_upload_path;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFileDropzone($file)
    {
        $response = array(
            'status' => FALSE,
            'data' => array(),
            'message' => array(),
        );

        $extension = $file->getClientOriginalExtension();
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-' . time() . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/product_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0775);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        $response['data']['file_upload_path'] = $file_upload_path;
        $response['data']['file_name'] = $file_name;
        $response['data']['is_default'] = 0;
        $response['data']['id'] = $file_upload_path;
        return $response;
    }
}