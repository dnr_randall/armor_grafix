<?php

namespace App\Repositories;

use App\Models\WhyChooseUsSection;
use File;

/**
 * Class WhyChooseUsSectionRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class WhyChooseUsSectionRepository
{
    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/WhyChooseUsSection;
     */
    public function getById($id)
    {
        $item = WhyChooseUsSection::findOrFail($id);
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get single instance
     *
     * @param  $slug
     *
     * @return App/Models/WhyChooseUsSection;
     */
    public function getActiveBySlug($slug)
    {
        $item = WhyChooseUsSection::where('is_active', 1)->where('slug', $slug)->first();
        if (!empty($item)) {
            $item = $this->getData($item);
        }
        return $item;
    }

    /**
     * Get all instance
     *
     * @return App/Models/WhyChooseUsSection;
     */
    public function getAll()
    {
        $items = WhyChooseUsSection::get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get all instance
     *
     * @return App/Models/WhyChooseUsSection;
     */
    public function getAllActive()
    {
        $items = WhyChooseUsSection::where('is_active', 1)->get();
        foreach ($items as $item) {
            $item = $this->getData($item);
        }
        return $items;
    }

    /**
     * Get data
     *
     * @param  $item
     *
     * @return \App\Models\WhyChooseUsSection;
     */
    public function getData($item)
    {
        if (!empty($item)) {

        }

        return $item;
    }

    /**
     * Upload and move file to directory
     *
     * @return string $file_upload_path;
     */
    public function uploadFile($file, $type = null)
    {
        $extension = $file->getClientOriginalExtension();
        $file_name = substr((pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)), 0, 30) . '-' . time() . ($type ? '-' . $type : '') . '.' . $extension;
        $file_name = preg_replace("/[^a-z0-9\_\-\.]/i", '', $file_name);
        $file_path = '/uploads/why_choose_us_section_images';
        $directory = public_path() . $file_path;

        if (!File::exists($directory)) {
            File::makeDirectory($directory, 0777);
        }

        $file->move($directory, $file_name);
        $file_upload_path = 'public' . $file_path . '/' . $file_name;
        return $file_upload_path;
    }
}