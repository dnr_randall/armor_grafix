<?php

namespace App\Repositories;

use App\Http\Traits\SystemSettingTrait;
use App\Models\User;
use File;
use Mail;

/**
 * Class UserRepository
 * @package App\Repositories
 * @author Randall Anthony Bondoc
 */
class UserRepository
{
    use SystemSettingTrait;

    /**
     * Get single instance
     *
     * @param  $id
     *
     * @return App/Models/User;
     */
    public function get($id)
    {
        $item = User::findOrFail($id);
        return $item;
    }

    public function sendEmail($params = null)
    {
        if (isset($params)) {
            $system_setting_name = $this->getSystemSettingByCode('SS0001');
            $system_setting_email = $this->getSystemSettingByCode('SS0002');
            $is_admin = isset($params['is_admin']) && $params['is_admin'];

            $data = [
                'type' => $params['type'],
                'subject' => $params['subject'],
                'user' => [
                    'name' => $params['user']['name'],
                    'email' => $params['user']['email']
                ],
                'user_data' => $params['user_data'],
                'attachments' => $params['attachments']
            ];

            Mail::send($params['view'], compact('data'), function ($message) use ($data, $system_setting_name, $system_setting_email, $is_admin) {
                $message->from($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                $message->bcc(config('constants.dnr_bcc'));
                if ($is_admin) {
                    $message->to($system_setting_email->value/*config('constants.no_reply_email')*/, $system_setting_name->value);
                } else {
                    $message->to($data['user']['email'], $data['user']['name']);
                }
                $message->subject($data['subject']);
                if (isset($data['attachments']) && count($data['attachments'])) {
                    foreach ($data['attachments'] as $attachment) {
                        if (File::exists($attachment)) {
                            $message->attach($attachment);
                        }
                    }
                }
            });
        }
    }

    public function updateOrCreateUserBillingAddress($user, $input = [])
    {
        $billing_address = [];
        $billing_address[] = $user->billing_address()->updateOrCreate(
            [
                'id' => isset($input['billing_id']) ? $input['billing_id'] : 0,
                'user_id' => isset($input['user_id']) ? $input['user_id'] : 0,
                'type' => 1,
            ],
            [
                'user_id' => isset($input['user_id']) ? $input['user_id'] : 0,
                'first_name' => isset($input['billing_first_name']) ? $input['billing_first_name'] : '',
                'last_name' => isset($input['billing_last_name']) ? $input['billing_last_name'] : '',
                'email' => isset($input['billing_email']) ? $input['billing_email'] : '',
                'phone' => isset($input['billing_phone']) ? $input['billing_phone'] : '',
                'ext' => isset($input['billing_ext']) ? $input['billing_ext'] : '',
                'company' => isset($input['billing_company']) ? $input['billing_company'] : '',
                'address' => isset($input['billing_address']) ? $input['billing_address'] : '',
                'address_2' => isset($input['billing_address_2']) ? $input['billing_address_2'] : '',
                'city' => isset($input['billing_city']) ? $input['billing_city'] : '',
                'state' => isset($input['billing_state']) ? $input['billing_state'] : '',
                'zip' => isset($input['billing_zip']) ? $input['billing_zip'] : '',
                'country' => isset($input['billing_country']) ? $input['billing_country'] : '',
                'type' => 1,
            ]);
        return $billing_address;
    }

    public function updateOrCreateUserShippingAddress($user, $input = [])
    {
        $shipping_address = [];
        $shipping_address[] = $user->shipping_address()->updateOrCreate(
            [
                'id' => isset($input['shipping_id']) ? $input['shipping_id'] : 0,
                'user_id' => isset($input['user_id']) ? $input['user_id'] : 0,
                'type' => 2,
            ],
            [
                'user_id' => isset($input['user_id']) ? $input['user_id'] : 0,
                'first_name' => isset($input['shipping_first_name']) ? $input['shipping_first_name'] : '',
                'last_name' => isset($input['shipping_last_name']) ? $input['shipping_last_name'] : '',
                'email' => isset($input['shipping_email']) ? $input['shipping_email'] : '',
                'phone' => isset($input['shipping_phone']) ? $input['shipping_phone'] : '',
                'ext' => isset($input['shipping_ext']) ? $input['shipping_ext'] : '',
                'company' => isset($input['shipping_company']) ? $input['shipping_company'] : '',
                'address' => isset($input['shipping_address']) ? $input['shipping_address'] : '',
                'address_2' => isset($input['shipping_address_2']) ? $input['shipping_address_2'] : '',
                'city' => isset($input['shipping_city']) ? $input['shipping_city'] : '',
                'state' => isset($input['shipping_state']) ? $input['shipping_state'] : '',
                'zip' => isset($input['shipping_zip']) ? $input['shipping_zip'] : '',
                'country' => isset($input['shipping_country']) ? $input['shipping_country'] : '',
                'type' => 2,
            ]);
        return $shipping_address;
    }
}