<?php

/* dashboard routes */
Route::get('/', function () {
    return redirect('/admin/pages');
});

/* dashboard */
Route::get('/dashboard', function () {
    return redirect('/admin/pages');
});
/* dashboard */

/* users */
Route::get('/users/draw',
    ['as' => 'admin.users.draw',
        'uses' => '\App\Http\Controllers\UserController@draw']
);

Route::resource('/users', 'UserController', [
    'as' => 'admin'
]);

Route::delete('/users/{id}/delete',
    ['as' => 'admin.users.delete',
        'uses' => '\App\Http\Controllers\UserController@destroy']
);
/* users */

/* roles */
Route::resource('/roles', 'RoleController', [
    'as' => 'admin'
]);

Route::delete('/roles/{id}/delete',
    ['as' => 'admin.roles.delete',
        'uses' => '\App\Http\Controllers\RoleController@destroy']
);
/* roles */

/* permissions */
Route::resource('/permissions', 'PermissionController', [
    'as' => 'admin'
]);

Route::delete('/permissions/{id}/delete',
    ['as' => 'admin.permissions.delete',
        'uses' => '\App\Http\Controllers\PermissionController@destroy']
);
/* permissions */

/* permission groups */
Route::resource('/permission_groups', 'PermissionGroupController', [
    'as' => 'admin'
]);

Route::delete('/permission_groups/{id}/delete',
    ['as' => 'admin.permission_groups.delete',
        'uses' => '\App\Http\Controllers\PermissionGroupController@destroy']
);
/* permission groups */

/* system settings */
Route::resource('/system_settings', 'SystemSettingController', [
    'as' => 'admin',
]);

Route::delete('/system_settings/{id}/delete',
    ['as' => 'admin.system_settings.delete',
        'uses' => '\App\Http\Controllers\SystemSettingController@destroy']
);
/* system settings */

/* posts */
Route::resource('/posts', 'PostController', [
    'as' => 'admin'
]);

Route::delete('/posts/{id}/delete',
    ['as' => 'admin.posts.delete',
        'uses' => '\App\Http\Controllers\PostController@destroy']
);
/* posts */

/* pages */
Route::resource('/pages', 'PageController', [
    'as' => 'admin'
]);

Route::delete('/pages/{id}/delete',
    ['as' => 'admin.pages.delete',
        'uses' => '\App\Http\Controllers\PageController@destroy']
);
/* pages */

/* ckeditor image upload */
Route::post('/ckeditor_image_upload',
    ['as' => 'admin.ckeditor_image_upload',
        'uses' => '\App\Http\Controllers\PageController@ckEditorImageUpload']
);
/* ckeditor image upload */

/* home_slides */
Route::resource('/home_slides', 'HomeSlideController', [
    'as' => 'admin'
]);

Route::delete('/home_slides/{id}/delete',
    ['as' => 'admin.home_slides.delete',
        'uses' => '\App\Http\Controllers\HomeSlideController@destroy']
);
/* home_slides */

/* contacts */
Route::resource('/contacts', 'ContactController', [
    'as' => 'admin'
]);

Route::delete('/contacts/{id}/delete',
    ['as' => 'admin.contacts.delete',
        'uses' => '\App\Http\Controllers\ContactController@destroy']
);
/* contacts */

/* faqs */
Route::resource('/faqs', 'FAQController', [
    'as' => 'admin'
]);

Route::delete('/faqs/{id}/delete',
    ['as' => 'admin.faqs.delete',
        'uses' => '\App\Http\Controllers\FAQController@destroy']
);
/* faqs */

/* galleries */
Route::resource('/galleries', 'GalleryController', [
    'as' => 'admin'
]);

Route::delete('/galleries/{id}/delete',
    ['as' => 'admin.galleries.delete',
        'uses' => '\App\Http\Controllers\GalleryController@destroy']
);
/* galleries */

/* shapes */
//Route::resource('/shapes', 'ShapeController', [
//    'as' => 'admin'
//]);

Route::get('/shapes',
    ['as' => 'admin.shapes.index',
        'uses' => 'ShapeController@index']
);
Route::get('/shapes/create/{parent_id?}',
    ['as' => 'admin.shapes.create',
        'uses' => 'ShapeController@create']
);
Route::post('/shapes/store/{parent_id?}',
    ['as' => 'admin.shapes.store',
        'uses' => 'ShapeController@store']
);
Route::get('/shapes/{id}/edit',
    ['as' => 'admin.shapes.edit',
        'uses' => 'ShapeController@edit']
);
Route::put('/shapes/{id}/update/{parent_id?}',
    ['as' => 'admin.shapes.update',
        'uses' => 'ShapeController@update']
);
Route::delete('/shapes/{id}/delete',
    ['as' => 'admin.shapes.delete',
        'uses' => '\App\Http\Controllers\ShapeController@destroy']
);
Route::get('/shapes/{parent_id?}',
    ['as' => 'admin.shapes.show',
        'uses' => 'ShapeController@show']
);
/* shapes */

/* sizes */
//Route::resource('/sizes', 'SizeController', [
//    'as' => 'admin'
//]);
//
//Route::delete('/sizes/{id}/delete',
//    ['as' => 'admin.sizes.delete',
//        'uses' => '\App\Http\Controllers\SizeController@destroy']
//);

Route::get('/sizes/{shape_id}',
    ['as' => 'admin.sizes.index',
        'uses' => 'SizeController@index']
);
Route::get('/sizes/create/{shape_id}',
    ['as' => 'admin.sizes.create',
        'uses' => 'SizeController@create']
);
Route::post('/sizes/store/{shape_id}',
    ['as' => 'admin.sizes.store',
        'uses' => 'SizeController@store']
);
Route::get('/sizes/{id}/edit',
    ['as' => 'admin.sizes.edit',
        'uses' => 'SizeController@edit']
);
Route::put('/sizes/{id}/update/{shape_id}',
    ['as' => 'admin.sizes.update',
        'uses' => 'SizeController@update']
);
Route::delete('/sizes/{id}/delete',
    ['as' => 'admin.sizes.delete',
        'uses' => '\App\Http\Controllers\SizeController@destroy']
);
/* sizes */

/* quantities */
//Route::resource('/quantities', 'QuantityController', [
//    'as' => 'admin'
//]);
//
//Route::delete('/quantities/{id}/delete',
//    ['as' => 'admin.quantities.delete',
//        'uses' => '\App\Http\Controllers\QuantityController@destroy']
//);

Route::get('/quantities/{size_id}',
    ['as' => 'admin.quantities.index',
        'uses' => 'QuantityController@index']
);
Route::get('/quantities/create/{size_id}',
    ['as' => 'admin.quantities.create',
        'uses' => 'QuantityController@create']
);
Route::post('/quantities/store/{size_id}',
    ['as' => 'admin.quantities.store',
        'uses' => 'QuantityController@store']
);
Route::get('/quantities/{id}/edit',
    ['as' => 'admin.quantities.edit',
        'uses' => 'QuantityController@edit']
);
Route::put('/quantities/{id}/update/{size_id}',
    ['as' => 'admin.quantities.update',
        'uses' => 'QuantityController@update']
);
Route::delete('/quantities/{id}/delete',
    ['as' => 'admin.quantities.delete',
        'uses' => '\App\Http\Controllers\QuantityController@destroy']
);
/* quantities */

/* designs */
//Route::resource('/designs', 'DesignController', [
//    'as' => 'admin'
//]);
//
//Route::delete('/designs/{id}/delete',
//    ['as' => 'admin.designs.delete',
//        'uses' => '\App\Http\Controllers\DesignController@destroy']
//);

Route::get('/designs/{shape_id}',
    ['as' => 'admin.designs.index',
        'uses' => 'DesignController@index']
);
Route::get('/designs/create/{shape_id}',
    ['as' => 'admin.designs.create',
        'uses' => 'DesignController@create']
);
Route::post('/designs/store/{shape_id}',
    ['as' => 'admin.designs.store',
        'uses' => 'DesignController@store']
);
Route::get('/designs/{id}/edit',
    ['as' => 'admin.designs.edit',
        'uses' => 'DesignController@edit']
);
Route::put('/designs/{id}/update/{shape_id}',
    ['as' => 'admin.designs.update',
        'uses' => 'DesignController@update']
);
Route::delete('/designs/{id}/delete',
    ['as' => 'admin.designs.delete',
        'uses' => '\App\Http\Controllers\DesignController@destroy']
);
/* designs */

/* materials */
Route::resource('/materials', 'MaterialController', [
    'as' => 'admin'
]);

Route::delete('/materials/{id}/delete',
    ['as' => 'admin.materials.delete',
        'uses' => '\App\Http\Controllers\MaterialController@destroy']
);
/* materials */

/* products */
Route::post('/products/upload_image', [
    'uses' => '\App\Http\Controllers\ProductController@uploadImage',
    'as' => 'admin.products.upload_image'
]);
Route::delete('/products/delete_image', [
    'uses' => '\App\Http\Controllers\ProductController@deleteImage',
    'as' => 'admin.products.delete_image'
]);

Route::resource('/products', 'ProductController', [
    'as' => 'admin'
]);

Route::delete('/products/{id}/delete',
    ['as' => 'admin.products.delete',
        'uses' => '\App\Http\Controllers\ProductController@destroy']
);
/* products */

/* coupon_codes */
Route::resource('/coupon_codes', 'CouponCodeController', [
    'as' => 'admin'
]);

Route::delete('/coupon_codes/{id}/delete',
    ['as' => 'admin.coupon_codes.delete',
        'uses' => '\App\Http\Controllers\CouponCodeController@destroy']
);
/* coupon_codes */

/* taxes */
Route::get('/taxes/draw',
    ['as' => 'admin.taxes.draw',
        'uses' => '\App\Http\Controllers\TaxController@draw']
);

Route::resource('/taxes', 'TaxController', [
    'as' => 'admin'
]);
/* taxes */

/* orders */
Route::get('/orders/draw', [
        'as' => 'admin.orders.draw',
        'uses' => '\App\Http\Controllers\OrderController@draw']
);

Route::post('/orders/{id}/update_status',
    ['as' => 'admin.orders.update_status',
        'uses' => '\App\Http\Controllers\OrderController@updateStatus']
);

Route::resource('/orders', 'OrderController', [
    'as' => 'admin'
]);

Route::delete('/orders/{id}/delete',
    ['as' => 'admin.orders.delete',
        'uses' => '\App\Http\Controllers\OrderController@destroy']
);
/* orders */

/* why_choose_us_sections */
Route::resource('/why_choose_us_sections', 'WhyChooseUsSectionController', [
    'as' => 'admin'
]);

Route::delete('/why_choose_us_sections/{id}/delete',
    ['as' => 'admin.why_choose_us_sections.delete',
        'uses' => '\App\Http\Controllers\WhyChooseUsSectionController@destroy']
);
/* why_choose_us_sections */