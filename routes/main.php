<?php

/*
|--------------------------------------------------------------------------
| Main Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "main" middleware group. Now create something great!
|
*/

//Route::group(
//    [
//        "middleware" => ['isAdmin', 'revalidate'],
//        "prefix" => 'admin'
//    ], function () {
//
//    /* dashboard routes */
//    Route::get('/', function () {
//        return redirect('/admin/dashboard');
//    });
//
//    /* dashboard */
//    Route::get('/dashboard', [
//        'uses' => '\App\Http\Controllers\AdminDashboardController@index',
//        'as' => 'admin.dashboard',
//    ]);
//    /* dashboard */
//
//    /* users */
//    Route::resource('/users', 'UserController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/users/{id}/delete',
//        ['as' => 'admin.users.delete',
//            'uses' => '\App\Http\Controllers\UserController@destroy']
//    );
//    /* users */
//
//    /* roles */
//    Route::resource('/roles', 'RoleController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/roles/{id}/delete',
//        ['as' => 'admin.roles.delete',
//            'uses' => '\App\Http\Controllers\RoleController@destroy']
//    );
//    /* roles */
//
//    /* permissions */
//    Route::resource('/permissions', 'PermissionController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/permissions/{id}/delete',
//        ['as' => 'admin.permissions.delete',
//            'uses' => '\App\Http\Controllers\PermissionController@destroy']
//    );
//    /* permissions */
//
//    /* permission groups */
//    Route::resource('/permission_groups', 'PermissionGroupController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/permission_groups/{id}/delete',
//        ['as' => 'admin.permission_groups.delete',
//            'uses' => '\App\Http\Controllers\PermissionGroupController@destroy']
//    );
//    /* permission groups */
//
//    /* system settings */
//    Route::resource('/system_settings', 'SystemSettingController', [
//        'as' => 'admin',
//    ]);
//
//    Route::delete('/system_settings/{id}/delete',
//        ['as' => 'admin.system_settings.delete',
//            'uses' => '\App\Http\Controllers\SystemSettingController@destroy']
//    );
//    /* system settings */
//
//    /* posts */
//    Route::resource('/posts', 'PostController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/posts/{id}/delete',
//        ['as' => 'admin.posts.delete',
//            'uses' => '\App\Http\Controllers\PostController@destroy']
//    );
//    /* posts */
//
//    /* pages */
//    Route::resource('/pages', 'PageController', [
//        'as' => 'admin'
//    ]);
//
//    Route::delete('/pages/{id}/delete',
//        ['as' => 'admin.pages.delete',
//            'uses' => '\App\Http\Controllers\PageController@destroy']
//    );
//    /* pages */
//
//
//    /* ckeditor image upload */
//    Route::post('/ckeditor_image_upload',
//        ['as' => 'admin.ckeditor_image_upload',
//            'uses' => '\App\Http\Controllers\PageController@ckEditorImageUpload']
//    );
//    /* ckeditor image upload */
//});

Route::group([
        "prefix" => 'paypal'
    ], function () {
        Route::get('confirm-transaction',
        ['as' => 'paypal.confirm', 'uses' => '\App\Http\Controllers\CheckoutController@paypalConfirm']);
});

Route::group(
    [
        "middleware" => ['isFront', 'revalidate'],
        "prefix" => 'customer'
    ], function () {

    /* dashboard routes */
    Route::get('/', function () {
        return redirect('customer/dashboard');
    });


    Route::get('/dashboard', [
        'uses' => '\App\Http\Controllers\FrontDashboardController@index',
        'as' => 'customer.dashboard',
    ]);

    Route::get('/orders', [
        'uses' => '\App\Http\Controllers\OrderController@indexOrders',
        'as' => 'customer.orders',
    ]);

    Route::get('/address', [
        'uses' => '\App\Http\Controllers\UserController@indexAddress',
        'as' => 'customer.address',
    ]);

    Route::put('/address/update/{id}', [
        'uses' => '\App\Http\Controllers\UserController@updateAddress',
        'as' => 'customer.address.update',
    ]);

    Route::get('/profile', [
        'uses' => '\App\Http\Controllers\UserController@indexProfile',
        'as' => 'customer.profile',
    ]);

    Route::put('/profile/update/{id}', [
        'uses' => '\App\Http\Controllers\UserController@updateProfile',
        'as' => 'customer.profile.update',
    ]);
});

Route::group(
    [
        "middleware" => ['revalidate'],
    ], function () {

    /* contacts */
    Route::post('/contact/store', [
        'uses' => '\App\Http\Controllers\ContactController@store',
        'as' => 'contact.store'
    ]);
    /* contact */

    /* carts */
    Route::post('/carts/store', [
        'uses' => '\App\Http\Controllers\CartController@store',
        'as' => 'carts.store'
    ]);

    Route::put('/carts/{id}/update',
        ['as' => 'carts.update',
            'uses' => '\App\Http\Controllers\CartController@update']
    );

    Route::delete('/carts/{id}/delete',
        ['as' => 'carts.delete',
            'uses' => '\App\Http\Controllers\CartController@destroy']
    );

    Route::post('/coupon_codes/validate',
        ['as' => 'coupon_codes.validate',
            'uses' => '\App\Http\Controllers\CouponCodeController@validateCouponCode']
    );
    /* carts */

    /* checkout */
    Route::post('/checkout',
        ['as' => 'checkout.store',
            'uses' => '\App\Http\Controllers\CheckoutController@checkout']
    ); 

    // validation
    Route::get('/checkout/validation',
    ['as' => 'checkout.validation',
        'uses' => '\App\Http\Controllers\CheckoutController@checkout_validation']
    );

    // storr transaction
    Route::get('/checkout/store/transaction/{transaction_id}',
    ['as' => 'checkout.store.transaction',
        'uses' => '\App\Http\Controllers\CheckoutController@checkout_store_transaction']
    );

    // success checkout
    Route::get('/checkout/{order_ref}/success',
    ['as' => 'checkout.success',
        'uses' => '\App\Http\Controllers\CheckoutController@checkout_success']
    );

    /* checkPrice */
    Route::post('/price-check',
        ['as' => 'price.check',
            'uses' => '\App\Http\Controllers\CartController@priceCheck']
    );

    /* check shipping rate */
    Route::post('/shipping-rate',
        ['as' => 'shipping.rate',
            'uses' => '\App\Http\Controllers\CartController@shippingRate']
    );
    /* checkout */

//    Route::get('/', '\App\Http\Controllers\PageController@home');
    Route::get('/{slug?}/{slug_2?}', '\App\Http\Controllers\PageController@showPages');
});

